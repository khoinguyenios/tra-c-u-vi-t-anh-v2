//
//  RecentViewController.h
//  TraCau
//
//  Created by Mr.NGUYEN on 6/16/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecentViewController : UIViewController

{
    
    __weak IBOutlet UIView *view_Content;
    __weak IBOutlet UITableView *tableView_Recent;
    __weak IBOutlet UITextField *textField_SearchRecent;
    __weak IBOutlet UIImageView *imageView_SearchBox;
    
    
}

@end
