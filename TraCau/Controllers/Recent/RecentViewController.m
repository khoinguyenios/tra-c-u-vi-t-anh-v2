//
//  RecentViewController.m
//  TraCau
//
//  Created by Mr.NGUYEN on 6/16/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import "RecentViewController.h"
#import "Model.h"
#import "Common.h"

@interface RecentViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    NSMutableArray *listRecent;
    NSMutableArray *searchResults;
    BOOL isSearching;
}

@end

@implementation RecentViewController

#pragma mark -
#pragma mark View Life
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/**   */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


//------------------------------------------------------------------------------------------
/**   */
- (void)viewDidLoad
{
    
    [self setVisualForViewDidLoad];
    [self setDataForViewDidLoad];
    [self registerNotification];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


//------------------------------------------------------------------------------------------
/**   */
-(void) viewWillAppear:(BOOL)animated{
    
    if (listRecent.count) {
        [listRecent removeAllObjects];
    }
     [self createListDataStructure];
    [tableView_Recent reloadData];
    
    
}


//------------------------------------------------------------------------------------------
/**   */
-(void)viewDidLayoutSubviews
{
    CGFloat yPoint = [Common buildingStatusBarUsingNavBar:YES WithLabelText:kRecentTitle inView:self.view];
    view_Content.frame = CGRectMake(0, yPoint, view_Content.frame.size.width, view_Content.frame.size.height);
    
    [self createDeleteButtonWithYPoint:yPoint];
    

    [self.view layoutIfNeeded];
    [self.view layoutSubviews ];
}


//------------------------------------------------------------------------------------------
/**   */
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*************************************************************************************************************************/




#pragma mark -
#pragma mark DidLoad Methods
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/** set up visual  */
-(void)setVisualForViewDidLoad
{
    UIImage *searchBoxImage = [UIImage imageNamed:@"recentBox.png"];
    if ([Common checkPlatform] == kPlatformIpad) {
        searchBoxImage = [searchBoxImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 200, 0, 5000) resizingMode:UIImageResizingModeStretch];
        imageView_SearchBox.backgroundColor = [UIColor clearColor];
    }
    else
    {
        searchBoxImage = [searchBoxImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 200, 0, 0) resizingMode:UIImageResizingModeStretch];
    }
    

    imageView_SearchBox.image = searchBoxImage;
    
    GADBannerView *adView =  [Common createGAView:self];
    adView.frame = CGRectMake((self.view.frame.size.width - adView.frame.size.width) / 2, self.view.frame.size.height - adView.frame.size.height - self.tabBarController.tabBar.frame.size.height, adView.frame.size.width, adView.frame.size.height ) ;
    [self.view addSubview:adView];
}


//------------------------------------------------------------------------------------------
/** set up data  */
-(void)setDataForViewDidLoad
{
    tableView_Recent.dataSource = self;
    tableView_Recent.delegate = self;
    
    textField_SearchRecent.delegate = self;
    textField_SearchRecent.placeholder = @"Tìm kiếm";
    listRecent = [[NSMutableArray alloc] init];
   
    
}


//------------------------------------------------------------------------------------------
/**  register notifications */
-(void)registerNotification
{
    
}

/*************************************************************************************************************************/




#pragma mark -
#pragma mark Ad Delegate
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(void)adViewDidReceiveAd:(GADBannerView *)view
{
    tableView_Recent.contentInset = UIEdgeInsetsMake(0, 0, view.frame.size.height+ 64 + 20, 0);

    
}
/*************************************************************************************************************************/

#pragma mark - Table view data source

//------------------------------------------------------------------------------------------
/**   */
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (isSearching) {
        return searchResults.count;
    }
    return listRecent.count;
}
//------------------------------------------------------------------------------------------
/**   */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self returnListHistoryAtIndex:section].count;
}

//------------------------------------------------------------------------------------------
/**   */
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    search_dateField *_search_dateField = [self returnSearchDateAtIndex:section];
    return _search_dateField.date;
}

//------------------------------------------------------------------------------------------
/**   */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSArray *listHistory =  [self returnListHistoryAtIndex:indexPath.section];
    search_historyField *_search_historyField = [listHistory objectAtIndex:indexPath.row];
    
    cell.textLabel.text = _search_historyField.search_word;
    
    return cell;
}


//------------------------------------------------------------------------------------------
/**   */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self showRecentAlertViewAtindexPath:indexPath];
}


//------------------------------------------------------------------------------------------
/**   */
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  YES;
}


//------------------------------------------------------------------------------------------
/**   */
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSMutableArray *workingArray;
        if (isSearching) {
            workingArray = searchResults;
        }
        else
        {
            workingArray = listRecent;
        }
        NSMutableArray *listHistory = [NSMutableArray arrayWithArray:[self returnListHistoryAtIndex:indexPath.section]];
        search_historyField *_search_historyField = [listHistory objectAtIndex:indexPath.row];
        
        search_historyModel *_search_historyModel = [[search_historyModel alloc] init];
        BOOL success = [_search_historyModel deleteRowByRowId:_search_historyField.rowid];
        if (success) {

            [tableView_Recent beginUpdates];
            
            [listHistory removeObjectAtIndex:indexPath.row];
            search_dateField *_search_dateField = [self returnSearchDateAtIndex:indexPath.section];
            [workingArray replaceObjectAtIndex:indexPath.section withObject:@[_search_dateField, listHistory]];
            
            if (listHistory.count == 0) {
                NSArray *listSearchHistoryByDate = [_search_historyModel getRowsInTablesearch_historyByDateId:_search_dateField.rowid];
                if (listSearchHistoryByDate.count == 0) {
                    search_dateModel *_search_dateModel = [[search_dateModel alloc] init];
                    [_search_dateModel deleteRowByRowId:_search_dateField.rowid];
                }
                [workingArray removeObjectAtIndex:indexPath.section];
                [tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
            else
            {
                [tableView_Recent deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
            
            
            [tableView_Recent endUpdates];

            
        }
        
    }
}

/*************************************************************************************************************************/

#pragma mark -
#pragma mark UITextField Delegate
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/**   */
-(BOOL )textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
//    NSString *searchText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (range.length > 0)
    {
        // We're deleting

    }
    else
    {
        // We're adding
    }
    return YES;
}


//------------------------------------------------------------------------------------------
/**   */
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField.text.length) {
    
    }
    
}


//------------------------------------------------------------------------------------------
/**   */
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{

    [self.view endEditing:YES];
    return YES;
}


//------------------------------------------------------------------------------------------
/**   */
-(BOOL)textFieldShouldClear:(UITextField *)textField
{
//    isSearching = NO;
//    [tableView_Recent reloadData];
    return YES;
}

//------------------------------------------------------------------------------------------
/**   */
- (IBAction)textFiedl_EditingChanged:(id)sender {
    
    if (textField_SearchRecent.text.length) {
        isSearching = YES;
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        for (NSArray *childArray in listRecent) {
            NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"search_word contains[c] %@", textField_SearchRecent.text];
            NSArray *array = [[childArray lastObject] filteredArrayUsingPredicate:resultPredicate];
            if (array.count) {
                [tempArray addObject:@[[childArray firstObject], array]];
            }
        }
        
        searchResults = tempArray;

    }
    else
    {
        isSearching = NO;
        if (listRecent.count) {
            [listRecent removeAllObjects];
        }
        [self createListDataStructure];
    }
    
    [tableView_Recent reloadData];
}

/*************************************************************************************************************************/




#pragma mark -
#pragma mark AlertView Speech
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(void)showRecentAlertViewAtindexPath: (NSIndexPath *)selectedIndexPath
{
    NSMutableArray *listHistory = [NSMutableArray arrayWithArray:[self returnListHistoryAtIndex:selectedIndexPath.section]];
    search_historyField *_search_historyField = [listHistory objectAtIndex:selectedIndexPath.row];

    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:kAlertRecentTitle andMessage:kAlertRecentMessage];
    
    [alertView addButtonWithTitle:kButtonRecentOfflineTitle type:SIAlertViewButtonTypeDefault handler:^(SIAlertView *alert) {
        
        [self switchToSearchTabUsing:_search_historyField];
    }];
    
    [alertView addButtonWithTitle:kButtonRecentOnlineTitle type:SIAlertViewButtonTypeDefault  handler:^(SIAlertView *alert) {
       [self switchToSearchTabUsing:_search_historyField.search_word];
    }];
     
    [alertView addButtonWithTitle:kButtonCancelTitle type:SIAlertViewButtonTypeDestructive handler:^(SIAlertView *alert) {
        
    }];
    
    
    alertView.transitionStyle = SIAlertViewTransitionStyleSlideFromBottom;
    
    [alertView show];
    
}


//------------------------------------------------------------------------------------------
/**   */
-(void)switchToSearchTabUsing: (id)sender
{
     self.tabBarController.selectedIndex = 0;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSwitchToSearchTabName object: sender];
}
    
/*************************************************************************************************************************/



#pragma mark -
#pragma mark AlertView Clear history
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(void)showClearConfirmAlertView
{

    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:kAlertClearRecentTitle andMessage:kAlertClearRecentMessage];
    
    [alertView addButtonWithTitle:kButtonCancelClearTitle type:SIAlertViewButtonTypeDefault handler:^(SIAlertView *alert) {
        
        
    }];
    
    
    [alertView addButtonWithTitle:kButtonClearRecentTitle type:SIAlertViewButtonTypeDestructive handler:^(SIAlertView *alert) {
        if (listRecent.count) {
            search_dateModel *_search_dateModel = [[search_dateModel alloc] init];
            [_search_dateModel clearAllRowsInTablesearch_date];
            search_historyModel *_search_historyModel = [[search_historyModel alloc] init];
            [_search_historyModel clearAllRowsInTablesearch_history];
            [listRecent removeAllObjects];
            [tableView_Recent reloadData];
        }

    }];
    
    
    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
    
    [alertView show];
    
}
/*************************************************************************************************************************/



#pragma mark -
#pragma mark Methods
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**  Create back button on navigation bar */
-(void)createDeleteButtonWithYPoint: (CGFloat)yPoint
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *image_StateNormal = [UIImage imageNamed:@"clearbutton.png"];
    UIImage *image_StatePress = [UIImage imageNamed:@"clearbutton_pressed.png"];
    [button setImage:image_StateNormal forState:UIControlStateNormal];
    [button setImage:image_StatePress forState:UIControlStateHighlighted];
    
    [button addTarget:self action:@selector(deleteHistory) forControlEvents:UIControlEventTouchUpInside];
    
    button.frame = CGRectMake( self.view.frame.size.width - image_StateNormal.size.width - kMarginNavButton, (yPoint - 40) + (40 - image_StateNormal.size.height )/ 2, image_StateNormal.size.width, image_StateNormal.size.height);
    
    [self.view addSubview:button];
    
}


//------------------------------------------------------------------------------------------
/**   */
-(void)deleteHistory
{
    [self showClearConfirmAlertView];
}



//------------------------------------------------------------------------------------------
/**   */
-(void)createListDataStructure
{
    search_dateModel *_search_dateModel = [[search_dateModel alloc] init];
    NSArray *listSearchDate = [_search_dateModel getAllRowsInTablesearch_date];
    
    search_historyModel *_search_historyModel = [[search_historyModel alloc] init];
    for (search_dateField *_search_dateField in listSearchDate) {
        NSArray *listSearchField = [_search_historyModel getRowsInTablesearch_historyByDateId:_search_dateField.rowid];
        [listRecent addObject:@[_search_dateField, listSearchField]];
    }

}

//------------------------------------------------------------------------------------------
/**   */
-(NSArray *)returnListHistoryAtIndex: (NSInteger)index
{
    NSArray *childArray;
    if (isSearching) {
        childArray = [searchResults objectAtIndex:index];

    }
    else
    {
        childArray = [listRecent objectAtIndex:index];

    }
    return [childArray lastObject];
}


//------------------------------------------------------------------------------------------
/**   */
-(search_dateField *)returnSearchDateAtIndex: (NSInteger)index
{
    NSArray *childArray;
    if (isSearching) {
        childArray = [searchResults objectAtIndex:index];
        
    }
    else
    {
        childArray = [listRecent objectAtIndex:index];
        
    }

    return [childArray firstObject];
}

/*************************************************************************************************************************/




@end
