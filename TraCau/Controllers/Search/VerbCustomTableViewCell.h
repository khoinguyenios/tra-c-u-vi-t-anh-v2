//
//  VerbCustomTableViewCell.h
//  TraCau
//
//  Created by Mr.NGUYEN on 6/10/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerbCustomTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label_Conjugation;
@property (weak, nonatomic) IBOutlet UILabel *label_Pronoun;

@end
