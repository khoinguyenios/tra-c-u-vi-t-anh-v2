//
//  DynamicHeightCell.h
//  Dynamic Height
//
//  Created by Tim Moose on 5/31/13.
//  Copyright (c) 2013 Tractable Labs. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DynamicHeightCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *label;

@end
