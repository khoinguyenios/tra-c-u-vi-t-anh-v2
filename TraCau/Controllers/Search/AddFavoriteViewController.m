//
//  AddFavoriteViewController.m
//  TraCau
//
//  Created by Mr.NGUYEN on 6/13/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import "AddFavoriteViewController.h"
#import "Model.h"
#import "Common.h"

@interface AddFavoriteViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    NSArray *listRoot;
}
@end

@implementation AddFavoriteViewController

@synthesize resultCell;
@synthesize dictType;


#pragma mark -
#pragma mark View Life
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/**   */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


//------------------------------------------------------------------------------------------
/**   */
- (void)viewDidLoad
{
    [self setVisualForViewDidLoad];
    [self setDataForViewDidLoad];
    [self registerNotification];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


//------------------------------------------------------------------------------------------
/**   */
-(void)viewDidLayoutSubviews
{
    
    
    CGFloat yPoint = [Common buildingStatusBarUsingNavBar:YES WithLabelText:kAddFavoriteTitle inView:self.view];
    tableView_Roots.frame = CGRectMake(0, yPoint, tableView_Roots.frame.size.width, tableView_Roots.frame.size.height);
    
    [self createBackButtonWithYPoint:yPoint];

    
    [self.view layoutIfNeeded];
    [self.view layoutSubviews ];
}



//------------------------------------------------------------------------------------------
/**   */
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*************************************************************************************************************************/






#pragma mark -
#pragma mark Methods
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**  Create back button on navigation bar */
-(void)createBackButtonWithYPoint: (CGFloat)yPoint
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *image_StateNormal = [UIImage imageNamed:@"backbutton.png"];
    UIImage *image_StatePress = [UIImage imageNamed:@"backbutton_pressed.png"];
    [button setImage:image_StateNormal forState:UIControlStateNormal];
    [button setImage:image_StatePress forState:UIControlStateHighlighted];
    
    [button addTarget:self action:@selector(dismissView_Tapped) forControlEvents:UIControlEventTouchUpInside];
    
    button.frame = CGRectMake( kMarginNavButton, (yPoint - 40) + (40 - image_StateNormal.size.height )/ 2, image_StateNormal.size.width, image_StateNormal.size.height);
    
    [self.view addSubview:button];
    
}


//------------------------------------------------------------------------------------------
/**   */
- (IBAction)dismissView_Tapped {
    [self dismissViewControllerAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSetFrameOnSearchView object:nil];
    }];
}
/*************************************************************************************************************************/







#pragma mark -
#pragma mark DidLoad Methods
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/** set up visual  */
-(void)setVisualForViewDidLoad
{
    tableView_Roots.dataSource = self;
    tableView_Roots.delegate = self;
    
}

//------------------------------------------------------------------------------------------
/** set up data  */
-(void)setDataForViewDidLoad
{
    favorite_rootModel *_favorite_rootModel = [[favorite_rootModel alloc] init];
    listRoot = [_favorite_rootModel getAllRowsInTablefavorite_root];
}


//------------------------------------------------------------------------------------------
/**  register notifications */
-(void)registerNotification
{
    
}

/*************************************************************************************************************************/

#pragma mark - Table view data source

//------------------------------------------------------------------------------------------
/**   */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return listRoot.count;
}


//------------------------------------------------------------------------------------------
/**   */
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
 {
     static NSString *CellIdentifier = @"Cell";
     UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
     
     // Configure the cell...
     if (cell == nil) {
         cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
     }
     
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
     favorite_rootField *_favorite_rootField = [listRoot objectAtIndex:indexPath.row ];
     
     cell.textLabel.text = _favorite_rootField.root_name;
     
     return cell;
 }



//------------------------------------------------------------------------------------------
/**   */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    favorite_sentencesModel *_favorite_sentencesModel = [[favorite_sentencesModel alloc] init];
    favorite_sentencesField *_favorite_sentencesField = [[favorite_sentencesField alloc] init];
    _favorite_sentencesField.sentence_type = dictType;
    if([dictType isEqualToString:kDictEn])
    {
        _favorite_sentencesField.sentence_en = resultCell.label_MainText.text;
        _favorite_sentencesField.sentence_vi = resultCell.label_SubText.text;
    }
    else
    {
        _favorite_sentencesField.sentence_vi = resultCell.label_MainText.text;
        _favorite_sentencesField.sentence_en = resultCell.label_SubText.text;
    }
    
    
    _favorite_sentencesField.root_id = [NSString stringWithFormat:@"%i", indexPath.row + 1];
    [_favorite_sentencesModel createRowInTablefavorite_sentences:_favorite_sentencesField];
    
    [self dismissView_Tapped];
}

@end
