//
//  SentenceCustomCell.m
//  TraCau
//
//  Created by Mr.NGUYEN on 6/11/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import "SentenceCustomCell.h"

@implementation SentenceCustomCell

@synthesize delegate;

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//------------------------------------------------------------------------------------------
/**   */
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.view_Content layoutSubviews];
    [self.view_Content layoutIfNeeded];
    
    [self.view_Menu layoutSubviews];
    [self.view_Menu layoutIfNeeded];
    
    [self.contentView layoutIfNeeded];
    
    self.label_MainText.preferredMaxLayoutWidth = CGRectGetWidth(self.label_MainText.frame);
    self.label_SubText.preferredMaxLayoutWidth = CGRectGetWidth(self.label_SubText.frame);
}


//------------------------------------------------------------------------------------------
/**   */
- (IBAction)btn_Favorite_Tapped:(id)sender {
    if ([delegate respondsToSelector:@selector(didSelectedFavorite:)]) {
        [delegate didSelectedFavorite:self];
    }
}

//------------------------------------------------------------------------------------------
/**   */
- (IBAction)btn_Rely_Tapped:(id)sender {
    if ([delegate respondsToSelector:@selector(didSelectedRely:)]) {
        [delegate didSelectedRely:self];
    }
}

//------------------------------------------------------------------------------------------
/**   */
- (IBAction)btn_Speaker_Tapped:(id)sender {
    if ([delegate respondsToSelector:@selector(didSelectedSpeak:)]) {
        [delegate didSelectedSpeak:self];
    }
}


//------------------------------------------------------------------------------------------
/**   */
- (IBAction)btn_Copy_Tapped:(id)sender {
    if ([delegate respondsToSelector:@selector(didSelectedCopy:)]) {
        [delegate didSelectedCopy:self];
    }
}

@end
