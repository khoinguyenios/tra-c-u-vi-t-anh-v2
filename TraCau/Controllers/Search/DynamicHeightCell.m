//
//  DynamicHeightCell.m
//  Dynamic Height
//
//  Created by Tim Moose on 5/31/13.
//  Copyright (c) 2013 Tractable Labs. All rights reserved.
//

#import "DynamicHeightCell.h"

@implementation DynamicHeightCell

// Need to implement layoutSubiews and set the preferred max layout width of the multi-line label or
// the cell height does not get correctly calculated when the device changes orientation.
//
// Credit to this GitHub example project and StackOverflow answer for providing the missing details:
//
// https://github.com/smileyborg/TableViewCellWithAutoLayout
// http://stackoverflow.com/questions/18746929/using-auto-layout-in-uitableview-for-dynamic-cell-layouts-variable-row-heights

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
    self.label.preferredMaxLayoutWidth = CGRectGetWidth(self.label.frame);
}

@end
