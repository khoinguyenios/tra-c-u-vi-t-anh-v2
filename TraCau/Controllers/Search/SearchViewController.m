//
//  SearchViewController.m
//  TraCau
//
//  Created by Mr.NGUYEN on 6/9/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import "SearchViewController.h"
#import "Model.h"
#import "SearchTableViewCell.h"
#import "VerbViewController.h"
#import "SentencesViewController.h"
#import "DynamicHeightCell.h"
#import "Common.h"


#define kKeyboardiPhoneHeight 216
#define kKeyboardiPadPortraitHeight 264

@interface SearchViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIWebViewDelegate, GADBannerViewDelegate>
{
    NSArray *searchResult;
    NSString *tableName;
    verbField *_verbResult;
    VerbViewController *verbVC;
    SentencesViewController *sentencesVC;
    wordsModel *_wordsModel;
    verbModel *_verbModel;
    NSString *searchTextInTextField;

}

@property (nonatomic, strong) DynamicHeightCell *sizingCell;

@end

@implementation SearchViewController

static NSString *DynamicCellIdentifier = @"DynamicCell";

#pragma mark -
#pragma mark View Life
/*************************************************************************************************************************/

#pragma mark -
#pragma mark
/*************************************************************************************************************************/

/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


//------------------------------------------------------------------------------------------
/**   */
- (void)viewDidLoad
{
    [self setVisualForViewDidLoad];
    [self setDataForViewDidLoad];
    [self registerNotification];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


//------------------------------------------------------------------------------------------
/**   */
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*************************************************************************************************************************/


#pragma mark -
#pragma mark DidLoad Methods
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/** set up visual  */
-(void)setVisualForViewDidLoad
{
    //add Verb view
    //[self setUpVerbView];
    //[self performSelectorInBackground:@selector(setUpVerbView) withObject:nil];
    
    //add sentence view
   // [self setUpSentencesView];
   // [self performSelectorInBackground:@selector(setUpSentencesView) withObject:nil];
    
    //add hidekeyboad button
    //[self setUpHideKeyboardButton];
    //[self performSelectorInBackground:@selector(setUpHideKeyboardButton) withObject:nil];

    self.view.backgroundColor = [UIColor colorWithRed:247/255.0f green:247/255.0f blue:247/255.0f alpha:1];

    webView_Words.backgroundColor = [UIColor colorWithRed:247/255.0f green:247/255.0f blue:247/255.0f alpha:1];
    webView_Words.hidden = YES;

    GADBannerView *adView =  [Common createGAView:self];
    adView.frame = CGRectMake((self.view.frame.size.width - adView.frame.size.width) / 2, self.view.frame.size.height - adView.frame.size.height - self.tabBarController.tabBar.frame.size.height, adView.frame.size.width, adView.frame.size.height ) ;
    [self.view addSubview:adView];
    
    [self setUpVerbView];
    [self setUpSentencesView];
    [self setUpHideKeyboardButton];
//    [self createTableSearchResult];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    
    dispatch_async(dispatch_get_main_queue(),^{
      
    });

});

}



-(void)viewWillAppear:(BOOL)animated
{
    CGFloat keyboardHeight;
    if ([Common checkPlatform] == kPlatformIpad) {
        keyboardHeight = kKeyboardiPadPortraitHeight;
    }
    else
    {
        keyboardHeight = kKeyboardiPhoneHeight;
    }

    tableView_SearchResult.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0);
}

//------------------------------------------------------------------------------------------
/**   */
-(void)viewDidLayoutSubviews
{
    CGFloat yPoint =  [Common buildingStatusBarUsingNavBar:NO WithLabelText:nil inView:self.view];
    containView.frame = CGRectMake(0, yPoint, containView.frame.size.width, containView.frame.size.height);
    
    CGFloat keyboardHeight;
    if ([Common checkPlatform] == kPlatformIpad) {
        keyboardHeight = kKeyboardiPadPortraitHeight;
    }
    else
    {
        keyboardHeight = kKeyboardiPhoneHeight;
    }
    
    CGFloat keyboardYPoint = self.view.frame.size.height - keyboardHeight - btn_HideKeyboard.frame.size.height;
    btn_HideKeyboard.frame = CGRectMake(self.view.frame.size.width - btn_HideKeyboard.frame.size.width, keyboardYPoint, btn_HideKeyboard.frame.size.width, btn_HideKeyboard.frame.size.height);


    
    [self.view layoutIfNeeded];
    [ self.view layoutSubviews];
    

}


//------------------------------------------------------------------------------------------
/** set up data  */
-(void)setDataForViewDidLoad
{
    tableView_SearchResult.dataSource = self;
    tableView_SearchResult.delegate = self;
    tableView_SearchResult.hidden = YES;
    
    textField_Search.delegate = self;
    
    webView_Words.delegate = self;
    
    _wordsModel = [[wordsModel alloc] init];
    _verbModel = [[verbModel alloc] init];
    
//    if (![Common isDatabaseExisted]) {
//        [Common showProgressViewWithStatus:@"Dang khoi tao du lieu"];
//        [Common initializingDatabase];
//        [Common dismissProgressView];
//    }
}


//------------------------------------------------------------------------------------------
/**  register notifications */
-(void)registerNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveAddFavoriteNotification) name:kNotificationSetFrameOnSearchView object:nil];
    
    // Listen for keyboard appearances and disappearances
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    //change cell size
    if ([Common isIOS7OrGreater]) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didChangePreferredContentSize:)
                                                     name:UIContentSizeCategoryDidChangeNotification object:nil];

    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didSwitchToSearchTab:)
                                                 name:kNotificationSwitchToSearchTabName object:nil];
}


/*************************************************************************************************************************/



#pragma mark -
#pragma mark Ad Delegate
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(void)adViewDidReceiveAd:(GADBannerView *)view
{
    sentencesVC.tableView_Sentences.contentInset = UIEdgeInsetsMake(0, 0, view.frame.size.height+ 64 + 20, 0);
    verbVC.tableView_Verb.contentInset = UIEdgeInsetsMake(0, 0, view.frame.size.height+ 64 + 20, 0);

}
/*************************************************************************************************************************/



#pragma mark -
#pragma mark Receive Notification Switch To Search Tab
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(void)didSwitchToSearchTab: (NSNotification *)notif
{

    [self btn_ApplySearch_ServiceSentence:notif.object];

}
/*************************************************************************************************************************/

#pragma mark -
#pragma mark SetUp SubView
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(void)setUpVerbView
{
    if (!verbVC) {
        verbVC = [[VerbViewController alloc] initWithNibName:[Common checkDevice:@"VerbViewController"] bundle:nil];

    }
        verbVC.view.hidden = YES;
    verbVC.view.frame = CGRectMake(0, CGRectGetMaxY(view_TextFieldSearch.frame) , CGRectGetWidth(verbVC.view.frame), CGRectGetHeight(verbVC.view.frame));
    
    [containView addSubview:verbVC.view];
    
}


//------------------------------------------------------------------------------------------
/**   */
-(void)setUpSentencesView
{
    if (!sentencesVC) {
        sentencesVC = [[SentencesViewController alloc] initWithNibName:[Common checkDevice:@"SentencesViewController"] bundle:nil];
    }
    
    sentencesVC.view.hidden = YES;
    sentencesVC.view.frame = CGRectMake(0, CGRectGetMaxY(view_TextFieldSearch.frame) , CGRectGetWidth(sentencesVC.view.frame), CGRectGetHeight(sentencesVC.view.frame));
    
    [containView addSubview:sentencesVC.view];
    
}


//------------------------------------------------------------------------------------------
/**   */
-(void)setUpHideKeyboardButton
{
    btn_HideKeyboard = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:btn_HideKeyboard];
    
    
    UIImage *imageStateNormal = [UIImage imageNamed:@"hidekeyboard.png"];
    UIImage *imageStatePressed = [UIImage imageNamed:@"hidekeyboard_pressed.png"];
    
    [btn_HideKeyboard setImage: imageStateNormal  forState:UIControlStateNormal];
    [btn_HideKeyboard setImage:imageStatePressed  forState:UIControlStateHighlighted];
    [btn_HideKeyboard addTarget:self action:@selector(btn_HideKeyboard_Tapped:) forControlEvents:UIControlEventTouchUpInside];
    
    btn_HideKeyboard.frame = CGRectMake(0, 0, 64, 64);
    btn_HideKeyboard.hidden = YES;
    [self.view addSubview:btn_HideKeyboard];
    
}
/*************************************************************************************************************************/






#pragma mark -
#pragma mark UITableView Delegate
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/**   */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == tableView_SearchResult) {
        if (_verbResult) {
            return searchResult.count + 2;

        }
        return searchResult.count + 1;
    }
    return 0;
}


//------------------------------------------------------------------------------------------
/**   */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < 2 ) {
        static NSString *CellIdentifier = @"SearchTableViewCell";
        
        SearchTableViewCell *cell = (SearchTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SearchTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row == 0) {
            //tra cau cell
            cell.imageView_SearchType.image = [UIImage imageNamed:@"prefix.png"];
            cell.label_SearchText.text = searchTextInTextField;
            cell.label_SearchText.textColor = [Common mainThemeColor];
            //            cell.isVerb = NO;
//            cell.searchText = searchTextInTextField;
            return cell;

        }
        
        
        //verb celll
        if (indexPath.row == 1 && _verbResult ) {
            
            UIImage *imageVerb = [UIImage imageNamed:@"verb.png"];
            cell.imageView_SearchType.image = imageVerb;
            cell.imageView_SearchType.frame = CGRectMake(CGRectGetMinX(cell.imageView_SearchType.frame), CGRectGetMinY(cell.imageView_SearchType.frame), 120, 32);
            cell.label_SearchText.frame = CGRectMake(CGRectGetMaxX(cell.imageView_SearchType.frame) + kMarginNavButton, CGRectGetMinY(cell.label_SearchText.frame), CGRectGetWidth(cell.label_SearchText.frame), CGRectGetHeight(cell.label_SearchText.frame));
            cell.label_SearchText.text = searchTextInTextField;
            cell.label_SearchText.textColor = [UIColor colorWithRed:35/255.0f green:129/255.0f blue:17/255.0f alpha:1];
            
//            cell.isVerb = NO;
//            cell.searchText = searchTextInTextField;
            return cell;
        }
        
       
    }
    
    //word cell
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:DynamicCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    
    return cell;
}




//------------------------------------------------------------------------------------------
/**   */
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}


//------------------------------------------------------------------------------------------
/**   */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger deltaNum = 1;
    if (searchResult.count && _verbResult)
        deltaNum++;
    
    if (indexPath.row >= deltaNum) {
        [self configureCell:self.sizingCell forRowAtIndexPath:indexPath];
        self.sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView_SearchResult.bounds), CGRectGetHeight(self.sizingCell.bounds));
        [self.sizingCell layoutIfNeeded];
        
        CGSize size = [self.sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        return size.height + 1;
    }

    return 44;
}




//------------------------------------------------------------------------------------------
/**   */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 && searchTextInTextField.length )
    {
        [self selectSearchSentence];
    }
    else if (indexPath.row == 1 &&  _verbResult)
    {
        [self selectSearchVerb];
    }
    else
    {
        [self selectSearchWordAtIndex:indexPath.row];
    }
    
    [self.view endEditing:YES];
    
    
}


/*************************************************************************************************************************/



#pragma mark -
#pragma mark Resizing Cell Methods
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
- (void)configureCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell isKindOfClass:[DynamicHeightCell class]])
    {
        DynamicHeightCell *textCell = (DynamicHeightCell *)cell;
        
        NSInteger deltaNum = 1;
        if (searchResult.count && _verbResult)
            deltaNum++;
        
        NSString *rowid = [searchResult objectAtIndex:indexPath.row - deltaNum];
        _wordsModel.usesTable = tableName;
        wordsField *_wordsField = [_wordsModel getRowByRowsInTablewordsByRowId:rowid];
        
        textCell.label.text = _wordsField.word;
    }
    
}


//------------------------------------------------------------------------------------------
/**   */
- (DynamicHeightCell *)sizingCell
{
    if (!_sizingCell)
    {
        _sizingCell = [tableView_SearchResult dequeueReusableCellWithIdentifier:DynamicCellIdentifier];
    }
    return _sizingCell;
}


//------------------------------------------------------------------------------------------
/**   */
- (void)didChangePreferredContentSize:(NSNotification *)notification
{
  //  [tableView_SearchResult reloadData];
}
/*************************************************************************************************************************/





#pragma mark -
#pragma mark Did Select Methods
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(void)selectSearchSentence
{
    [self btn_ApplySearch_ServiceSentence:nil];
}

//------------------------------------------------------------------------------------------
/**   */
-(void)selectSearchVerb
{
    [Common showProgressViewWithStatus:kAlertWaitingMessage];
   
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // time-consuming task
        dispatch_async(dispatch_get_main_queue(), ^{
            verbVC.view.hidden = NO;
            webView_Words.hidden = YES;
            sentencesVC.view.hidden = YES;
            tableView_SearchResult.hidden = YES;
            [verbVC reloadDataWithVerbResultId:_verbResult.verb_id];
        });
    });
    
}

//------------------------------------------------------------------------------------------
/**   */
-(void)selectSearchWordAtIndex: (NSInteger)index
{
    webView_Words.hidden = NO;
    [Common showProgressViewWithStatus:kAlertWaitingMessage];
    NSInteger deltaNum = 1;
    if (_verbResult) {
        deltaNum++;
    }
    NSString *rowid = [searchResult objectAtIndex:index - deltaNum];
    
   
    _wordsModel.usesTable = tableName;
    wordsField *_wordsField = [_wordsModel getRowByRowsInTablewordsByRowId:rowid];
    textField_Search.text = _wordsField.word;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"conjunction_template" ofType:@"html"];
    NSString *html = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    html = [html stringByReplacingOccurrencesOfString:@"<!-- REPLACE ME -->" withString:_wordsField.content];
    // Tell the web view to load it
    [webView_Words loadHTMLString:html baseURL:[[NSBundle mainBundle] bundleURL]];

    verbVC.view.hidden = YES;
    webView_Words.hidden = NO;
    sentencesVC.view.hidden = YES;
    tableView_SearchResult.hidden = YES;
    
}
/*************************************************************************************************************************/


#pragma mark -
#pragma mark UIWebview Delegate
/*************************************************************************************************************************/
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    sleep(1);
    [Common dismissProgressView];
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [Common dismissProgressView];
}
/*************************************************************************************************************************/

#pragma mark -
#pragma mark UITextField Delegate
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/**   */
-(BOOL )textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *searchText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (range.length > 0)
    {
        // We're deleting
        if (searchText.length == 0) {
            searchTextInTextField = searchText;
            searchResult = nil;
            [tableView_SearchResult reloadData];
        }
    }
    else
    {
        // We're adding
    }
    return YES;
}


//------------------------------------------------------------------------------------------
/**   */
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField.text.length) {
        tableView_SearchResult.hidden = NO;
        [containView bringSubviewToFront:tableView_SearchResult];
    }
    
}


//------------------------------------------------------------------------------------------
/**   */
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self btn_ApplySearch_ServiceSentence:nil];
    return YES;
}


//------------------------------------------------------------------------------------------
/**   */
-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    tableView_SearchResult.hidden = YES;
    searchResult = nil;
    [tableView_SearchResult reloadData];
    
    return YES;
}

/*************************************************************************************************************************/




#pragma mark -
#pragma mark Search Methods
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/**   */
-(void)getTableNameFromSearchText: (NSString *)searchText
{
    if (![Common isNonNumericWithString:searchText]) {
        tableName = @"words_num";
    }
    else
    {
        unichar uc;
        [[searchText lowercaseString] getBytes:&uc maxLength:2 usedLength:NULL encoding:NSUTF16LittleEndianStringEncoding options:0 range:NSMakeRange(0, 1) remainingRange:NULL];;
        NSString *u = [NSString stringWithFormat:@"%04x", uc];
        tableName = [NSString stringWithFormat:@"words_%@", [u uppercaseString]];
    }
    
}


//------------------------------------------------------------------------------------------
/**   */
- (void)filterContentForSearchText:(NSString*)searchText
{
    _wordsModel.usesTable = tableName;
    searchResult = [_wordsModel getAllRowsIdInTablewordsWithKeyWord:searchText];
    
    _verbResult = [_verbModel searchVerbByKeyWord:searchText];
    
    [tableView_SearchResult reloadData];
    
}


//------------------------------------------------------------------------------------------
/**   */
- (IBAction)btn_ApplySearch_ServiceSentence:(id)sender {
    
    id senderObject = sender;
    if ([senderObject isKindOfClass:[search_historyField class]])
    {
        search_historyField *_search_historyField = (search_historyField*) senderObject;
        textField_Search.text = searchTextInTextField = _search_historyField.search_word;
    }
    else if ([senderObject isKindOfClass:[NSString class]])
    {
        NSString *notifString = [NSString stringWithFormat:@"%@", senderObject];
        textField_Search.text = searchTextInTextField = notifString;

    }
    else
    {
        senderObject = textField_Search.text;
    }
    
    if (textField_Search.text.length) {
        verbVC.view.hidden = YES;
        webView_Words.hidden = YES;
        sentencesVC.view.hidden = NO;
        [Common showProgressViewWithStatus:kAlertWaitingMessage];
        [sentencesVC reloadDataWithSentence:senderObject];
        
    }
    
    [self btn_HideKeyboard_Tapped:nil];
}

//------------------------------------------------------------------------------------------
/**   */
- (IBAction)btn_HideKeyboard_Tapped:(id)sender {
    [self.view endEditing:YES];
    btn_HideKeyboard.hidden = YES;
    tableView_SearchResult.hidden = YES;
}


//------------------------------------------------------------------------------------------
/**   */
- (IBAction)textFieldSearch_EditingChanged:(UITextField *)sender {
    
    searchTextInTextField = sender.text;
    
    if (searchTextInTextField.length) {
        [self getTableNameFromSearchText:[searchTextInTextField substringToIndex:1]];
    }
    
    if (searchTextInTextField.length == 1) {
        tableView_SearchResult.hidden = NO;
        [containView bringSubviewToFront:tableView_SearchResult];
    }
    
    if (searchTextInTextField.length) {
        [self filterContentForSearchText:searchTextInTextField];
    }
}


/*************************************************************************************************************************/




#pragma mark -
#pragma mark Keyboard Notification Methods
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
- (void)keyboardDidShow: (NSNotification *) notif{
    // Do something here
    btn_HideKeyboard.hidden = NO;
    
}


//------------------------------------------------------------------------------------------
/**   */
- (void)keyboardDidHide: (NSNotification *) notif{
    // Do something here
    btn_HideKeyboard.hidden = YES;
    if (!textField_Search.text.length) {
        textField_Search.text = searchTextInTextField;
    }
    
}
/*************************************************************************************************************************/




#pragma mark -
#pragma mark Add Favorite Notification Methods
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/** Set frame again  */
-(void)receiveAddFavoriteNotification
{
    //    NSLog(@"===================After=================");
    //    NSLog(@"Sentence x: %f y: %f", sentencesVC.view.frame.origin.x, sentencesVC.view.frame.origin.y);
    //    NSLog(@"containView x: %f y: %f", containView.frame.origin.x, containView.frame.origin.y);
    verbVC.view.frame = CGRectMake(0, CGRectGetMaxY(view_TextFieldSearch.frame) , CGRectGetWidth(verbVC.view.frame), CGRectGetHeight(verbVC.view.frame));
    
    sentencesVC.view.frame = CGRectMake(0, CGRectGetMaxY(view_TextFieldSearch.frame) , CGRectGetWidth(sentencesVC.view.frame), CGRectGetHeight(sentencesVC.view.frame));
    
    
}
/*************************************************************************************************************************/


@end
