//
//  VerbViewController.h
//  TraCau
//
//  Created by Mr.NGUYEN on 6/10/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerbViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    
    __weak IBOutlet UISegmentedControl *segment_ConjugationGroup;

}

@property (nonatomic, weak) IBOutlet UITableView *tableView_Verb;

//------------------------------------------------------------------------------------------
/**  reload Data With Verb ResultId */
-(void)reloadDataWithVerbResultId: (NSString *)verbResultId;

@end
