//
//  AddFavoriteViewController.h
//  TraCau
//
//  Created by Mr.NGUYEN on 6/13/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SentenceCustomCell.h"

@interface AddFavoriteViewController : UIViewController
{
    __weak IBOutlet UITableView *tableView_Roots;
    
}

@property (nonatomic, strong) SentenceCustomCell *resultCell;
@property (nonatomic, strong) NSString *dictType;

@end
