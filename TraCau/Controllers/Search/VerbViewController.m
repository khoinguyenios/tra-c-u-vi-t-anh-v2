//
//  VerbViewController.m
//  TraCau
//
//  Created by Mr.NGUYEN on 6/10/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import "VerbViewController.h"
#import "VerbCustomTableViewCell.h"
#import "Model.h"
#import "Common.h"

typedef enum : NSUInteger {
    indicative,
    subjunctive,
    conditional,
    other,
} ConjugationGroup;

@interface VerbViewController ()
{
    NSString *verbId;
    NSArray *dataContainer;
    
    NSArray *listindicative;
    NSArray *listSubjunctive;
    NSArray *listConditional;
    NSArray *listOther;
    
    NSArray *listPronouns;
    NSUInteger conjugationGroupSelection;
}
@end

@implementation VerbViewController

@synthesize tableView_Verb = _tableView_Verb;


#pragma mark -
#pragma mark View Life
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/**   */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


//------------------------------------------------------------------------------------------
/**   */
- (void)viewDidLoad
{
    [self setVisualForViewDidLoad];
    [self setDataForViewDidLoad];
    [self registerNotification];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


//------------------------------------------------------------------------------------------
/**   */
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*************************************************************************************************************************/


#pragma mark -
#pragma mark DidLoad Methods
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/** set up visual  */
-(void)setVisualForViewDidLoad
{
    _tableView_Verb.dataSource = self;
    _tableView_Verb.delegate = self;
    
    [self setUpSegment];
    
    _tableView_Verb.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
}

//------------------------------------------------------------------------------------------
/** set up data  */
-(void)setDataForViewDidLoad
{
    conjugationGroupSelection = indicative;
    [self createListPronouns];
}


//------------------------------------------------------------------------------------------
/**  register notifications */
-(void)registerNotification
{
    
}

/*************************************************************************************************************************/



#pragma mark -
#pragma mark Methods
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(void)setUpSegment
{
    //set title
    [segment_ConjugationGroup setTitle:kSegmentInd forSegmentAtIndex:0];
    [segment_ConjugationGroup setTitle:kSegmentSub forSegmentAtIndex:1];
    [segment_ConjugationGroup setTitle:kSegmentCon forSegmentAtIndex:2];
    [segment_ConjugationGroup setTitle:kSegmentOther forSegmentAtIndex:3];
    
}


//------------------------------------------------------------------------------------------
/**   */
- (IBAction)segmentValueChanged:(id)sender {
    
    conjugationGroupSelection = segment_ConjugationGroup.selectedSegmentIndex;
    [_tableView_Verb reloadData];
}

/*************************************************************************************************************************/



#pragma mark -
#pragma mark UITableView Delegate
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/**   */
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return [self returnCurrentListData].count;
}


//------------------------------------------------------------------------------------------
/**   */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (conjugationGroupSelection != other) {
        return [self returnArrayConjugationAtIndex:section].count - 1;
    }
    return [self returnArrayConjugationAtIndex:section].count;
}


//------------------------------------------------------------------------------------------
/**   */
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    conjugation_typeField *_conjugation_typeField = [self returnConjugationTypeFieldAtIndex:section];
    return [_conjugation_typeField.description uppercaseString];
}


//------------------------------------------------------------------------------------------
/**   */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"VerbCustomTableViewCell";
    VerbCustomTableViewCell *cell = (VerbCustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VerbCustomTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
    dispatch_async(queue, ^{
        
        NSArray *conjugationList = [self returnArrayConjugationAtIndex:indexPath.section];
        conjugationField *_conjugationField =  [conjugationList objectAtIndex:indexPath.row];
        
        NSString *pronoun;
        if (_conjugationField.pronounid.length == 0) {
            pronoun = @"";
        }
        else
        {
            pronounField *_pronounField = [listPronouns objectAtIndex:[_conjugationField.pronounid intValue]];
            pronoun = _pronounField.pronoun;
        }
        
                dispatch_sync(dispatch_get_main_queue(), ^{
                    cell.label_Conjugation.text = _conjugationField.verb;
                    cell.label_Pronoun.text = pronoun;
                   });
    });
    
    return cell;
}

/*************************************************************************************************************************/



#pragma mark -
#pragma mark Reload methods
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(void)reloadDataWithVerbResultId: (NSString *)verbResultId
{
     [Common dismissProgressView];
    if (![verbResultId isEqualToString:verbId]) {
        verbId = verbResultId;
        
        listindicative = [self createListDataByGroupId:indicative + 1];
        [_tableView_Verb reloadData];
        
        dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            // Add code here to do background processing
            //
            //
            listSubjunctive = [self createListDataByGroupId:subjunctive + 1];
            listConditional = [self createListDataByGroupId:conditional + 1];
            listOther = [self createListDataByGroupId:other + 1];
           
        });
    }
    
}
/*************************************************************************************************************************/



#pragma mark -
#pragma mark Building data methods
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/**   */
-(NSArray *)createListDataByGroupId: (NSUInteger)groupId
{
    conjugationModel *_conjugationModel = [[conjugationModel alloc] init];
    conjugation_typeModel *_conjugation_typeModel = [[conjugation_typeModel alloc] init];
    
    NSString *conjugation_group_id = [NSString stringWithFormat:@"%lu", (unsigned long)groupId];
    NSArray *conjugationTypes = [_conjugation_typeModel getRowsInTableconjugation_typeByConjugationGroupId:conjugation_group_id];
    NSMutableArray *conjugationStructureArray = [[NSMutableArray alloc] init];
    
    for (conjugation_typeField *_conjugation_typeField in conjugationTypes) {
        NSArray *conjugationList = [_conjugationModel getRowIdsInTableconjugationByVerbId:verbId AndConjugationTypeId:_conjugation_typeField.conjugation_type_id];
        NSArray *wrapperArray = @[_conjugation_typeField, conjugationList];
        [conjugationStructureArray addObject: wrapperArray];
    }
    
    _conjugationModel = nil;
    _conjugation_typeModel = nil;
    
   return conjugationStructureArray;

}


//------------------------------------------------------------------------------------------
/**   */
-(void)createListPronouns
{
    pronounModel *_pronounModel = [[pronounModel alloc] init];
    listPronouns = [_pronounModel getAllRowsInTablepronoun];
    _pronounModel = nil;
}

/*************************************************************************************************************************/


#pragma mark -
#pragma mark Retreive data methods
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(NSArray *)returnCurrentListData
{
    
    switch (conjugationGroupSelection) {
        case indicative:
            return listindicative;
            break;
        case subjunctive:
            return listSubjunctive;
            break;
        case conditional:
            return listConditional;
            break;
        default:
            return listOther;
            break;
    }
}



//------------------------------------------------------------------------------------------
/**   */
-(NSArray *)returnArrayConjugationAtIndex: (NSInteger)index
{
    NSArray *arrayConjugation;
    if ([self returnCurrentListData].count) {
        arrayConjugation = [[[self returnCurrentListData] objectAtIndex:index] lastObject];
    }
    
    return arrayConjugation;
}


//------------------------------------------------------------------------------------------
/**   */
-(conjugation_typeField *)returnConjugationTypeFieldAtIndex: (NSInteger)index
{
    conjugation_typeField *_conjugation_typeField;
    if ([self returnCurrentListData].count) {
        _conjugation_typeField = [[[self returnCurrentListData] objectAtIndex:index] firstObject];
    }
    
    return _conjugation_typeField;
}

/*************************************************************************************************************************/





@end
