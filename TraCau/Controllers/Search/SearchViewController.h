//
//  SearchViewController.h
//  TraCau
//
//  Created by Mr.NGUYEN on 6/9/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController

{
      UIButton *btn_HideKeyboard;
    __weak IBOutlet UIView *containView;
    __weak IBOutlet UIWebView *webView_Words;
    __weak IBOutlet UITableView *tableView_SearchResult;
    __weak IBOutlet UITextField *textField_Search;
    __weak IBOutlet UIView *view_TextFieldSearch;
    __weak IBOutlet UIButton *btn_Search;
    
    __weak IBOutlet UIScrollView *scrollView_FitTableSize;
    __weak IBOutlet NSLayoutConstraint *constraint_TableViewSearchHeight;

}

- (IBAction)btn_ApplySearch_ServiceSentence:(id)sender;
- (IBAction)btn_HideKeyboard_Tapped:(id)sender;

- (IBAction)textFieldSearch_EditingChanged:(id)sender;

@end
