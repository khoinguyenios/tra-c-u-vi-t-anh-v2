//
//  SearchTableViewCell.h
//  TraCau
//
//  Created by Mr.NGUYEN on 6/10/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTableViewCell : UITableViewCell
@property (weak, nonatomic)  UILabel *label_SearchText;
@property (weak, nonatomic)  UIImageView *imageView_SearchType;

@property (assign, nonatomic) BOOL isVerb;
@property (copy, nonatomic) NSString *searchText;
@end
