//
//  SentencesViewController.m
//  TraCau
//
//  Created by Mr.NGUYEN on 6/10/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import "SentencesViewController.h"
#import "SentenceCustomCell.h"
#import "AddFavoriteViewController.h"
#import "Model.h"
#import "Common.h"

@interface SentencesViewController ()<NSURLConnectionDelegate, NSURLConnectionDataDelegate, UITableViewDataSource, UITableViewDelegate, SentenceCustomCellDelegate>
{
    NSArray *listSentences;
    NSMutableData *responseData;
    NSString *sentence;

    NSString *dictType;
}

@property (nonatomic, strong) SentenceCustomCell *sizingCell;

@end


@implementation SentencesViewController

static NSString *CellIdentifier = @"Cell";


#pragma mark -
#pragma mark View Life
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/**   */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


//------------------------------------------------------------------------------------------
/**   */
- (void)viewDidLoad
{
    [self setVisualForViewDidLoad];
    [self setDataForViewDidLoad];
    [self registerNotification];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


//------------------------------------------------------------------------------------------
/**   */
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*************************************************************************************************************************/


#pragma mark -
#pragma mark DidLoad Methods
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/** set up visual  */
-(void)setVisualForViewDidLoad
{
    _tableView_Sentences.dataSource = self;
    _tableView_Sentences.delegate = self;
}

//------------------------------------------------------------------------------------------
/** set up data  */
-(void)setDataForViewDidLoad
{
    
}


//------------------------------------------------------------------------------------------
/**  register notifications */
-(void)registerNotification
{
    
}

/*************************************************************************************************************************/



#pragma mark -
#pragma mark UITableView Delegate
/*************************************************************************************************************************/


//------------------------------------------------------------------------------------------
/**   */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return listSentences.count;
}




//------------------------------------------------------------------------------------------
/**   */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    SentenceCustomCell *cell = (SentenceCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SentenceCustomCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    return cell;
}


//------------------------------------------------------------------------------------------
/**   */
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}


//------------------------------------------------------------------------------------------
/**   */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self configureCell:self.sizingCell forRowAtIndexPath:indexPath];
    self.sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(_tableView_Sentences.bounds), CGRectGetHeight(self.sizingCell.bounds));
    [self.sizingCell layoutIfNeeded];
    
    CGSize size = [self.sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 1;
    

}


//------------------------------------------------------------------------------------------
/**   */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [Common dismissKeyboard];
    if(self.sizingCell)
    {
        self.sizingCell.view_Content.hidden = NO;
        self.sizingCell.view_Menu.hidden = YES;

    }
    SentenceCustomCell *cell = (SentenceCustomCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.view_Content.hidden = YES;
    cell.view_Menu.hidden = NO;

    self.sizingCell = cell;
    
}

/*************************************************************************************************************************/



#pragma mark -
#pragma mark Resizing Cell Methods
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
- (void)configureCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell isKindOfClass:[SentenceCustomCell class]])
    {
        SentenceCustomCell *textCell = (SentenceCustomCell *)cell;
        NSDictionary *dict = [listSentences objectAtIndex:indexPath.row][@"fields"];

        NSString *enString = [Common stringByStrippingHTMLFromString:[dict valueForKey:@"en"]];
        NSString *viString = [Common stringByStrippingHTMLFromString:[dict valueForKey:@"vi"]] ;
        NSMutableAttributedString * string ;

        if ([dictType isEqualToString:kDictEn]) {
            string = [[NSMutableAttributedString alloc]initWithString:enString];
            NSRange range = [[enString lowercaseString] rangeOfString: [sentence lowercaseString]];
            [string addAttribute:NSForegroundColorAttributeName value:[Common mainThemeColor] range:range];
            [textCell.label_MainText setAttributedText:string];
            textCell.label_SubText.text = viString;
            
        }
        else
        {
            string = [[NSMutableAttributedString alloc]initWithString:viString];
            NSRange range = [[viString lowercaseString] rangeOfString: [sentence lowercaseString]];
            [string addAttribute:NSForegroundColorAttributeName value:[Common mainThemeColor] range:range];
            [textCell.label_MainText setAttributedText:string];
            textCell.label_SubText.text = enString;
        }
        textCell.delegate = self;
        textCell.view_Menu.hidden = YES;
        textCell.view_Content.hidden = NO;
    }
    
}


//------------------------------------------------------------------------------------------
/**   */
- (SentenceCustomCell *)sizingCell
{
    if (!_sizingCell)
    {
        _sizingCell = [_tableView_Sentences dequeueReusableCellWithIdentifier:CellIdentifier];

        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SentenceCustomCell" owner:self options:nil];
        _sizingCell = [nib objectAtIndex:0];
        
    }
    return _sizingCell;
}


//------------------------------------------------------------------------------------------
/**   */
- (void)didChangePreferredContentSize:(NSNotification *)notification
{
    [_tableView_Sentences reloadData];
}
/*************************************************************************************************************************/




#pragma mark -
#pragma mark SentenceCell Delegate
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/** Copy function  */
-(void)didSelectedCopy:(SentenceCustomCell *)cell
{
    NSString *enString;
    NSString *viString;
    if ([dictType isEqualToString:kDictEn ]) {
        enString = cell.label_MainText.text;
        viString = cell.label_SubText.text;
    }
    else
    {
        viString = cell.label_MainText.text;
        enString = cell.label_SubText.text;
    }

    [Common showCopyAlertViewWithENSentence:enString VISentence:viString];

}

//------------------------------------------------------------------------------------------
/**  Favorite function */
-(void)didSelectedFavorite:(SentenceCustomCell *)cell
{
    AddFavoriteViewController *addFavoriteViewController = [[AddFavoriteViewController alloc] initWithNibName:[Common checkDevice:@"AddFavoriteViewController"] bundle:nil];
    addFavoriteViewController.dictType = dictType;
    addFavoriteViewController.resultCell = cell;
    
    [self presentViewController:addFavoriteViewController animated:YES completion:nil];
    
}


//------------------------------------------------------------------------------------------
/** Rely function  */
-(void)didSelectedRely:(SentenceCustomCell *)cell{
    
    NSLog(@"%@", cell.label_MainText.text);
}


//------------------------------------------------------------------------------------------
/** Speak function */
-(void)didSelectedSpeak:(SentenceCustomCell *)cell
{
    NSString *speechText ;
    
    if ([dictType isEqualToString:kDictEn ]) {
        speechText = cell.label_MainText.text;
    }
    else
    {
        speechText = cell.label_SubText.text;
    }
    
    [Common showSpeechAlertViewWithSpeechText:speechText];
    
}


/*************************************************************************************************************************/


//------------------------------------------------------------------------------------------
/** reload when active search  */
-(void)reloadDataWithSentence: (id)reloadingObject
{
    if ([reloadingObject isKindOfClass:[search_historyField class]]) {
        
        [Common dismissProgressView];
        search_historyField *_search_historyField = (search_historyField *)reloadingObject;
        NSData *data = [_search_historyField.search_content dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        sentence = _search_historyField.search_word;
        [self handleSearchResult:json];
    }
    else
    {
        NSString *sentencesString = (NSString *) reloadingObject;
        [self reloadViewDataWithSentence:sentencesString];
        
    }
}


//------------------------------------------------------------------------------------------
/**   */
-(void)reloadViewDataWithSentence:(NSString *)sentencesString
{
    sentence = sentencesString;
    dictType = kDictVi;
    if ([Common isContainNonEnglishCharacter:sentence]) {
        dictType = kDictEn;
    }
    
    //NSLog(@"%@",dictType);
    NSString *urlStr = [NSString stringWithFormat:kServiceURL, [sentencesString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] , dictType];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]] delegate:self startImmediately:YES];
    if (connection) {
        if (responseData) {
            responseData = nil;
        }
        responseData = [[NSMutableData alloc] init];
    }

}



#pragma mark -
#pragma mark Connection Delegate
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [Common dismissProgressView];
}

//------------------------------------------------------------------------------------------
/**   */
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [responseData appendData:data];
}


//------------------------------------------------------------------------------------------
/**   */
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    [Common dismissProgressView];
    NSError* error;
    NSDictionary* resultDict = [NSJSONSerialization JSONObjectWithData:responseData  options:kNilOptions error:&error];
    
    [self handleSearchResult:resultDict];
}


//------------------------------------------------------------------------------------------
/**   */
-(void)handleSearchResult: (NSDictionary *)resultDict
{
    [self addToRecentSearchResult:resultDict];
    //NSLog(@"json %@", json);
    listSentences = [resultDict objectForKey:@"sentences"];
    
    [_tableView_Sentences reloadData];

}

/*************************************************************************************************************************/



#pragma mark -
#pragma mark Add to recent list
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/**   */
-(void)addToRecentSearchResult: (NSDictionary *)resultDict
{
    if (![self checkRecentIsExisted]) {
        search_dateModel *_search_dateModel = [[search_dateModel alloc] init];
        NSString *todayString = [Common convertToLocalFormatDate:[NSDate date]];
        search_dateField *_search_dateField = [_search_dateModel getRowInTablesearch_dateByDate:todayString];
        if (!_search_dateField) {
           _search_dateField = [self createNewSearchDateFieldWithDateString:todayString];
        }
        if (_search_dateField.rowid.length) {
            [self createNewRecentFieldWithResultDict:resultDict AndDateId:_search_dateField.rowid];
        }
    }
}


//------------------------------------------------------------------------------------------
/**   */
-(search_dateField *)createNewSearchDateFieldWithDateString: (NSString *)dateString
{
    search_dateModel *_search_dateModel = [[search_dateModel alloc] init];
    search_dateField  *_search_dateField = [[search_dateField alloc] init];
    _search_dateField.date = dateString;
    BOOL success = [_search_dateModel createRowInTablesearch_date:_search_dateField];
    if (success) {
        _search_dateField.rowid =  [NSString stringWithFormat:@"%li",(long)[_search_dateModel getInsertID]];
        
        return _search_dateField;
    }
    
    return nil;
}

//------------------------------------------------------------------------------------------
/**   */
-(void)createNewRecentFieldWithResultDict: (NSDictionary *)resultDict AndDateId: (NSString *)dateId
{
    search_historyModel *_search_historyModel = [[search_historyModel alloc] init];
    
    search_historyField *_search_historyField = [[search_historyField alloc] init];
    _search_historyField.search_date_id = dateId;
    _search_historyField.search_word = sentence;
    NSError* error;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:resultDict
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    NSString* nsJson=  [[NSString alloc] initWithData:jsonData
                                             encoding:NSUTF8StringEncoding];
    _search_historyField.search_content = nsJson;
    
    [_search_historyModel createRowInTablesearch_history:_search_historyField];

}

//------------------------------------------------------------------------------------------
/**   */
-(BOOL)checkRecentIsExisted
{
    search_historyModel *_search_historyModel = [[search_historyModel alloc] init];
    search_historyField *_search_historyField = [_search_historyModel getRowInTablesearch_historyBySearchWord:sentence];
    _search_historyModel = nil;
    if (_search_historyField) {
        return YES;
    }
    return NO;
}
/*************************************************************************************************************************/
@end
