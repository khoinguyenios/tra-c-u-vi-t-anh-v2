//
//  SentenceCustomCell.h
//  TraCau
//
//  Created by Mr.NGUYEN on 6/11/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SentenceCustomCell;

@protocol SentenceCustomCellDelegate <NSObject>

-(void)didSelectedRely: (SentenceCustomCell * )cell;
-(void)didSelectedFavorite: (SentenceCustomCell * )cell;
-(void)didSelectedSpeak: (SentenceCustomCell * )cell;
-(void)didSelectedCopy: (SentenceCustomCell * )cell;

@end

@interface SentenceCustomCell : UITableViewCell
{
    
}
@property (weak, nonatomic) IBOutlet UIView *view_Menu;
@property (weak, nonatomic) IBOutlet UIView *view_Content;
@property (weak, nonatomic) IBOutlet UILabel *label_MainText;
@property (weak, nonatomic) IBOutlet UILabel *label_SubText;

@property (weak, nonatomic) id <SentenceCustomCellDelegate> delegate;
@end
