//
//  FavoriteSentenceCell.m
//  TraCau
//
//  Created by Gau Uni on 6/18/14.
//  Copyright (c) 2014 Gau Uni. All rights reserved.
//

#import "FavoriteSentenceCell.h"

@implementation FavoriteSentenceCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
    self.labelMainText.preferredMaxLayoutWidth = CGRectGetWidth(self.labelMainText.frame);
    self.labelSubText.preferredMaxLayoutWidth = CGRectGetWidth(self.labelSubText.frame);
}
@end
