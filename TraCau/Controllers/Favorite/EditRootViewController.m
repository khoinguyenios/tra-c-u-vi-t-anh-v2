//
//  EditRootViewController.m
//  TraCau
//
//  Created by Mr.NGUYEN on 6/11/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import "EditRootViewController.h"
#import "Common.h"

@interface EditRootViewController ()<UITextFieldDelegate>

@end

@implementation EditRootViewController

@synthesize rootField;
@synthesize  delegate;

#pragma mark -
#pragma mark View Life
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/**   */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


//------------------------------------------------------------------------------------------
/**   */
- (void)viewDidLoad
{
    [self setVisualForViewDidLoad];
    [self setDataForViewDidLoad];
    [self registerNotification];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

//------------------------------------------------------------------------------------------
/**   */
-(void)viewDidLayoutSubviews
{
    NSString *title_Text;
    if (rootField) {
        textField_RootName.text = rootField.root_name;
        title_Text = kEditFavoriteRootTitle;
        
    }
    else
    {
        title_Text = kAddNewFavoriteRootTitle;
        
    }

    
    CGFloat yPoint = [Common buildingStatusBarUsingNavBar:YES WithLabelText:title_Text inView:self.view];
    view_ContainTextField.frame = CGRectMake(0, yPoint, view_ContainTextField.frame.size.width, view_ContainTextField.frame.size.height);
    
    [self createBackButtonWithYPoint:yPoint];
    [self createDoneButtonWithYPoint:yPoint];
    
    [self.view layoutIfNeeded];
    [self.view layoutSubviews ];
}


//------------------------------------------------------------------------------------------
/**   */
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*************************************************************************************************************************/



#pragma mark -
#pragma mark Methods
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**  Create back button on navigation bar */
-(void)createBackButtonWithYPoint: (CGFloat)yPoint
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *image_StateNormal = [UIImage imageNamed:@"backbutton.png"];
    UIImage *image_StatePress = [UIImage imageNamed:@"backbutton_pressed.png"];
    [button setImage:image_StateNormal forState:UIControlStateNormal];
    [button setImage:image_StatePress forState:UIControlStateHighlighted];
    
    [button addTarget:self action:@selector(btn_Dismiss_Tapped:) forControlEvents:UIControlEventTouchUpInside];
    
    button.frame = CGRectMake( kMarginNavButton, (yPoint - 40) + (40 - image_StateNormal.size.height )/ 2, image_StateNormal.size.width, image_StateNormal.size.height);
    
    [self.view addSubview:button];
    
}

//------------------------------------------------------------------------------------------
/**  Create done button on navigation bar */
-(void)createDoneButtonWithYPoint: (CGFloat)yPoint
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *image_StateNormal = [UIImage imageNamed:@"donebutton.png"];
    UIImage *image_StatePress = [UIImage imageNamed:@"donebutton_pressed.png"];
    [button setImage:image_StateNormal forState:UIControlStateNormal];
    [button setImage:image_StatePress forState:UIControlStateHighlighted];
    
    [button addTarget:self action:@selector(btn_Done_Tapped:) forControlEvents:UIControlEventTouchUpInside];
    
    button.frame = CGRectMake( self.view.frame.size.width - image_StateNormal.size.width - kMarginNavButton, (yPoint - 40) + (40 - image_StateNormal.size.height )/ 2, image_StateNormal.size.width, image_StateNormal.size.height);
    
    [self.view addSubview:button];
    
}

/*************************************************************************************************************************/





#pragma mark -
#pragma mark DidLoad Methods
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/** set up visual  */
-(void)setVisualForViewDidLoad
{
    if (rootField) {
        textField_RootName.text = rootField.root_name;
    }

    textField_RootName.delegate = self;
    [textField_RootName becomeFirstResponder];
    
    GADBannerView *adView =  [Common createGAView:self];
    adView.frame = CGRectMake((self.view.frame.size.width - adView.frame.size.width) / 2, self.view.frame.size.height - adView.frame.size.height - self.tabBarController.tabBar.frame.size.height, adView.frame.size.width, adView.frame.size.height ) ;
    [self.view addSubview:adView];
    
}

//------------------------------------------------------------------------------------------
/** set up data  */
-(void)setDataForViewDidLoad
{
    
}


//------------------------------------------------------------------------------------------
/**  register notifications */
-(void)registerNotification
{
    
}

/*************************************************************************************************************************/

- (IBAction)btn_Dismiss_Tapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)btn_Done_Tapped:(id)sender {
    if (textField_RootName.text.length) {
        BOOL success;
         if (rootField) {
           success =  [self editRootName];
         }
        else
        {
           success =  [self addNewRootName];
        }
        
        if (success) {
            [self dismissViewControllerAnimated:YES completion:nil];

        }
    }
}



//------------------------------------------------------------------------------------------
/**   */
-(BOOL)editRootName
{
    BOOL success;
    if ([self checkValidateRootName]) {
        favorite_rootModel *_favorite_rootModel = [[favorite_rootModel alloc] init];
        rootField.root_name = textField_RootName.text;
        success = [_favorite_rootModel updateRowInTablefavorite_root:rootField];
        if (success) {
            if ([delegate respondsToSelector:@selector(didEditRootField::)])
            {
                [delegate didEditRootField:rootField :YES];
            }
            
        }
    }
    else
    {
        success = NO;
    }
    return success;

}



//------------------------------------------------------------------------------------------
/**   */
-(BOOL)addNewRootName
{
    BOOL success;
    if ([self checkValidateRootName]) {
        favorite_rootModel *_favorite_rootModel = [[favorite_rootModel alloc] init];
        favorite_rootField *_favorite_rootField = [[favorite_rootField alloc] init];
        _favorite_rootField.root_name = textField_RootName.text;
         success = [_favorite_rootModel createRowInTablefavorite_root:_favorite_rootField];
        if (success) {
            if ([delegate respondsToSelector:@selector(didEditRootField::)])
            {
                [delegate didEditRootField:_favorite_rootField:NO];
            }
            
        }
    }
    else
    {
        success = NO;
    }
    return success;
}




//------------------------------------------------------------------------------------------
/**   */
-(BOOL)checkValidateRootName
{
    favorite_rootModel *_favorite_rootModel = [[favorite_rootModel alloc] init];
    favorite_rootField *_favorite_rootField = [_favorite_rootModel getRowInTablefavorite_rootByRootName:textField_RootName.text];
    NSString *alertMessage = kAlertRootExisted;
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alertMessage message:alertMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    
    if ((_favorite_rootField && rootField && [rootField.rowid isEqualToString:_favorite_rootField.rowid]) || !_favorite_rootField ) {
        return YES;
    }

    [alertView show];
    return NO;
}



#pragma mark -
#pragma mark UITextfield Delegate
/*************************************************************************************************************************/
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self btn_Done_Tapped:nil];
    
    return YES;
}
/*************************************************************************************************************************/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
