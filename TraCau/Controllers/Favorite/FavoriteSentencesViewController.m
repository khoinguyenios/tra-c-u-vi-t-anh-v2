//
//  FavoriteSentencesViewController.m
//  TraCau
//
//  Created by Mr.NGUYEN on 6/13/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import "FavoriteSentencesViewController.h"
#import "Model.h"
#import "FavoriteSentenceCell.h"
#import "Common.h"

@interface FavoriteSentencesViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *listSentences;
}

@property (nonatomic, strong) FavoriteSentenceCell *sizingCell;

@end

@implementation FavoriteSentencesViewController

static NSString *CellIdentifier = @"Cell";

@synthesize favoriteRootId;
@synthesize favoriteRootName;

#pragma mark -
#pragma mark View Life
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/**   */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


//------------------------------------------------------------------------------------------
/**   */
- (void)viewDidLoad
{
    [self setVisualForViewDidLoad];
    [self setDataForViewDidLoad];
    [self registerNotification];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


//------------------------------------------------------------------------------------------
/**   */
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*************************************************************************************************************************/




#pragma mark -
#pragma mark DidLoad Methods
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/** set up visual  */
-(void)setVisualForViewDidLoad
{

    tableView_Sentence.dataSource = self;
    tableView_Sentence.delegate = self;
    
    if ([Common isIOS7OrGreater])
    tableView_Sentence.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
    
    GADBannerView *adView =  [Common createGAView:self];
    adView.frame = CGRectMake((self.view.frame.size.width - adView.frame.size.width) / 2, self.view.frame.size.height - adView.frame.size.height - self.tabBarController.tabBar.frame.size.height, adView.frame.size.width, adView.frame.size.height ) ;
    [self.view addSubview:adView];
    
}


//------------------------------------------------------------------------------------------
/**   */
-(void)viewDidLayoutSubviews
{
    CGFloat yPoint = [Common buildingStatusBarUsingNavBar:YES WithLabelText:favoriteRootName inView:self.view];
        tableView_Sentence.frame = CGRectMake(0, yPoint, tableView_Sentence.frame.size.width, self.view.frame.size.height - yPoint);

    [self createBackButtonWithYPoint:yPoint];
    
    [self.view layoutIfNeeded];
    [self.view layoutSubviews ];
}



//------------------------------------------------------------------------------------------
/** set up data  */
-(void)setDataForViewDidLoad
{
    favorite_sentencesModel *_favorite_sentencesModel = [[favorite_sentencesModel alloc] init];
    listSentences = [_favorite_sentencesModel getRowsInTablefavorite_sentencesByRootId:favoriteRootId];
}


//------------------------------------------------------------------------------------------
/**  register notifications */
-(void)registerNotification
{
    //change cell size
     if ([Common isIOS7OrGreater])
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didChangePreferredFavoriteSentenceCellContentSize:)
                                                 name:UIContentSizeCategoryDidChangeNotification object:nil];
}

/*************************************************************************************************************************/




#pragma mark -
#pragma mark Methods
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**  Create back button on navigation bar */
-(void)createBackButtonWithYPoint: (CGFloat)yPoint
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *image_StateNormal = [UIImage imageNamed:@"backbutton.png"];
    UIImage *image_StatePress = [UIImage imageNamed:@"backbutton_pressed.png"];
    [button setImage:image_StateNormal forState:UIControlStateNormal];
    [button setImage:image_StatePress forState:UIControlStateHighlighted];
    
    [button addTarget:self action:@selector(backToRootView) forControlEvents:UIControlEventTouchUpInside];
    
    button.frame = CGRectMake( kMarginNavButton, (yPoint - 40) + (40 - image_StateNormal.size.height )/ 2, image_StateNormal.size.width, image_StateNormal.size.height);
    
    [self.view addSubview:button];

}

//------------------------------------------------------------------------------------------
/**   */
-(void)backToRootView
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*************************************************************************************************************************/




#pragma mark -
#pragma mark Resizing Cell Methods
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
- (void)configureCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell isKindOfClass:[FavoriteSentenceCell class]])
    {
        FavoriteSentenceCell *textCell = (FavoriteSentenceCell *)cell;
        favorite_sentencesField *_favorite_sentencesField = [listSentences objectAtIndex:indexPath.row];
        
        // TODO: Mark red for keyword
        
        if ([_favorite_sentencesField.sentence_type isEqualToString:kDictVi]) {
            textCell.labelMainText.text = _favorite_sentencesField.sentence_vi;
            textCell.labelSubText.text = _favorite_sentencesField.sentence_en;
        }
        else
        {
            textCell.labelMainText.text = _favorite_sentencesField.sentence_en;
            textCell.labelSubText.text = _favorite_sentencesField.sentence_vi;
            
        }

    }
}


//------------------------------------------------------------------------------------------
/**   */
- (FavoriteSentenceCell *)sizingCell
{
    if (!_sizingCell)
    {
        _sizingCell = [tableView_Sentence dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    return _sizingCell;
}


//------------------------------------------------------------------------------------------
/**   */
- (void)didChangePreferredFavoriteSentenceCellContentSize:(NSNotification *)notification
{
    [tableView_Sentence reloadData];
}
/*************************************************************************************************************************/



#pragma mark - Table view data source


//------------------------------------------------------------------------------------------
/**   */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return listSentences.count;
}


//------------------------------------------------------------------------------------------
/**   */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    
    return cell;
}


//------------------------------------------------------------------------------------------
/**   */
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}


//------------------------------------------------------------------------------------------
/**   */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self configureCell:self.sizingCell forRowAtIndexPath:indexPath];
    self.sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView_Sentence.bounds), CGRectGetHeight(self.sizingCell.bounds));
    [self.sizingCell layoutIfNeeded];
    
    CGSize size = [self.sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 1;
    
}


//------------------------------------------------------------------------------------------
/**   */
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  YES;
}


//------------------------------------------------------------------------------------------
/**   */
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        favorite_sentencesModel *_favorite_sentencesModel = [[favorite_sentencesModel alloc] init];
         favorite_sentencesField *_favorite_sentencesField = [listSentences objectAtIndex:indexPath.row];
        
       BOOL success = [_favorite_sentencesModel deleteRowByRowId:_favorite_sentencesField.rowid];
        if (success) {
            [tableView_Sentence beginUpdates];
            [tableView_Sentence deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [listSentences removeObjectAtIndex:indexPath.row];
            [tableView_Sentence endUpdates];
            
        }
    }
}


//------------------------------------------------------------------------------------------
/**   */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    favorite_sentencesField *_favorite_sentencesField = [listSentences objectAtIndex:indexPath.row];
   [Common showSpeechAlertViewWithSpeechText:_favorite_sentencesField.sentence_en];

        

}
@end
