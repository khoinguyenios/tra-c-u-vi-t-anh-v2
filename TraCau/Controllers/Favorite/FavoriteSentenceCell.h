//
//  FavoriteSentenceCell.h
//  TraCau
//
//  Created by Gau Uni on 6/18/14.
//  Copyright (c) 2014 Gau Uni. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoriteSentenceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelMainText;
@property (weak, nonatomic) IBOutlet UILabel *labelSubText;

@end
