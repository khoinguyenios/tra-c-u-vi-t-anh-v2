//
//  EditRootViewController.h
//  TraCau
//
//  Created by Mr.NGUYEN on 6/11/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"

@protocol   EditRootViewControllerDelegate <NSObject>

-(void)didEditRootField: (favorite_rootField *)_favorite_rootField : (BOOL)isEdit;

@end


@interface EditRootViewController : UIViewController
{
    
    __weak IBOutlet UIView *view_ContainTextField;
    __weak IBOutlet UITextField *textField_RootName;
}
@property (nonatomic, strong) favorite_rootField *rootField;
@property (nonatomic, weak) id  <EditRootViewControllerDelegate> delegate;
@end
