//
//  FavoriteCustomCell.h
//  TraCau
//
//  Created by Mr.NGUYEN on 6/11/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FavoriteCustomCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *label_RootName;
@property (nonatomic, weak) IBOutlet UIButton *btn_EditRootName;

@end
