//
//  FavoriteSentencesViewController.h
//  TraCau
//
//  Created by Mr.NGUYEN on 6/13/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoriteSentencesViewController : UIViewController
{
    
    __weak IBOutlet UITableView *tableView_Sentence;
}
@property (nonatomic, strong) NSString *favoriteRootId;
@property (nonatomic, strong) NSString *favoriteRootName;
@end
