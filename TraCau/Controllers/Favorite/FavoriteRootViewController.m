//
//  FavoriteRootViewController.m
//  TraCau
//
//  Created by Mr.NGUYEN on 6/13/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import "FavoriteRootViewController.h"
#import "Model.h"
#import "FavoriteSentencesViewController.h"
#import "FavoriteCustomCell.h"
#import "EditRootViewController.h"
#import "Common.h"

@interface FavoriteRootViewController ()<UITableViewDataSource, UITableViewDelegate, EditRootViewControllerDelegate>
{
    NSMutableArray *listFavoriteRoot;
}
@end

@implementation FavoriteRootViewController

#pragma mark -
#pragma mark View Life
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/**   */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


//------------------------------------------------------------------------------------------
/**   */
- (void)viewDidLoad
{
    [self setVisualForViewDidLoad];
    [self setDataForViewDidLoad];
    [self registerNotification];
    
    [super viewDidLoad];
    

    // Do any additional setup after loading the view.
}

//------------------------------------------------------------------------------------------
/**   */
-(void)viewDidLayoutSubviews
{
    CGFloat yPoint = [Common buildingStatusBarUsingNavBar:YES WithLabelText:kFavoriteRootTitle inView:self.view];
    tableView_FavoriteRoot.frame = CGRectMake(0, yPoint, tableView_FavoriteRoot.frame.size.width, self.view.frame.size.height - yPoint);
    
    [self createAddButtonWithYPoint:yPoint];
    
    [self.view layoutIfNeeded];
    [self.view layoutSubviews ];
}


//------------------------------------------------------------------------------------------
/**   */
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*************************************************************************************************************************/



#pragma mark -
#pragma mark Methods
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**  Create back button on navigation bar */
-(void)createAddButtonWithYPoint: (CGFloat)yPoint
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *image_StateNormal = [UIImage imageNamed:@"addbutton.png"];
    UIImage *image_StatePress = [UIImage imageNamed:@"addbutton_pressed.png"];
    [button setImage:image_StateNormal forState:UIControlStateNormal];
    [button setImage:image_StatePress forState:UIControlStateHighlighted];
    
    [button addTarget:self action:@selector(addNewFavoriteRoot) forControlEvents:UIControlEventTouchUpInside];
    
    button.frame = CGRectMake( self.view.frame.size.width - image_StateNormal.size.width - kMarginNavButton, (yPoint - 40) + (40 - image_StateNormal.size.height )/ 2, image_StateNormal.size.width, image_StateNormal.size.height);
    
    [self.view addSubview:button];
    
}

//------------------------------------------------------------------------------------------
/**   */
-(void)addNewFavoriteRoot
{
    EditRootViewController *editRootVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EditRootViewController"];
    editRootVC.delegate = self;
    editRootVC.rootField  = nil;
    [self presentViewController:editRootVC animated:YES completion:nil];
}

/*************************************************************************************************************************/




#pragma mark -
#pragma mark Ad Delegate
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(void)adViewDidReceiveAd:(GADBannerView *)view
{
    tableView_FavoriteRoot.contentInset = UIEdgeInsetsMake(tableView_FavoriteRoot.contentInset.top, tableView_FavoriteRoot.contentInset.left, view.frame.size.height, tableView_FavoriteRoot.contentInset.right);
    
    
}
/*************************************************************************************************************************/


#pragma mark -
#pragma mark DidLoad Methods
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/** set up visual  */
-(void)setVisualForViewDidLoad
{
    tableView_FavoriteRoot.dataSource = self;
    tableView_FavoriteRoot.delegate = self;
    
    GADBannerView *adView =  [Common createGAView:self];
    adView.frame = CGRectMake((self.view.frame.size.width - adView.frame.size.width) / 2, self.view.frame.size.height - adView.frame.size.height - self.tabBarController.tabBar.frame.size.height, adView.frame.size.width, adView.frame.size.height ) ;
    [self.view addSubview:adView];
}

//------------------------------------------------------------------------------------------
/** set up data  */
-(void)setDataForViewDidLoad
{
    favorite_rootModel *_favorite_rootModel = [[favorite_rootModel alloc] init];
    listFavoriteRoot = [_favorite_rootModel getAllRowsInTablefavorite_root];
}


//------------------------------------------------------------------------------------------
/**  register notifications */
-(void)registerNotification
{
    
}

/*************************************************************************************************************************/




#pragma mark - Table view data source


//------------------------------------------------------------------------------------------
/**   */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return listFavoriteRoot.count;
}


//------------------------------------------------------------------------------------------
/**   */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    FavoriteCustomCell *cell = (FavoriteCustomCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[FavoriteCustomCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    favorite_rootField *_favorite_rootField = [listFavoriteRoot objectAtIndex:indexPath.row];
    
    cell.label_RootName.text = _favorite_rootField.root_name;
    cell.btn_EditRootName.tag = indexPath.row;
    
    return cell;
}

//------------------------------------------------------------------------------------------
/**   */
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  YES;
}


//------------------------------------------------------------------------------------------
/**   */
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        favorite_rootField *_favorite_rootField = [listFavoriteRoot objectAtIndex:indexPath.row];
        
        
        BOOL success = [self deleteChildSentencesFromRootId:_favorite_rootField.rowid];
        if (success) {
            
            favorite_rootModel *_favorite_rootModel = [[favorite_rootModel alloc] init];
            success =  [_favorite_rootModel deleteRowByRowId:_favorite_rootField.rowid];
            
            if (success) {
                [tableView_FavoriteRoot beginUpdates];
                [tableView_FavoriteRoot deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                [listFavoriteRoot removeObjectAtIndex:indexPath.row];
                [tableView_FavoriteRoot endUpdates];
                
            }
            
        }
        
    }
}


//------------------------------------------------------------------------------------------
/**   */
-(BOOL)deleteChildSentencesFromRootId: (NSString *)rootId
{
    favorite_sentencesModel *_favorite_sentencesModel = [[favorite_sentencesModel alloc] init];
    NSArray *listSentences = [_favorite_sentencesModel getRowsInTablefavorite_sentencesByRootId:rootId];
    
    BOOL error;
    for (favorite_sentencesField *_favorite_sentencesField in listSentences) {
        error = [_favorite_sentencesModel deleteRowByRowId:_favorite_sentencesField.rowid];
        if (error) {
            return NO;
        }
    }
    
    return YES;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"PushDetail"])
    {
        NSIndexPath *indexPath = [tableView_FavoriteRoot indexPathForSelectedRow];
        favorite_rootField *_favorite_rootField = [listFavoriteRoot objectAtIndex: indexPath.row];
        
        FavoriteSentencesViewController *destViewController = segue.destinationViewController;
        destViewController.favoriteRootId = _favorite_rootField.rowid;
        destViewController.favoriteRootName = _favorite_rootField.root_name;
        
    }
    else if ([[segue identifier] isEqualToString:@"AddNewRoot"])
    {
        
    }
    else
    {
        UIButton *editButton = (UIButton *)sender;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:editButton.tag inSection:0];
        favorite_rootField *_favorite_rootField = [listFavoriteRoot objectAtIndex:indexPath.row];
        
        EditRootViewController *destViewController = segue.destinationViewController;
        destViewController.delegate = self;
        destViewController.rootField = _favorite_rootField;
        
    }
    
}




#pragma mark -
#pragma mark EditRootView Delegate
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(void)didEditRootField:(favorite_rootField *)_favorite_rootField :(BOOL)isEdit
{
    if (isEdit) {
        NSInteger insertIndex = [_favorite_rootField.rowid intValue] - 1;
        [listFavoriteRoot replaceObjectAtIndex:insertIndex withObject:_favorite_rootField];
        [tableView_FavoriteRoot beginUpdates];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:insertIndex inSection:0];
        [tableView_FavoriteRoot reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [tableView_FavoriteRoot endUpdates];
        
    }
    else
    {
        [listFavoriteRoot addObject:_favorite_rootField];
        [tableView_FavoriteRoot beginUpdates];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:listFavoriteRoot.count - 1 inSection:0];
        [tableView_FavoriteRoot insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [tableView_FavoriteRoot endUpdates];
        
        
    }
    
}
/*************************************************************************************************************************/

@end
