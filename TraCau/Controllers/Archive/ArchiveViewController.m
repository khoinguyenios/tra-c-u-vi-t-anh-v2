//
//  ArchiveViewController.m
//  TraCau
//
//  Created by Mr.NGUYEN on 6/16/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import "ArchiveViewController.h"
#import "Model.h"
#import "PatternDetailViewController.h"
#import "Common.h"
#import <Social/Social.h>

@interface ArchiveViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    NSArray *listPatternCategory;
}
@end

@implementation ArchiveViewController

#pragma mark -
#pragma mark View Life
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/**   */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


//------------------------------------------------------------------------------------------
/**   */
- (void)viewDidLoad
{
    [self setVisualForViewDidLoad];
    [self setDataForViewDidLoad];
    [self registerNotification];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

//------------------------------------------------------------------------------------------
/**   */
-(void)viewDidLayoutSubviews
{
    CGFloat yPoint = [Common buildingStatusBarUsingNavBar:YES WithLabelText:kArchiveTitle inView:self.view];
    tableView_Archive.frame = CGRectMake(0, yPoint, tableView_Archive.frame.size.width, self.view.frame.size.height - yPoint );
    
    [self createFacebookButtonWithYPoint:yPoint];
    
    [self.view layoutIfNeeded];
    [self.view layoutSubviews ];
}



//------------------------------------------------------------------------------------------
/**   */
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*************************************************************************************************************************/


#pragma mark -
#pragma mark Methods
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(void)createFacebookButtonWithYPoint: (CGFloat)yPoint
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *image_StateNormal = [UIImage imageNamed:@"fbbutton.png"];
    UIImage *image_StatePress = [UIImage imageNamed:@"fbbutton_pressed.png"];
    [button setImage:image_StateNormal forState:UIControlStateNormal];
    [button setImage:image_StatePress forState:UIControlStateHighlighted];
    
    [button addTarget:self action:@selector(goToFacebookPage) forControlEvents:UIControlEventTouchUpInside];
    
    button.frame = CGRectMake( self.view.frame.size.width - image_StateNormal.size.width - kMarginNavButton, (yPoint - 40) + (40 - image_StateNormal.size.height )/ 2, image_StateNormal.size.width, image_StateNormal.size.height);
    
    [self.view addSubview:button];
}


//------------------------------------------------------------------------------------------
/**   */
-(void)goToFacebookPage
{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [controller setInitialText:kShareString];
        [controller addImage:[UIImage imageNamed:kShareImageName]];
        [controller addURL:[NSURL URLWithString:kShareLinkApp]];
        
            [self presentViewController:controller animated:YES completion:nil];
        
            
        
    } else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please go to Settings to login Facebook" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}
/*************************************************************************************************************************/





#pragma mark -
#pragma mark Ad Delegate
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(void)adViewDidReceiveAd:(GADBannerView *)view
{
    tableView_Archive.contentInset = UIEdgeInsetsMake(tableView_Archive.contentInset.top, tableView_Archive.contentInset.left, view.frame.size.height, tableView_Archive.contentInset.right);
    
}
/*************************************************************************************************************************/



#pragma mark -
#pragma mark DidLoad Methods
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/** set up visual  */
-(void)setVisualForViewDidLoad
{
    tableView_Archive.dataSource = self;
    tableView_Archive.delegate = self;
    
    //Fix top space
    if ([Common isIOS7OrGreater]) {
        tableView_Archive.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
    }
    
    GADBannerView *adView =  [Common createGAView:self];
    adView.frame = CGRectMake((self.view.frame.size.width - adView.frame.size.width) / 2, self.view.frame.size.height - adView.frame.size.height - self.tabBarController.tabBar.frame.size.height, adView.frame.size.width, adView.frame.size.height ) ;
    [self.view addSubview:adView];

}

//------------------------------------------------------------------------------------------
/** set up data  */
-(void)setDataForViewDidLoad
{
    pattern_categoryModel *_pattern_categoryModel = [[pattern_categoryModel alloc] init];
    listPatternCategory = [_pattern_categoryModel getAllRowsInTablepattern_category];
}


//------------------------------------------------------------------------------------------
/**  register notifications */
-(void)registerNotification
{
    
}

/*************************************************************************************************************************/


#pragma mark - Table view data source

//------------------------------------------------------------------------------------------
/**   */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return listPatternCategory.count;
}



//------------------------------------------------------------------------------------------
/**   */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    pattern_categoryField *_pattern_categoryField = [listPatternCategory objectAtIndex:indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = [NSString stringWithFormat:@"%@. %@", _pattern_categoryField.pattern_category_id,_pattern_categoryField.name] ;
    cell.textLabel.font = [UIFont systemFontOfSize:17];
    return cell;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSIndexPath *indexPath = [tableView_Archive indexPathForSelectedRow];
    pattern_categoryField *_pattern_categoryField = [listPatternCategory objectAtIndex: indexPath.row];
    
    PatternDetailViewController *destViewController = segue.destinationViewController;
    destViewController.categoryId = _pattern_categoryField.pattern_category_id;
    destViewController.titleText =  [NSString stringWithFormat:@"%@. %@", _pattern_categoryField.pattern_category_id,_pattern_categoryField.name];
}


@end
