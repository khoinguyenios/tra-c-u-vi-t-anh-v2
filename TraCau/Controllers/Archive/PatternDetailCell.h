//
//  PatternDetailCell.h
//  TraCau
//
//  Created by Gau Uni on 6/18/14.
//  Copyright (c) 2014 Gau Uni. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PatternDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label_SentenceEN;
@property (weak, nonatomic) IBOutlet UILabel *label_SentenceVI;

@end
