//
//  PatternDetailViewController.h
//  TraCau
//
//  Created by Mr.NGUYEN on 6/16/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PatternDetailViewController : UIViewController
{
    
    __weak IBOutlet UITableView *tableView_PatternDetail;
}
@property (nonatomic , strong) NSString *categoryId;
@property (nonatomic, strong) NSString *titleText;
@end
