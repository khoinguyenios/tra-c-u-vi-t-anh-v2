//
//  PatternDetailViewController.m
//  TraCau
//
//  Created by Mr.NGUYEN on 6/16/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import "PatternDetailViewController.h"
#import "Model.h"
#import "PatternDetailCell.h"
#import "Common.h"

@interface PatternDetailViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    NSArray *listPattern;
}

@property (nonatomic, strong) PatternDetailCell *sizingCell;

@end

@implementation PatternDetailViewController

static NSString *CellIdentifier = @"Cell";

@synthesize categoryId;
@synthesize titleText;

#pragma mark -
#pragma mark View Life
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/**   */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


//------------------------------------------------------------------------------------------
/**   */
- (void)viewDidLoad
{
    [self setVisualForViewDidLoad];
    [self setDataForViewDidLoad];
    [self registerNotification];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


//------------------------------------------------------------------------------------------
/**   */
-(void)viewDidLayoutSubviews
{
    CGFloat yPoint = [Common buildingStatusBarUsingNavBar:YES WithLabelText:titleText inView:self.view];
    tableView_PatternDetail.frame = CGRectMake(0, yPoint, tableView_PatternDetail.frame.size.width, self.view.frame.size.height - yPoint);
    
    [self createBackButtonWithYPoint:yPoint];
    
    [self.view layoutIfNeeded];
    [self.view layoutSubviews ];
}



//------------------------------------------------------------------------------------------
/**   */
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*************************************************************************************************************************/


#pragma mark -
#pragma mark Methods
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(void)createBackButtonWithYPoint: (CGFloat)yPoint
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *image_StateNormal = [UIImage imageNamed:@"backbutton.png"];
    UIImage *image_StatePress = [UIImage imageNamed:@"backbutton_pressed.png"];
    [button setImage:image_StateNormal forState:UIControlStateNormal];
    [button setImage:image_StatePress forState:UIControlStateHighlighted];
    
    [button addTarget:self action:@selector(backToRootView) forControlEvents:UIControlEventTouchUpInside];
    
    button.frame = CGRectMake( kMarginNavButton, (yPoint - 40) + (40 - image_StateNormal.size.height )/ 2, image_StateNormal.size.width, image_StateNormal.size.height);
    
    [self.view addSubview:button];
}


//------------------------------------------------------------------------------------------
/**   */
-(void)backToRootView
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
/*************************************************************************************************************************/




#pragma mark -
#pragma mark Ad Delegate
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
-(void)adViewDidReceiveAd:(GADBannerView *)view
{
    tableView_PatternDetail.contentInset = UIEdgeInsetsMake(tableView_PatternDetail.contentInset.top, tableView_PatternDetail.contentInset.left, view.frame.size.height, tableView_PatternDetail.contentInset.right);
    
    
}
/*************************************************************************************************************************/



#pragma mark -
#pragma mark DidLoad Methods
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/** set up visual  */
-(void)setVisualForViewDidLoad
{
    tableView_PatternDetail.dataSource = self;
    tableView_PatternDetail.delegate = self;
    
    if ([Common isIOS7OrGreater]) {
         tableView_PatternDetail.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
    }
    GADBannerView *adView =  [Common createGAView:self];
    adView.frame = CGRectMake((self.view.frame.size.width - adView.frame.size.width) / 2, self.view.frame.size.height - adView.frame.size.height - self.tabBarController.tabBar.frame.size.height, adView.frame.size.width, adView.frame.size.height ) ;
    [self.view addSubview:adView];
    
}

//------------------------------------------------------------------------------------------
/** set up data  */
-(void)setDataForViewDidLoad
{
    patternModel *_patternModel = [[patternModel alloc] init];
    listPattern = [_patternModel getPatternsByCategoryId:categoryId];
  
    
}


//------------------------------------------------------------------------------------------
/**  register notifications */
-(void)registerNotification
{
    //change cell size
     if ([Common isIOS7OrGreater])
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didChangePreferredPatternDetailCellContentSize:)
                                                 name:UIContentSizeCategoryDidChangeNotification object:nil];
}

/*************************************************************************************************************************/



#pragma mark - Table view data source

//------------------------------------------------------------------------------------------
/**   */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return listPattern.count;
}


//------------------------------------------------------------------------------------------
/**   */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    return cell;
}


//------------------------------------------------------------------------------------------
/**   */
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}


//------------------------------------------------------------------------------------------
/**   */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    [self configureCell:self.sizingCell forRowAtIndexPath:indexPath];
    self.sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView_PatternDetail.bounds), CGRectGetHeight(self.sizingCell.bounds));
    [self.sizingCell layoutIfNeeded];
    
    CGSize size = [self.sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 1;

}

//------------------------------------------------------------------------------------------
/**   */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    patternField *_patternField= [listPattern objectAtIndex:indexPath.row];
    [Common showSpeechAlertViewWithSpeechText:_patternField.sentence_en];

}


#pragma mark -
#pragma mark Resizing Cell Methods
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
- (void)configureCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell isKindOfClass:[PatternDetailCell class]])
    {
        PatternDetailCell *textCell = (PatternDetailCell *)cell;
        patternField *_patternField= [listPattern objectAtIndex:indexPath.row];
        textCell.label_SentenceEN.text = _patternField.sentence_en;
        textCell.label_SentenceVI.text = _patternField.sentence_vi;
        textCell.label_SentenceVI.textColor = [Common mainThemeColor];
    }
}


//------------------------------------------------------------------------------------------
/**   */
- (PatternDetailCell *)sizingCell
{
    if (!_sizingCell)
    {
        _sizingCell = [tableView_PatternDetail dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    return _sizingCell;
}


//------------------------------------------------------------------------------------------
/**   */
- (void)didChangePreferredPatternDetailCellContentSize:(NSNotification *)notification
{
    [tableView_PatternDetail reloadData];
}
/*************************************************************************************************************************/
@end
