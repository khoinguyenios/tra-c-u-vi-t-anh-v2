//
//  MoreViewController.m
//  TraCau
//
//  Created by Gau Uni on 6/20/14.
//  Copyright (c) 2014 Gau Uni. All rights reserved.
//

#import "MoreViewController.h"
#import "Common.h"

@interface MoreViewController ()

@end

@implementation MoreViewController

#pragma mark -
#pragma mark View Life
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/**   */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//------------------------------------------------------------------------------------------
/**   */
-(void)viewDidLayoutSubviews
{
    CGFloat yPoint = [Common buildingStatusBarUsingNavBar:YES WithLabelText:kMoreTitle inView:self.view];
//    view_Content.frame = CGRectMake(0, yPoint, view_Content.frame.size.width, view_Content.frame.size.height);
//    
//    [self createDeleteButtonWithYPoint:yPoint];
    
    
    [self.view layoutIfNeeded];
    [self.view layoutSubviews ];
}

//------------------------------------------------------------------------------------------
/**   */
- (void)viewDidLoad
{
    [self setVisualForViewDidLoad];
    [self setDataForViewDidLoad];
    [self registerNotification];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


//------------------------------------------------------------------------------------------
/**   */
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*************************************************************************************************************************/


#pragma mark -
#pragma mark DidLoad Methods
/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/** set up visual  */
-(void)setVisualForViewDidLoad
{
    
}

//------------------------------------------------------------------------------------------
/** set up data  */
-(void)setDataForViewDidLoad
{
    
}


//------------------------------------------------------------------------------------------
/**  register notifications */
-(void)registerNotification
{
    
}

/*************************************************************************************************************************/


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
