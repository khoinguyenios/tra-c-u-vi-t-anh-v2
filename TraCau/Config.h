//
//  Config.h
//  TraCau
//
//  Created by Mr.NGUYEN on 6/9/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#define kNameDatabase @"TraCau.sqlite"

#define kShareLinkApp @""
#define kShareImageName @""
#define kShareString @""

#define kDictEn @"en"
#define kDictVi @"vi"

#define kServiceURL @"http://tracau.vn:1981//WBBcwnwQpV89/%@/%@/JSON_NO_CALLBACK"

#define kNotificationSetFrameOnSearchView @"SetFrame"

#define kiSpeechAPIKey @"c0d7f242ff6f2af2ec07ca8ef4657e3d"

#define kFontSizeViewTitle 22

#define kAlertWaitingMessage @"Xin chờ giây lát..."

#define kMarginNavButton 10

#define kGAUnitId @"ca-app-pub-3675488384187921/4829404491"

#pragma mark -
#pragma mark Tabbar Item Title
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**  */
#define kSearchTab @"Tra cứu"

//------------------------------------------------------------------------------------------
/**  */
#define kFavoriteTab @"Ưa thích"

//------------------------------------------------------------------------------------------
/**  */
#define kArchiveTab @"Mẫu câu"

//------------------------------------------------------------------------------------------
/**  */
#define kRecentTab @"Lịch sử"

//------------------------------------------------------------------------------------------
/**  */
#define kMoreTab @"Trợ giúp"

/*************************************************************************************************************************/



#pragma mark -
#pragma mark View Label Title
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/** Title for AlertRootExisted  */
#define kAlertRootExisted @"Tên danh mục đã tồn tại. Bạn hãy chọn tên khác"

//------------------------------------------------------------------------------------------
/** Title for View Add Favorite  */
#define kAddFavoriteTitle @"Chọn danh mục lưu trữ"


//------------------------------------------------------------------------------------------
/** Title for View Recent  */
#define kRecentTitle @"Lịch sử"


//------------------------------------------------------------------------------------------
/** Title for View Archive  */
#define kArchiveTitle @"Mẫu câu thông dụng"


//------------------------------------------------------------------------------------------
/** Title for View Edit Favorite  Root*/
#define kEditFavoriteRootTitle @"Sửa tên danh mục"


//------------------------------------------------------------------------------------------
/** Title for View Add New Favorite  Root*/
#define kAddNewFavoriteRootTitle @"Đặt tên danh mục"

//------------------------------------------------------------------------------------------
/** Title for View Favorite  Root*/
#define kFavoriteRootTitle @"Danh mục ưa thích"

//------------------------------------------------------------------------------------------
/** Title for View More */
#define kMoreTitle @"Trợ giúp"
/*************************************************************************************************************************/



#pragma mark -
#pragma mark Alert Speech
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/** Alert title*/
#define kAlertSpeechTitle @"Phát âm Tiếng Anh"

//------------------------------------------------------------------------------------------
/** Alert message*/
#define kAlertSpeechMessage @"Bạn có thể lựa chọn phát âm Tiếng Anh với Ứng dụng có sẵn (giọng đọc Robotic - không cần kết nối internet) hoặc Dịch vụ iSpeech ( giọng đọc Natural - cần kết nối internet)"

#define kAlertSpeechiOS6Message @"Chức năng này chỉ hoạt động trên iOS 7"


//------------------------------------------------------------------------------------------
/** Button using speech native*/
#define kButtonNativeSpeechTitle @"Ứng dụng có sẵn (Offline)"

//------------------------------------------------------------------------------------------
/** Button using iSpeech*/
#define kButtoniSpeechTitle @"Dịch vụ iSpeech (Online)"

//------------------------------------------------------------------------------------------
/** Button cancel*/
#define kButtonCancelTitle @"Bỏ qua"

/*************************************************************************************************************************/



#pragma mark -
#pragma mark Alert Copy
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/** Alert copy title*/
#define kAlertCopyTitle @"Sao chép"

//------------------------------------------------------------------------------------------
/** Alert copy message*/
#define kAlertCopyMessage @"Sao chép vào bộ nhớ để sử dụng trong các ứng dụng soạn thảo khác như: email, tin nhắn..."

//------------------------------------------------------------------------------------------
/** Button all sentences*/
#define kButtonAllSentenceTitle @"Toàn bộ cặp câu"

//------------------------------------------------------------------------------------------
/** Button en sentence*/
#define kButtonENSentenceTitle @"Câu Tiếng Anh"

//------------------------------------------------------------------------------------------
/** Button vi sentence*/
#define kButtonVISentenceTitle @"Câu Tiếng Việt"
/*************************************************************************************************************************/



#pragma mark -
#pragma mark Alert Recent
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/** Alert title*/
#define kAlertRecentTitle @"Xem lại tra cứu cũ"

//------------------------------------------------------------------------------------------
/** Alert message*/
#define kAlertRecentMessage @"Bạn có thể xem lại tra cứu gần đây bằng kết quả lưu lại trong máy hoặc kết quả cập nhật mới nhất trên mạng internet)"

//------------------------------------------------------------------------------------------
/** Button using dữ liệu cũ*/
#define kButtonRecentOfflineTitle @"Dữ liệu cũ (Offline)"

//------------------------------------------------------------------------------------------
/** Button using dữ liệu trực tuyến*/
#define kButtonRecentOnlineTitle @"Dữ liệu trực tuyến (Online)"

#define kNotificationSwitchToSearchTabName @"SwitchToSearchTab"
/*************************************************************************************************************************/


#pragma mark -
#pragma mark Alert Clear history
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/** Alert clear title*/
#define kAlertClearRecentTitle @"Xác nhận"

//------------------------------------------------------------------------------------------
/** Alert clear message*/
#define kAlertClearRecentMessage @"Bạn có đồng ý xóa tất cả danh sách các câu/cụm từ đã tìm kiếm?"

//------------------------------------------------------------------------------------------
/** Button using dữ liệu cũ*/
#define kButtonClearRecentTitle @"Đồng ý"

//------------------------------------------------------------------------------------------
/** Button using dữ liệu trực tuyến*/
#define kButtonCancelClearTitle @"Không"

/*************************************************************************************************************************/


#pragma mark -
#pragma mark Segment Title
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
#define kSegmentInd @"Chỉ định"

//------------------------------------------------------------------------------------------
/**   */
#define kSegmentSub @"Giả định"

//------------------------------------------------------------------------------------------
/**   */
#define kSegmentCon @"Điều kiện"

//------------------------------------------------------------------------------------------
/**   */
#define kSegmentOther @"Khác"

/*************************************************************************************************************************/


