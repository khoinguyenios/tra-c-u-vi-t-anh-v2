//
//  AppDelegate.m
//  TraCau
//
//  Created by Mr.NGUYEN on 6/9/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import "AppDelegate.h"
#import "iSpeechSDK.h"
#import "Common.h"

@implementation AppDelegate

//@synthesize appModel = _appModel;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    //iSpeech
    [[iSpeechSDK sharedSDK] setAPIKey:kiSpeechAPIKey];
    
    
    //AppModel
//    if (!_appModel) {
//        _appModel = [[AppModel alloc] init];
//        
//    }
    
    
    // Assign tab bar item with titles
    [self configTabbar];

    
    return YES;
}

//------------------------------------------------------------------------------------------
/**   */
-(void)configTabbar
{
    UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
    UITabBar *tabBar = tabBarController.tabBar;
    // TODO: move to common methods
    
    //[tabBar setTintColor:[UIColor redColor]];
    
    //Search tab
    UIImage *search_Normal = [UIImage imageNamed:@"search.png"];
    UIImage *search_Highlighted = [UIImage imageNamed:@"search_selected.png"];
    
    //Favorite tab
    UIImage *favorite_Normal = [UIImage imageNamed:@"favorite.png"];
    UIImage *favorite_Highlighted = [UIImage imageNamed:@"favorite_selected.png"];
    
    //Archive tab
    UIImage *archive_Normal = [UIImage imageNamed:@"archive.png"];
    UIImage *archive_Highlighted = [UIImage imageNamed:@"archive_selected.png"];
    
    //Recent tab
    UIImage *recent_Normal = [UIImage imageNamed:@"recent.png"];
    UIImage *recent_Highlighted = [UIImage imageNamed:@"recent_selected.png"];
    
    //More tab
    UIImage *more_Normal = [UIImage imageNamed:@"more.png"];
    UIImage *more_Highlighted = [UIImage imageNamed:@"more_selected.png"];
    
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
    UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
    UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
    UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
    
    UITabBarItem *tabBarItem5;
    if (tabBar.items.count == 5) {
         tabBarItem5 = [tabBar.items objectAtIndex:4];
    }
    

    
    if ([Common isIOS7OrGreater])
    {
        
        //Search tab

        search_Normal = [search_Normal imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        search_Highlighted = [search_Highlighted imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
        tabBarItem1 =  [tabBarItem1 initWithTitle:kSearchTab image:search_Normal selectedImage:search_Highlighted];
        
        //Favorite tab
        
        favorite_Normal = [favorite_Normal imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        favorite_Highlighted = [favorite_Highlighted imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];

        tabBarItem2 =  [tabBarItem2 initWithTitle:kFavoriteTab image:favorite_Normal selectedImage:favorite_Highlighted];
        
        //Archive tab
        
        archive_Normal = [archive_Normal imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        archive_Highlighted = [archive_Highlighted imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];

        tabBarItem3 =  [tabBarItem3 initWithTitle:kArchiveTab image: archive_Normal selectedImage: archive_Highlighted];
        
        //Recent tab

        recent_Normal = [recent_Normal imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        recent_Highlighted = [recent_Highlighted imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];

        tabBarItem4 =  [tabBarItem4 initWithTitle:kRecentTab image:recent_Normal selectedImage:recent_Highlighted];
        
        //More tab
        
        if (tabBarItem5) {
            more_Normal = [more_Normal imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            more_Highlighted = [more_Highlighted imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            
            tabBarItem5 =  [tabBarItem5 initWithTitle:kMoreTab image:more_Normal selectedImage:more_Highlighted];

        }
        
        
    }
    else
    {
        //Search tab
        tabBarItem1.title = kSearchTab;
        [tabBarItem1 setFinishedSelectedImage:search_Highlighted withFinishedUnselectedImage:search_Normal];
        
        //Favorite tab
        tabBarItem2.title = kFavoriteTab;
        [tabBarItem2 setFinishedSelectedImage:favorite_Highlighted withFinishedUnselectedImage:favorite_Normal];
        
        //Archive tab
        tabBarItem3.title = kArchiveTab;
        [tabBarItem3 setFinishedSelectedImage:archive_Highlighted withFinishedUnselectedImage:archive_Normal];
        
        //Recent tab
        tabBarItem4.title = kRecentTab;
        [tabBarItem4 setFinishedSelectedImage:recent_Highlighted withFinishedUnselectedImage:recent_Normal];
        
        //More tab
        if (tabBarItem5) {
            tabBarItem5.title = kMoreTab;
            [tabBarItem5 setFinishedSelectedImage:more_Highlighted withFinishedUnselectedImage:more_Normal];

        }
        
    }
    
    // Change the tab bar background
    UIImage* tabBarBackground = [UIImage imageNamed: [Common getImageName:@"tabbarBG.png"]];
    [[UITabBar appearance] setBackgroundImage:tabBarBackground];
    
    UIImage *tabItemSelectedImage = [[UIImage imageNamed: [Common getImageName:@"tabitem_selected.png"]] stretchableImageWithLeftCapWidth:10 topCapHeight:0];
    
    [[UITabBar appearance] setSelectionIndicatorImage:tabItemSelectedImage];
    
    // Change the title color of tab bar items
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:  [UIColor grayColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    UIColor *titleHighlightedColor = [UIColor redColor];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:   titleHighlightedColor, NSForegroundColorAttributeName,   nil] forState:UIControlStateSelected];


}

//------------------------------------------------------------------------------------------
/**   */
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}


//------------------------------------------------------------------------------------------
/**   */
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


//------------------------------------------------------------------------------------------
/**   */
- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}


//------------------------------------------------------------------------------------------
/**   */
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


//------------------------------------------------------------------------------------------
/**   */
- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
