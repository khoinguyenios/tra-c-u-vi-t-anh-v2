//
//  Common.m
//  TraCau
//
//  Created by Mr.NGUYEN on 6/9/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import "Common.h"
#import "iSpeechSDK.h"

#import <AVFoundation/AVFoundation.h>
#import "SVProgressHUD.h"

#import "FMDB.h"
#import "MBProgressHUD.h"
#import "GADBannerView.h"


@implementation Common



#pragma mark -
#pragma mark Initializing database
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
+(NSString *)documentsDirectoryPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}

//------------------------------------------------------------------------------------------
/**   */
+(NSArray *)listTableNameInDatabase
{
    NSArray *listTableName = @[@"conjugation", @"conjugation_group", @"conjugation_type", @"favorite_root", @"favorite_sentences", @"pattern", @"pattern_category", @"pronoun", @"search_date", @"search_history", @"verb",  @"words_0061", @"words_0062", @"words_0063", @"words_0064", @"words_0065", @"words_0066", @"words_0067", @"words_0068", @"words_0069", @"words_006A", @"words_006B", @"words_006C", @"words_006D", @"words_006E", @"words_006F", @"words_0070", @"words_0071", @"words_0072", @"words_0073", @"words_0074", @"words_0075", @"words_0076", @"words_0077", @"words_0078", @"words_0079", @"words_007A", @"words_00E0", @"words_00E1", @"words_00E2", @"words_00E8", @"words_00E9", @"words_00EA", @"words_00EC", @"words_00ED", @"words_00F2", @"words_00F3", @"words_00F4", @"words_00F5", @"words_00F9", @"words_00FA", @"words_00FC", @"words_0103", @"words_0129", @"words_01A1", @"words_01B0", @"words_1EA1", @"words_1EA3", @"words_1EA5", @"words_1EA7", @"words_1EA9", @"words_1EAB", @"words_1EAD", @"words_1EAF", @"words_1EB3", @"words_1EB5", @"words_1EB9", @"words_1EBB", @"words_1EBD", @"words_1EBF", @"words_1EC1", @"words_1EC5", @"words_1EC7", @"words_1EC9", @"words_1ECB", @"words_1ECD", @"words_1ECF", @"words_1ED1", @"words_1ED3", @"words_1ED5", @"words_1ED8", @"words_1EDB", @"words_1EDD", @"words_1EDF", @"words_1EE1", @"words_1EE3", @"words_1EE5", @"words_1EE7", @"words_1EE9", @"words_1EEB", @"words_1EED", @"words_1EF1", @"words_1EF3", @"words_1EF7", @"words_num"];
    
    return listTableName;
}


//------------------------------------------------------------------------------------------
/**   */
+(BOOL)isDatabaseExisted
{
    NSString *documentsDirectory = [self documentsDirectoryPath];
    NSString *dbPath = [documentsDirectory stringByAppendingPathComponent:kNameDatabase];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isExisted = [fileManager fileExistsAtPath:dbPath];
    return  isExisted;
}



//------------------------------------------------------------------------------------------
/**   */
+(void)initializingDatabase
{
    NSDate *methodStart = [NSDate date];
    
    /* ... Do whatever you need to do ... */
    NSArray *listTableName = [self listTableNameInDatabase];
    
    NSString *documentsDirectory = [self documentsDirectoryPath];
    NSString *dbPath = [documentsDirectory stringByAppendingPathComponent:kNameDatabase];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    //check database can open or not
    if (![db open]) {
        NSLog(@"Could not open db.");
        
        return;
    }
    
    for (NSString *tableName in listTableName) {
        //--read text from file sql
        NSString *dbFile = [[NSBundle mainBundle] pathForResource:tableName ofType:@"sql"];
        NSString *sqlText = [NSString stringWithContentsOfFile:dbFile encoding:NSUTF8StringEncoding error:nil];
        NSArray *statementArray = [sqlText componentsSeparatedByString:@";\n"];
        
//        NSLog(@"%@ %lu", tableName, statementArray.count - 2);
        //create statement
        NSLog(@"%@",  [statementArray objectAtIndex:1]);
        NSString *createStatement =  [statementArray objectAtIndex:1];
        if (statementArray.count && createStatement.length) {
            [db executeUpdate: [statementArray objectAtIndex:1]];
            
            //insert statement
            [db beginTransaction];
            
            [statementArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                if (idx > 1) {
                    NSString *sqlStatement = obj;
                    sqlStatement = [sqlStatement stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                    [db executeUpdate:sqlStatement];
                }
            }];
            
            [db commit];
        }
    }
    
    [db close];
    
    
    NSDate *methodFinish = [NSDate date];
    NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
    NSLog(@"executionTime = %f", executionTime);
}
/*************************************************************************************************************************/


#pragma mark -
#pragma mark Check Platform
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
+(kTypePlatform) checkPlatform
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        return kPlatformIpad;
    else{
        
        CGSize sizeScreen  = [UIScreen mainScreen].bounds.size;
        //NSLog(@"size screen: %@", NSStringFromCGSize(sizeScreen));
        if (sizeScreen.height == 568.0f)
            return kPlatformNormallyiPhone5;
        else
            
            return kPlatformNormallyiPhone;
    }
    
    return kPlatformN_A;
}


//------------------------------------------------------------------------------------------
/**   */
+ (NSString *) getImageName:(NSString *) name
{
    if ([self checkPlatform] == kPlatformIpad)
    {
        name = [NSString stringWithFormat:@"iPad-%@", name];
    }
    
    if ([self checkPlatform] == kPlatformNormallyiPhone || [self checkPlatform] == kPlatformNormallyiPhone5) {
        name = [NSString stringWithFormat:@"iPhone-%@", name];
    }
    return name;
}


//------------------------------------------------------------------------------------------
/**   */
+(NSString *) checkDevice:(NSString *)viewFirstName{
    NSString *viewName=@"";
    kTypePlatform typePlatform = [Common checkPlatform];
    switch (typePlatform) {
        case kPlatformIpad:
        {
            viewName = [NSString stringWithFormat:@"iPad_%@", viewFirstName];
        }
            
            break;
        default:
            viewName =viewFirstName;
            break;
    }
    
    return viewName;
}
/*************************************************************************************************************************/



#pragma mark -
#pragma mark Setup Navigationbar & status bar
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
+(CGFloat)buildingStatusBarUsingNavBar: (BOOL) isUsingNavBar WithLabelText: (NSString *)labelText inView: (UIView *)view
{

    CGFloat yPoint = 0;
    if ([self isIOS7OrGreater]) {
        
        yPoint = 20;
        UIImage *statusBarBackgroundImage = [UIImage imageNamed:@"navBG2.png"];
        UIImageView *imageView_StatusBarBackground = [[UIImageView alloc] initWithImage:statusBarBackgroundImage];
        imageView_StatusBarBackground.frame = CGRectMake(0, 0, view.frame.size.width, yPoint);
        [view addSubview:imageView_StatusBarBackground];
    }
    
    if (isUsingNavBar) {
        UIImage *navBarBackground = [UIImage imageNamed:@"navBG.png"];
        UIImageView *imageView_NavBarBackground = [[UIImageView alloc] initWithImage:navBarBackground];
        imageView_NavBarBackground.frame = CGRectMake(0, yPoint, view.frame.size.width, 40);
        [view addSubview:imageView_NavBarBackground];
        
        UILabel *label = [[UILabel alloc] init];
        
        label.frame = CGRectMake(0, 0, view.frame.size.width - 90, imageView_NavBarBackground.frame.size.height);
        label.text = labelText;
        label.font = [UIFont systemFontOfSize:kFontSizeViewTitle];
        label.textAlignment = NSTextAlignmentCenter;
        label.backgroundColor = [UIColor clearColor];
        //[label sizeToFit];
        label.center = imageView_NavBarBackground.center;
        [view addSubview:label];
        
        yPoint += 40;
        
    }
    
    //return current position to arrange another views
    return yPoint;
}
/*************************************************************************************************************************/





#pragma mark -
#pragma mark AlertView Speech
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
+(void)showSpeechAlertViewWithSpeechText: (NSString *)speechText
{
    NSString *alertMessage;
    if ([self isIOS7OrGreater]) {
        alertMessage = kAlertSpeechMessage;
    }
    else
    {
        alertMessage = kAlertSpeechiOS6Message;
    }
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:kAlertSpeechTitle andMessage:alertMessage];
    
    [alertView addButtonWithTitle:kButtonNativeSpeechTitle type:SIAlertViewButtonTypeDefault handler:^(SIAlertView *alert) {
        
        [self usingNativeSpeechWithText:speechText] ;
    }];
    [alertView addButtonWithTitle:kButtoniSpeechTitle type:SIAlertViewButtonTypeDefault  handler:^(SIAlertView *alert) {
        [self usingiSpeechWithText:speechText]; }];
    [alertView addButtonWithTitle:kButtonCancelTitle type:SIAlertViewButtonTypeDestructive handler:nil];
    
    
    alertView.transitionStyle = SIAlertViewTransitionStyleSlideFromBottom;
    
    [alertView show];

}


//------------------------------------------------------------------------------------------
/** Speech using iSpeech API  */
+(void)usingiSpeechWithText: (NSString *)speechText
{
    
    ISSpeechSynthesis *synthesis = [[ISSpeechSynthesis alloc] initWithText:speechText];
    
    NSError *err;
    
    if(![synthesis speak:&err]) {
        //NSLog(@"ERROR: %@", err);
    }
    
}

//------------------------------------------------------------------------------------------
/** Speech using AVSpeechSynthesizer (Native) */
+(void)usingNativeSpeechWithText: (NSString *)speechText
{
    if ([self isIOS7OrGreater])
    {
        if([AVSpeechSynthesizer class]) {
            AVSpeechUtterance *utterance = [[AVSpeechUtterance alloc] initWithString:speechText];
            utterance.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"en-US"];
            utterance.rate = 0.2;
            AVSpeechSynthesizer *speechSynthesizer = [[AVSpeechSynthesizer alloc] init];
            [speechSynthesizer speakUtterance:utterance];
            
        }

    }
    else
    {
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:kAlertSpeechTitle andMessage:kAlertSpeechiOS6Message];
        
        [alertView addButtonWithTitle:kButtonCancelTitle type:SIAlertViewButtonTypeDestructive handler:nil];
        alertView.transitionStyle = SIAlertViewTransitionStyleFade;
        
        [alertView show];
    }
    
}



/*************************************************************************************************************************/




#pragma mark -
#pragma mark AlertView Copy
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**  Show copy options */
+(void)showCopyAlertViewWithENSentence: (NSString *)enSentence VISentence: (NSString *)viSentence
{

    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:kAlertCopyTitle andMessage:kAlertCopyMessage];
    
    //All sentence
    [alertView addButtonWithTitle:kButtonAllSentenceTitle type:SIAlertViewButtonTypeDefault handler:^(SIAlertView *alert) {
        NSString *copyString = [enSentence stringByAppendingString: [NSString stringWithFormat:@"\n%@", viSentence]];
        [self copyToClipboardWithString: copyString];
        
    }];
    
    //EN sentence
    [alertView addButtonWithTitle:kButtonENSentenceTitle type:SIAlertViewButtonTypeDefault  handler:^(SIAlertView *alert) {
        [self copyToClipboardWithString: enSentence];
    }];
    
    //Vi Sentence
    [alertView addButtonWithTitle:kButtonVISentenceTitle type:SIAlertViewButtonTypeDefault  handler:^(SIAlertView *alert) {
         [self copyToClipboardWithString: viSentence];
    }];
    
    //Cancel
    [alertView addButtonWithTitle:kButtonCancelTitle type:SIAlertViewButtonTypeDestructive handler:nil];
    
    alertView.transitionStyle = SIAlertViewTransitionStyleSlideFromBottom;
    [alertView show];
    
}


//------------------------------------------------------------------------------------------
/** Copy string to clipboard  */
+(void)copyToClipboardWithString: (NSString *)copyString
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = copyString;
}




/*************************************************************************************************************************/



#pragma mark -
#pragma mark Utilities
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
+(void)dismissKeyboard
{
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}



//------------------------------------------------------------------------------------------
/**   */
+(BOOL)isContainNonEnglishCharacter: (NSString *)checkedString
{
    //    NSCharacterSet *alphaSet = [NSCharacterSet alphanumericCharacterSet];
    //    BOOL valid = [[checkedString stringByTrimmingCharactersInSet:alphaSet] isEqualToString:@""];
    //    return valid;
    //
    //    NSRegularExpression *regex = [[NSRegularExpression alloc]
    //                                   initWithPattern:@"[a-zA-Z]" options:0 error:NULL];
    //
    //    // Assuming you have some NSString `myString`.
    //    NSUInteger matches = [regex numberOfMatchesInString:checkedString options:0
    //                                                  range:NSMakeRange(0, [checkedString length])];
    //
    //    if (matches > 0) {
    //        // `myString` contains at least one English letter.
    //    }
    
    return [checkedString canBeConvertedToEncoding:NSASCIIStringEncoding];
}


//------------------------------------------------------------------------------------------
/**   */
+(BOOL)isNonNumericWithString: (NSString *)searchKey
{
    NSCharacterSet *alphanumericSet = [NSCharacterSet alphanumericCharacterSet];
    NSCharacterSet *numberSet = [NSCharacterSet decimalDigitCharacterSet];
    BOOL isAplhaNumericOnly= [[searchKey stringByTrimmingCharactersInSet:alphanumericSet] isEqualToString:@""] && ![[searchKey stringByTrimmingCharactersInSet:numberSet] isEqualToString:@""];
    return isAplhaNumericOnly;
}

//------------------------------------------------------------------------------------------
/**   */
+(NSString *) stringByStrippingHTMLFromString: (NSString *)string {
    NSRange r;
    NSString *s = string;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

//------------------------------------------------------------------------------------------
/**   */
+ (NSString *)languageForString:(NSString *) text{
    
    
    if (text.length < 100) {
        
        return (NSString *)CFBridgingRelease(CFStringTokenizerCopyBestStringLanguage((CFStringRef)text, CFRangeMake(0, text.length)));
    } else {
        
        return (NSString *)CFBridgingRelease(CFStringTokenizerCopyBestStringLanguage((CFStringRef)text, CFRangeMake(0, 100)));
    }
    
}



//------------------------------------------------------------------------------------------
/**   */
+ (BOOL) isIOS7OrGreater;
{
    /* [[UIDevice currentDevice] systemVersion] returns "4.0", "3.1.3" and so on. */
    NSString* ver = [[UIDevice currentDevice] systemVersion];
    //    NSLog(@"Check ios410 %@",ver);
    /* I assume that the default iOS version will be 4.0, that is why I set this to 4.0 */
    float version = 7.0;
    
    if ([ver length]>=3)
    {
        /*
         The version string has the format major.minor.revision (eg. "3.1.3").
         I am not interested in the revision part of the version string, so I can do this.
         It will return the float value for "3.1", because substringToIndex is called first.
         */
        version = [[ver substringToIndex:3] floatValue];
    }
    return (version >= 7.0);
}


//------------------------------------------------------------------------------------------
/**   */
+(NSString *)convertToLocalFormatDate: (NSDate *)dateTime
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    NSString *formatedDateString = [dateFormatter stringFromDate: dateTime];
    dateFormatter = nil;
    
    return formatedDateString;
}


//------------------------------------------------------------------------------------------
/**   */
+(UIColor *)mainThemeColor
{
    return [UIColor colorWithRed:200/255.0f green:69/255.0f blue:48/255.0f alpha:1];
}
/*************************************************************************************************************************/



#pragma mark -
#pragma mark ProgressView

/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/** Show progress view  */
+(void)showProgressViewWithStatus: (NSString *)status
{

    [MBProgressHUD showHUDWithStatus:status];
}

//------------------------------------------------------------------------------------------
/**  Dismiss progress view */
+(void)dismissProgressView
{
    [MBProgressHUD hideHUD];
}

/*************************************************************************************************************************/


#pragma mark -
#pragma mark
/*************************************************************************************************************************/
+(BOOL)addSkipBackupAttributeToItemAtPath: (NSString *)path
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: path]);
    NSURL *urlFromPath = [NSURL URLWithString:path];
    NSError *error = nil;
    BOOL success = [urlFromPath setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [urlFromPath lastPathComponent], error);
    }
    return success;
}


/*************************************************************************************************************************/


#pragma mark -
#pragma mark Admobs
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
+(GADBannerView *)createGAView:(id)targetViewController
{
    GADBannerView *bannerView_;
    if ([Common checkPlatform] == kPlatformIpad) {
        bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeLeaderboard];
    }
    else
    {
        bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    }
    
    // Specify the ad unit ID.
    bannerView_.adUnitID = kGAUnitId;

    // Let the runtime know which UIViewController to restore after taking
    // the user wherever the ad goes and add it to the view hierarchy.
    bannerView_.rootViewController = targetViewController;

    
    // Initiate a generic request to load it with an ad.
    [bannerView_ setDelegate:targetViewController];
    [bannerView_ loadRequest:[GADRequest request]];
    
    return bannerView_;
}
/*************************************************************************************************************************/

@end
