//
//  Common.h
//  TraCau
//
//  Created by Mr.NGUYEN on 6/9/14.
//  Copyright (c) 2014 Mr.NGUYEN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SIAlertView.h"
#import "GADBannerView.h"
typedef enum {
    kPlatformIpad = 0,
    kPlatformNormallyiPhone = 1,
    kPlatformNormallyiPhone5 = 2,
    kPlatformN_A = -1,
} kTypePlatform;


@interface Common : NSObject

#pragma mark -
#pragma mark Initializing database
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
+(BOOL)isDatabaseExisted;

//------------------------------------------------------------------------------------------
/**   */
+(void)initializingDatabase;
/*************************************************************************************************************************/



#pragma mark -
#pragma mark Check Platform
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**   */
+(kTypePlatform) checkPlatform;

//------------------------------------------------------------------------------------------
/**   */
+ (NSString *) getImageName:(NSString *) name;

//------------------------------------------------------------------------------------------
/**   */
+(NSString *) checkDevice:(NSString *)viewFirstName;
/*************************************************************************************************************************/




//------------------------------------------------------------------------------------------
/** Set up navigation bar for view  */
+(CGFloat)buildingStatusBarUsingNavBar: (BOOL) isUsingNavBar WithLabelText: (NSString *)labelText inView: (UIView *)view;



#pragma mark -
#pragma mark AlertView Speech
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/** Show alert view with options for speech  */
+(void)showSpeechAlertViewWithSpeechText: (NSString *)speechText;
/*************************************************************************************************************************/


#pragma mark -
#pragma mark AlertView Copy
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/**  Show copy options */
+(void)showCopyAlertViewWithENSentence: (NSString *)enSentence VISentence: (NSString *)viSentence;

/*************************************************************************************************************************/



#pragma mark -
#pragma mark Utilities
/*************************************************************************************************************************/
//------------------------------------------------------------------------------------------
/** Dismiss keyboard from any view  */
+(void)dismissKeyboard;

//------------------------------------------------------------------------------------------
/** Check  string is numeric or not */
+(BOOL)isNonNumericWithString: (NSString *)searchKey;

//------------------------------------------------------------------------------------------
/** Check string is contain non english character or not  */
+(BOOL)isContainNonEnglishCharacter: (NSString *)checkedString;

//------------------------------------------------------------------------------------------
/**  Stripping HTML From String */
+(NSString *) stringByStrippingHTMLFromString: (NSString *)string;

//------------------------------------------------------------------------------------------
/** Detect language of string  */
+ (NSString *)languageForString:(NSString *) text;

//------------------------------------------------------------------------------------------
/**   */
+ (BOOL) isIOS7OrGreater;


//------------------------------------------------------------------------------------------
/**  Format NSDate to format dd.MM.yyyy */
+(NSString *)convertToLocalFormatDate: (NSDate *)dateTime;


//------------------------------------------------------------------------------------------
/**  Return main theme app color */
+(UIColor *)mainThemeColor;

//------------------------------------------------------------------------------------------
/**   */
+(BOOL)addSkipBackupAttributeToItemAtPath: (NSString *)path;
/*************************************************************************************************************************/


#pragma mark -
#pragma mark ProgressView

/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/** Show progress view  */
+(void)showProgressViewWithStatus: (NSString *)status;

//------------------------------------------------------------------------------------------
/**  Dismiss progress view */
+(void)dismissProgressView;

/*************************************************************************************************************************/

//------------------------------------------------------------------------------------------
/**   */
+(GADBannerView *)createGAView:(id)targetViewController;

@end
