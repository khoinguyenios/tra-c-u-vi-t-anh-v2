DROP TABLE IF EXISTS "words_1EC5";
CREATE TABLE "words_1EC5" ("word" TEXT, "content" TEXT, "word_id" TEXT);
INSERT INTO "words_1EC5" VALUES('ễnh','<span class="title">ễnh</span><br /><span class="type">verb</span><ul><li>to swell<ul><li><span class="example"><a href="entry://ăn" class="aexample">ăn</a> <a href="entry://nhiều" class="aexample">nhiều</a> <a href="entry://quá" class="aexample">quá</a> <a href="entry://ễnh" class="aexample">ễnh</a> <a href="entry://bụng" class="aexample">bụng</a>:</span><span class="mexample">to have a swelling belly for having eaten too much food</span></li></ul></li></ul>',NULL);
INSERT INTO "words_1EC5" VALUES('ễnh ương','<span class="title">ễnh ương</span><br /><span class="type">noun</span><ul><li>bull-frog</li></ul>',NULL);
