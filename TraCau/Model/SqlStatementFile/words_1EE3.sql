DROP TABLE IF EXISTS "words_1EE3";
CREATE TABLE "words_1EE3" ("word" TEXT, "content" TEXT, "word_id" TEXT);
INSERT INTO "words_1EE3" VALUES('16973','ợ','<span class="title">ợ</span><br /><span class="type">verb</span><ul><li>to belch; to eruct</li></ul>');
INSERT INTO "words_1EE3" VALUES('16987','ợt','<span class="title">ợt</span><br /><ul><li>Like child"s play<ul><li><span class="example"><a href="entry://Dễ" class="aexample">Dễ</a> <a href="entry://ợt" class="aexample">ợt</a>:</span><span class="mexample">Easy as child"s play</span></li></ul></li></ul>');
INSERT INTO "words_1EE3" VALUES('31162','ợ nóng','<span class="title">ợ nóng [ợ nóng]</span><br /><ul><li>heartburn</li></ul><span class="title">Chuyên ngành kỹ thuật</span><br /><span class="title">Lĩnh vực: y học</span><br /><ul><li>brash</li></ul>');
INSERT INTO "words_1EE3" VALUES('285790','ợ chua','<span class="title">Chuyên ngành kỹ thuật</span><br /><span class="title">Lĩnh vực: y học</span><br /><ul><li>oxyrygmia</li></ul>');
INSERT INTO "words_1EE3" VALUES('285791','ợ, ợ hơi','<span class="title">Chuyên ngành kỹ thuật</span><br /><span class="title">Lĩnh vực: y học</span><br /><ul><li>ructus</li></ul>');
