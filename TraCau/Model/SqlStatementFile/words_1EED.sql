DROP TABLE IF EXISTS "words_1EED";
CREATE TABLE "words_1EED" ("word" TEXT, "content" TEXT, "word_id" TEXT);
INSERT INTO "words_1EED" VALUES('ửng hồng','<span class="title">ửng hồng</span><br /><span class="type">verb</span><ul><li>to redden, to turn pink</li></ul>',NULL);
INSERT INTO "words_1EED" VALUES('ửng','<span class="title">ửng</span><br /><span class="type">tính từ</span><ul><li>redden, tinge with red, grow red colour, blush</li></ul>',NULL);
INSERT INTO "words_1EED" VALUES('ửng đỏ','<span class="title">ửng đỏ</span><br /><span class="type">động từ</span><ul><li>blush</li></ul><span class="title">Chuyên ngành kỹ thuật</span><br /><span class="title">Lĩnh vực: y học</span><br /><ul><li>suffusion</li></ul>',NULL);
