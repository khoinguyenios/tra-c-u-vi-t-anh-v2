DROP TABLE IF EXISTS "words_1EF7";
CREATE TABLE "words_1EF7" ("word" TEXT, "content" TEXT, "word_id" TEXT);
INSERT INTO "words_1EF7" VALUES('ỷ','<span class="title">ỷ</span><br /><span class="type">verb</span><ul><li>to depend on</li></ul>',NULL);
INSERT INTO "words_1EF7" VALUES('ỷ eo','<span class="title">ỷ eo</span><br /><span class="type">động từ</span><ul><li>reproach someone with something</li></ul>',NULL);
INSERT INTO "words_1EF7" VALUES('ỷ lại','<span class="title">ỷ lại</span><br /><span class="type">động từ</span><ul><li>rely on others</li></ul>',NULL);
INSERT INTO "words_1EF7" VALUES('ỷ thế','<span class="title">ỷ thế</span><br /><span class="type">động từ</span><ul><li>count on one"s power, one"s position, one"s influence</li></ul>',NULL);
