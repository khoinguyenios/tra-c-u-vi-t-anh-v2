DROP TABLE IF EXISTS "words_1EB9";
CREATE TABLE "words_1EB9" ("word" TEXT, "content" TEXT, "word_id" TEXT);
INSERT INTO "words_1EB9" VALUES('ẹo','<span class="title">ẹo</span><br /><span class="type">verb</span><ul><li>to twist, to bend</li></ul>',NULL);
INSERT INTO "words_1EB9" VALUES('ẹp','<span class="title">ẹp</span><br /><span class="type">verb</span><ul><li>to deflate, to flatten</li></ul>',NULL);
INSERT INTO "words_1EB9" VALUES('ẹo lưng','<span class="title">ẹo lưng [ẹo lưng]</span><br /><ul><li>Bent back.</li></ul>',NULL);
