DROP TABLE IF EXISTS "pronoun";
CREATE TABLE "pronoun" ("pronoun_id" TEXT PRIMARY KEY ,"pronoun" text);
INSERT INTO "pronoun" VALUES('1','I');
INSERT INTO "pronoun" VALUES('2','you');
INSERT INTO "pronoun" VALUES('3','he');
INSERT INTO "pronoun" VALUES('4','we');
INSERT INTO "pronoun" VALUES('5','you');
INSERT INTO "pronoun" VALUES('6','they');
INSERT INTO "pronoun" VALUES('7','');
