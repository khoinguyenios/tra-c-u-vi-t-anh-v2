DROP TABLE IF EXISTS "words_0129";
CREATE TABLE "words_0129" ("word" TEXT, "content" TEXT, "word_id" TEXT);
INSERT INTO "words_0129" VALUES('ĩnh','<span class="title">ĩnh [ĩnh]</span><br /><ul><li>swell out, puff up</li></ul>',NULL);
INSERT INTO "words_0129" VALUES('ĩnh bụng','<span class="title">ĩnh bụng [ĩnh bụng]</span><br /><ul><li>pregnant</li><li>a pot belly</li></ul>',NULL);
INSERT INTO "words_0129" VALUES('ĩnh ương','<span class="title">ĩnh ương [ĩnh ương]</span><br /><ul><li>bullfrog</li></ul>',NULL);
