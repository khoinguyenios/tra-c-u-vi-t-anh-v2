DROP TABLE IF EXISTS "words_1EEB";
CREATE TABLE "words_1EEB" ("word" TEXT, "content" TEXT, "word_id" TEXT);
INSERT INTO "words_1EEB" VALUES('ừ','<span class="title">ừ [ừ',NULL);
INSERT INTO "words_1EEB" VALUES('ừ ào','<span class="title">ừ ào - say "yes" and not mean it</span><br />',NULL);
INSERT INTO "words_1EEB" VALUES('ừ hữ','<span class="title">ừ hữ - answer evasively</span><br />',NULL);
INSERT INTO "words_1EEB" VALUES('ừ ừ ào ào','<span class="title">ừ ừ ào ào - say yes to everything and do nothing</span><br />',NULL);
INSERT INTO "words_1EEB" VALUES('ừng ực','<span class="title">ừng ực</span><br /><span class="type">động từ</span><ul><li>gulp down, swallow</li></ul>',NULL);
