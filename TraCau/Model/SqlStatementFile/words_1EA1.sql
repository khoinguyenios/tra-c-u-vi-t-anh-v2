DROP TABLE IF EXISTS "words_1EA1";
CREATE TABLE "words_1EA1" ("word" TEXT, "content" TEXT, "word_id" TEXT);
INSERT INTO "words_1EA1" VALUES('ạ','<span class="title">ạ</span><br /><span class="type">từ đệm</span><ul><li><span class="example"><a href="entry://Vâng" class="aexample">Vâng</a> <a href="entry://ạ" class="aexample">ạ</a>:</span><span class="mexample">yes, sir</span></li></ul>',NULL);
INSERT INTO "words_1EA1" VALUES('ạch','<span class="title">ạch</span><br /><span class="type">noun</span><ul><li>flump; flop<ul><li><span class="example"><a href="entry://té" class="aexample">té</a> <a href="entry://cái" class="aexample">cái</a> <a href="entry://ạch" class="aexample">ạch</a>:</span><span class="mexample">to fall flop</span></li></ul></li></ul>',NULL);
INSERT INTO "words_1EA1" VALUES('ạo ực','<span class="title">ạo ực</span><br /><span class="type">danh từ</span><ul><li>hallucination; have qualms</li></ul>',NULL);
