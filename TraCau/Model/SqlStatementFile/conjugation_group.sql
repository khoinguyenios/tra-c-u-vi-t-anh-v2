DROP TABLE IF EXISTS "conjugation_group";
CREATE TABLE "conjugation_group" ("conjugation_group_id" TEXT,"conjugation_group_name" TEXT);
INSERT INTO "conjugation_group" VALUES(NULL,'indicative');
INSERT INTO "conjugation_group" VALUES(NULL,'subjunctive');
INSERT INTO "conjugation_group" VALUES(NULL,'conditional');
INSERT INTO "conjugation_group" VALUES(NULL,'other');
