


////conjugation_groupField.h 
//  Project 
// 
// Created by TeamiOS on 2014-06-19 03:02:46 +0000. 
//
// 

#import <Foundation/Foundation.h>

@interface conjugation_groupField : NSObject
{
	NSString *rowid;
	NSString *conjugation_group_id;
	NSString *conjugation_group_name;
}

@property (nonatomic, retain) NSString *rowid;
@property (nonatomic, retain) NSString *conjugation_group_id;
@property (nonatomic, retain) NSString *conjugation_group_name;
 

@end