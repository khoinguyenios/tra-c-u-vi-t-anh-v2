

////conjugation_groupField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-06-19 03:02:46 +0000. 
//
// 

#import "conjugation_groupField.h"

@implementation conjugation_groupField
@synthesize rowid;
@synthesize conjugation_group_id;
@synthesize conjugation_group_name;


-(void) dealloc
{
	[rowid release];
	[conjugation_group_id  release];
	[conjugation_group_name  release];

	[super  dealloc];
}

@end