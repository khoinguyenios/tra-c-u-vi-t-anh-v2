


////wordsField.h 
//  Project 
// 
// Created by TeamiOS on 2014-06-09 07:49:43 +0000. 
//
// 

#import <Foundation/Foundation.h>

@interface wordsField : NSObject
{
	NSString *rowid;
	NSString *word;
	NSString *content;
	NSString *word_id;
}

@property (nonatomic, retain) NSString *rowid;
@property (nonatomic, retain) NSString *word;
@property (nonatomic, retain) NSString *content;
@property (nonatomic, retain) NSString *word_id;
 

@end