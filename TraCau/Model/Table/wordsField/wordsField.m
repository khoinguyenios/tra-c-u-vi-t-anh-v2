

////wordsField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-06-09 07:49:43 +0000. 
//
// 

#import "wordsField.h"

@implementation wordsField
@synthesize rowid;
@synthesize word;
@synthesize content;
@synthesize word_id;


-(void) dealloc
{
	[rowid release];
	[word  release];
	[content  release];
	[word_id  release];

	[super  dealloc];
}

@end