


////conjugation_typeField.h 
//  Project 
// 
// Created by TeamiOS on 2014-06-19 03:04:15 +0000. 
//
// 

#import <Foundation/Foundation.h>

@interface conjugation_typeField : NSObject
{
	NSString *rowid;
	NSString *conjugation_type_id;
	NSString *type;
	NSString *description;
	NSString *conjugation_group_id;
}

@property (nonatomic, retain) NSString *rowid;
@property (nonatomic, retain) NSString *conjugation_type_id;
@property (nonatomic, retain) NSString *type;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, retain) NSString *conjugation_group_id;
 

@end