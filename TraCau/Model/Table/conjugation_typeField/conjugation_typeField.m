

////conjugation_typeField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-06-19 03:04:15 +0000. 
//
// 

#import "conjugation_typeField.h"

@implementation conjugation_typeField
@synthesize rowid;
@synthesize conjugation_type_id;
@synthesize type;
@synthesize description;
@synthesize conjugation_group_id;


-(void) dealloc
{
	[rowid release];
	[conjugation_type_id  release];
	[type  release];
	[description  release];
	[conjugation_group_id  release];

	[super  dealloc];
}

@end