


////pattern_categoryField.h 
//  Project 
// 
// Created by TeamiOS on 2014-06-09 07:46:59 +0000. 
//
// 

#import <Foundation/Foundation.h>

@interface pattern_categoryField : NSObject
{
	NSString *rowid;
	NSString *pattern_category_id;
	NSString *name;
	NSString *sequence;
	NSString *name_ansi;
}

@property (nonatomic, retain) NSString *rowid;
@property (nonatomic, retain) NSString *pattern_category_id;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *sequence;
@property (nonatomic, retain) NSString *name_ansi;
 

@end