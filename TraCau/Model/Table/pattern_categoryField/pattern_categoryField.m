

////pattern_categoryField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-06-09 07:46:59 +0000. 
//
// 

#import "pattern_categoryField.h"

@implementation pattern_categoryField
@synthesize rowid;
@synthesize pattern_category_id;
@synthesize name;
@synthesize sequence;
@synthesize name_ansi;


-(void) dealloc
{
	[rowid release];
	[pattern_category_id  release];
	[name  release];
	[sequence  release];
	[name_ansi  release];

	[super  dealloc];
}

@end