

////patternField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-06-09 07:46:31 +0000. 
//
// 

#import "patternField.h"

@implementation patternField
@synthesize rowid;
@synthesize pattern_id;
@synthesize category_id;
@synthesize sequence;
@synthesize sentence_vi;
@synthesize sentence_en;


-(void) dealloc
{
	[rowid release];
	[pattern_id  release];
	[category_id  release];
	[sequence  release];
	[sentence_vi  release];
	[sentence_en  release];

	[super  dealloc];
}

@end