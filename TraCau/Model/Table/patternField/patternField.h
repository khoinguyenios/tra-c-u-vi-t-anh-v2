


////patternField.h 
//  Project 
// 
// Created by TeamiOS on 2014-06-09 07:46:31 +0000. 
//
// 

#import <Foundation/Foundation.h>

@interface patternField : NSObject
{
	NSString *rowid;
	NSString *pattern_id;
	NSString *category_id;
	NSString *sequence;
	NSString *sentence_vi;
	NSString *sentence_en;
}

@property (nonatomic, retain) NSString *rowid;
@property (nonatomic, retain) NSString *pattern_id;
@property (nonatomic, retain) NSString *category_id;
@property (nonatomic, retain) NSString *sequence;
@property (nonatomic, retain) NSString *sentence_vi;
@property (nonatomic, retain) NSString *sentence_en;
 

@end