

////favorite_sentencesField.m
//  Project
//
//  Created by TeamiOS on 2014-06-11 03:49:46 +0000.
//
//

#import "favorite_sentencesField.h"

@implementation favorite_sentencesField
@synthesize rowid;
@synthesize sentence_id;
@synthesize root_id;
@synthesize sentence_vi;
@synthesize sentence_en;
@synthesize sentence_type;


-(void) dealloc
{
	[rowid release];
	[sentence_id  release];
	[root_id  release];
	[sentence_vi  release];
	[sentence_en  release];
	[sentence_type  release];
    
	[super  dealloc];
}

@end