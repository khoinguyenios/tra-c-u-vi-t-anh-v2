


////favorite_sentencesField.h
//  Project
//
// Created by TeamiOS on 2014-06-11 03:49:46 +0000.
//
//

#import <Foundation/Foundation.h>

@interface favorite_sentencesField : NSObject
{
	NSString *rowid;
	NSString *sentence_id;
	NSString *root_id;
	NSString *sentence_vi;
	NSString *sentence_en;
	NSString *sentence_type;
}

@property (nonatomic, retain) NSString *rowid;
@property (nonatomic, retain) NSString *sentence_id;
@property (nonatomic, retain) NSString *root_id;
@property (nonatomic, retain) NSString *sentence_vi;
@property (nonatomic, retain) NSString *sentence_en;
@property (nonatomic, retain) NSString *sentence_type;


@end