

////verbField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-06-09 07:48:47 +0000. 
//
// 

#import "verbField.h"

@implementation verbField
@synthesize rowid;
@synthesize verb_id;
@synthesize english;
@synthesize italian;
@synthesize simplified_italian;
@synthesize portuguese;
@synthesize simplified_portuguese;
@synthesize spanish;
@synthesize simplified_spanish;
@synthesize french;
@synthesize simplified_french;
@synthesize german;
@synthesize simplified_german;
@synthesize dutch;
@synthesize simplified_dutch;


-(void) dealloc
{
	[rowid release];
	[verb_id  release];
	[english  release];
	[italian  release];
	[simplified_italian  release];
	[portuguese  release];
	[simplified_portuguese  release];
	[spanish  release];
	[simplified_spanish  release];
	[french  release];
	[simplified_french  release];
	[german  release];
	[simplified_german  release];
	[dutch  release];
	[simplified_dutch  release];

	[super  dealloc];
}

@end