


////verbField.h 
//  Project 
// 
// Created by TeamiOS on 2014-06-09 07:48:47 +0000. 
//
// 

#import <Foundation/Foundation.h>

@interface verbField : NSObject
{
	NSString *rowid;
	NSString *verb_id;
	NSString *english;
	NSString *italian;
	NSString *simplified_italian;
	NSString *portuguese;
	NSString *simplified_portuguese;
	NSString *spanish;
	NSString *simplified_spanish;
	NSString *french;
	NSString *simplified_french;
	NSString *german;
	NSString *simplified_german;
	NSString *dutch;
	NSString *simplified_dutch;
}

@property (nonatomic, retain) NSString *rowid;
@property (nonatomic, retain) NSString *verb_id;
@property (nonatomic, retain) NSString *english;
@property (nonatomic, retain) NSString *italian;
@property (nonatomic, retain) NSString *simplified_italian;
@property (nonatomic, retain) NSString *portuguese;
@property (nonatomic, retain) NSString *simplified_portuguese;
@property (nonatomic, retain) NSString *spanish;
@property (nonatomic, retain) NSString *simplified_spanish;
@property (nonatomic, retain) NSString *french;
@property (nonatomic, retain) NSString *simplified_french;
@property (nonatomic, retain) NSString *german;
@property (nonatomic, retain) NSString *simplified_german;
@property (nonatomic, retain) NSString *dutch;
@property (nonatomic, retain) NSString *simplified_dutch;
 

@end