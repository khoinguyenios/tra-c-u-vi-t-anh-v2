


////favorite_rootField.h 
//  Project 
// 
// Created by TeamiOS on 2014-06-09 07:44:00 +0000. 
//
// 

#import <Foundation/Foundation.h>

@interface favorite_rootField : NSObject
{
	NSString *rowid;
	NSString *root_id;
	NSString *root_name;
}

@property (nonatomic, retain) NSString *rowid;
@property (nonatomic, retain) NSString *root_id;
@property (nonatomic, retain) NSString *root_name;
 

@end