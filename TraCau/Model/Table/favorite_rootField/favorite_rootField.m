

////favorite_rootField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-06-09 07:44:00 +0000. 
//
// 

#import "favorite_rootField.h"

@implementation favorite_rootField
@synthesize rowid;
@synthesize root_id;
@synthesize root_name;


-(void) dealloc
{
	[rowid release];
	[root_id  release];
	[root_name  release];

	[super  dealloc];
}

@end