

////search_historyField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-06-19 08:22:00 +0000. 
//
// 

#import "search_historyField.h"

@implementation search_historyField
@synthesize rowid;
@synthesize search_date_id;
@synthesize search_word;
@synthesize search_content;


-(void) dealloc
{
	[rowid release];
	[search_date_id  release];
	[search_word  release];
	[search_content  release];

	[super  dealloc];
}

@end