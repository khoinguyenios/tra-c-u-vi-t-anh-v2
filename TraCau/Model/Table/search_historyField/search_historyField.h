


////search_historyField.h 
//  Project 
// 
// Created by TeamiOS on 2014-06-19 08:22:00 +0000. 
//
// 

#import <Foundation/Foundation.h>

@interface search_historyField : NSObject
{
	NSString *rowid;
	NSString *search_date_id;
	NSString *search_word;
	NSString *search_content;
}

@property (nonatomic, retain) NSString *rowid;
@property (nonatomic, retain) NSString *search_date_id;
@property (nonatomic, retain) NSString *search_word;
@property (nonatomic, retain) NSString *search_content;
 

@end