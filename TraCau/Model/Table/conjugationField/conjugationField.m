

////conjugationField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-06-09 07:42:26 +0000. 
//
// 

#import "conjugationField.h"

@implementation conjugationField
@synthesize rowid;
@synthesize conjugation_id;
@synthesize verb;
@synthesize verbid;
@synthesize pronounid;
@synthesize conjugation_type_id;
@synthesize not_used;
@synthesize orto;
@synthesize irregular;


-(void) dealloc
{
	[rowid release];
	[conjugation_id  release];
	[verb  release];
	[verbid  release];
	[pronounid  release];
	[conjugation_type_id  release];
	[not_used  release];
	[orto  release];
	[irregular  release];

	[super  dealloc];
}

@end