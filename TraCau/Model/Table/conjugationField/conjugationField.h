


////conjugationField.h 
//  Project 
// 
// Created by TeamiOS on 2014-06-09 07:42:26 +0000. 
//
// 

#import <Foundation/Foundation.h>

@interface conjugationField : NSObject
{
	NSString *rowid;
	NSString *conjugation_id;
	NSString *verb;
	NSString *verbid;
	NSString *pronounid;
	NSString *conjugation_type_id;
	NSString *not_used;
	NSString *orto;
	NSString *irregular;
}

@property (nonatomic, retain) NSString *rowid;
@property (nonatomic, retain) NSString *conjugation_id;
@property (nonatomic, retain) NSString *verb;
@property (nonatomic, retain) NSString *verbid;
@property (nonatomic, retain) NSString *pronounid;
@property (nonatomic, retain) NSString *conjugation_type_id;
@property (nonatomic, retain) NSString *not_used;
@property (nonatomic, retain) NSString *orto;
@property (nonatomic, retain) NSString *irregular;
 

@end