


////pronounField.h 
//  Project 
// 
// Created by TeamiOS on 2014-06-09 07:47:37 +0000. 
//
// 

#import <Foundation/Foundation.h>

@interface pronounField : NSObject
{
	NSString *rowid;
	NSString *pronoun_id;
	NSString *pronoun;
}

@property (nonatomic, retain) NSString *rowid;
@property (nonatomic, retain) NSString *pronoun_id;
@property (nonatomic, retain) NSString *pronoun;
 

@end