

////pronounField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-06-09 07:47:37 +0000. 
//
// 

#import "pronounField.h"

@implementation pronounField
@synthesize rowid;
@synthesize pronoun_id;
@synthesize pronoun;


-(void) dealloc
{
	[rowid release];
	[pronoun_id  release];
	[pronoun  release];

	[super  dealloc];
}

@end