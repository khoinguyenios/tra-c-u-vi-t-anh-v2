

////pattern_categoryField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-06-09 07:46:59 +0000. 
//

//--CREATE TABLE "pattern_category" ("pattern_category_id" TEXT, "name" TEXT, "sequence" TEXT, "name_ansi" TEXT, )

#import "pattern_categoryModel.h"

@implementation pattern_categoryModel

/** init class */
-(id) init
{
	self = [super init];

	if (self)
	{
		self.usesTable = @"pattern_category";
	}
	return self;
}


/* free memory */
-(void) dealloc
{
	[super dealloc];
}



/********************************************************************************/
#pragma mark  - Insert Row To Table

-(BOOL)createRowInTablepattern_category:(pattern_categoryField *) data
{
	NSString *pattern_category_id = data.pattern_category_id;
	NSString *name = data.name;
	NSString *sequence = data.sequence;
	NSString *name_ansi = data.name_ansi;

	NSString *pattern_category_idInit = @"";
	NSString *nameInit = @"";
	NSString *sequenceInit = @"";
	NSString *name_ansiInit = @"";


	if (pattern_category_id)
		pattern_category_idInit = pattern_category_id;

	if (name)
		nameInit = name;

	if (sequence)
		sequenceInit = sequence;

	if (name_ansi)
		name_ansiInit = name_ansi;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		const char *sql = "INSERT INTO \"pattern_category\" (\"pattern_category_id\",\"name\",\"sequence\",\"name_ansi\") VALUES (?1,?2,?3,?4)";

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--pattern_category_id
	sqlite3_bind_text(createStmt, 1, [pattern_category_idInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--name
	sqlite3_bind_text(createStmt, 2, [nameInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--sequence
	sqlite3_bind_text(createStmt, 3, [sequenceInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--name_ansi
	sqlite3_bind_text(createStmt, 4, [name_ansiInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Update Row To Table

-(BOOL)updateRowInTablepattern_category:(pattern_categoryField *) data
{

	NSString *pattern_category_id = data.pattern_category_id;
	NSString *name = data.name;
	NSString *sequence = data.sequence;
	NSString *name_ansi = data.name_ansi;

	NSString *pattern_category_idInit = @"";
	NSString *nameInit = @"";
	NSString *sequenceInit = @"";
	NSString *name_ansiInit = @"";


	if (pattern_category_id)
		pattern_category_idInit = pattern_category_id;

	if (name)
		nameInit = name;

	if (sequence)
		sequenceInit = sequence;

	if (name_ansi)
		name_ansiInit = name_ansi;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		NSString *sqlOriginal = [NSString stringWithFormat:@"UPDATE \"pattern_category\" SET \"pattern_category_id\" = ?, \"name\" = ?, \"sequence\" = ?, \"name_ansi\" = ? WHERE rowid = %@", data.rowid];

		const char *sql = sqlOriginal.UTF8String;

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--pattern_category_id
	sqlite3_bind_text(createStmt, 1, [pattern_category_idInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--name
	sqlite3_bind_text(createStmt, 2, [nameInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--sequence
	sqlite3_bind_text(createStmt, 3, [sequenceInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--name_ansi
	sqlite3_bind_text(createStmt, 4, [name_ansiInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Delete Row To Table

-(BOOL)deleteRowByRowId:(NSString *) rowid
{
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE rowid = %@", self.usesTable, rowid];
	//--NSLog(@"sql: %@", sql);

	char *err;
	if (sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {

		NSLog(@"reture code: %d",sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err));

		sqlite3_close(db);
		NSAssert(0, @"Error insert table ZCARDGAME.");

	}

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select All Row In Table

-(NSMutableArray *) getAllRowsInTablepattern_category
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ ", self.usesTable];

	return [self getArrayDataBy_SQL:qsql];
}


//--Retrieving Array Data
-(NSMutableArray *) getArrayDataBy_SQL:(NSString *)qsql
{
	NSMutableArray *array = [NSMutableArray array];

	sqlite3_stmt *statement;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--pattern_category_id
			char *pattern_category_id = (char *) sqlite3_column_text(statement, 1);
			NSString *pattern_category_idStr = (pattern_category_id == NULL) ? @"":[NSString stringWithUTF8String:pattern_category_id];

			//--name
			char *name = (char *) sqlite3_column_text(statement, 2);
			NSString *nameStr = (name == NULL) ? @"":[NSString stringWithUTF8String:name];

			//--sequence
			char *sequence = (char *) sqlite3_column_text(statement, 3);
			NSString *sequenceStr = (sequence == NULL) ? @"":[NSString stringWithUTF8String:sequence];

			//--name_ansi
			char *name_ansi = (char *) sqlite3_column_text(statement, 4);
			NSString *name_ansiStr = (name_ansi == NULL) ? @"":[NSString stringWithUTF8String:name_ansi];



			//--add Object
			pattern_categoryField *object = [[pattern_categoryField alloc]init];

			object.rowid = rowidStr;
			object.pattern_category_id = pattern_category_idStr;
			object.name = nameStr;
			object.sequence = sequenceStr;
			object.name_ansi = name_ansiStr;
			
			[array addObject:object];

			//--free memory
			[object release];

		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return array;
}



/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select Row By rowid In Table

-(pattern_categoryField *) getRowByRowsInTablepattern_categoryByRowId:(NSString *) rowid
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE rowid = \"%@\" ", self.usesTable,rowid];

	return [self getData_pattern_categoryField_BySQL:qsql];
}


//--Retrieving Data
-(pattern_categoryField *) getData_pattern_categoryField_BySQL:(NSString *) qsql
{
	sqlite3_stmt *statement;
	pattern_categoryField *object = nil;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--pattern_category_id
			char *pattern_category_id = (char *) sqlite3_column_text(statement, 1);
			NSString *pattern_category_idStr = (pattern_category_id == NULL) ? @"":[NSString stringWithUTF8String:pattern_category_id];

			//--name
			char *name = (char *) sqlite3_column_text(statement, 2);
			NSString *nameStr = (name == NULL) ? @"":[NSString stringWithUTF8String:name];

			//--sequence
			char *sequence = (char *) sqlite3_column_text(statement, 3);
			NSString *sequenceStr = (sequence == NULL) ? @"":[NSString stringWithUTF8String:sequence];

			//--name_ansi
			char *name_ansi = (char *) sqlite3_column_text(statement, 4);
			NSString *name_ansiStr = (name_ansi == NULL) ? @"":[NSString stringWithUTF8String:name_ansi];



			//--add Object
			object = [[[pattern_categoryField alloc] init] autorelease];

			object.rowid = rowidStr;
			object.pattern_category_id = pattern_category_idStr;
			object.name = nameStr;
			object.sequence = sequenceStr;
			object.name_ansi = name_ansiStr;
			
		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return object;
}



/********************************************************************************/
@end