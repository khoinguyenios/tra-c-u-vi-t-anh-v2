

////pattern_categoryModel.h 
//  Project 
// 
//  Created by TeamiOS on 2014-06-09 07:46:59 +0000. 
//
// 

#import <Foundation/Foundation.h>
#import "AppModel.h"
#import "pattern_categoryField.h"

@interface pattern_categoryModel : AppModel
{

}


/** Get all rows in Table pattern_category **/
-(NSMutableArray *) getAllRowsInTablepattern_category;

/** Get rows by rowid in Table pattern_category **/
-(pattern_categoryField *) getRowByRowsInTablepattern_categoryByRowId:(NSString *) rowid;


/** create a new row **/
-(BOOL)createRowInTablepattern_category:(pattern_categoryField *) data;

/** update row **/
-(BOOL)updateRowInTablepattern_category:(pattern_categoryField *) data;

/** delete row **/
-(BOOL)deleteRowByRowId:(NSString *) rowid;

@end