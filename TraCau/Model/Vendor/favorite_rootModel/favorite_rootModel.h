

////favorite_rootModel.h 
//  Project 
// 
//  Created by TeamiOS on 2014-06-09 07:44:00 +0000. 
//
// 

#import <Foundation/Foundation.h>
#import "AppModel.h"
#import "favorite_rootField.h"

@interface favorite_rootModel : AppModel
{

}


/** Get all rows in Table favorite_root **/
-(NSMutableArray *) getAllRowsInTablefavorite_root;

/** Get rows by rowid in Table favorite_root **/
-(favorite_rootField *) getRowByRowsInTablefavorite_rootByRowId:(NSString *) rowid;

-(favorite_rootField *) getRowInTablefavorite_rootByRootName: (NSString *)rootName;

/** create a new row **/
-(BOOL)createRowInTablefavorite_root:(favorite_rootField *) data;

/** update row **/
-(BOOL)updateRowInTablefavorite_root:(favorite_rootField *) data;

/** delete row **/
-(BOOL)deleteRowByRowId:(NSString *) rowid;

@end