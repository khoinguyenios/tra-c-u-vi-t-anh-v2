

////favorite_rootField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-06-09 07:44:00 +0000. 
//

//--CREATE TABLE "favorite_root" ("root_id" TEXT, "root_name" TEXT, )

#import "favorite_rootModel.h"

@implementation favorite_rootModel

/** init class */
-(id) init
{
	self = [super init];

	if (self)
	{
		self.usesTable = @"favorite_root";
	}
	return self;
}


/* free memory */
-(void) dealloc
{
	[super dealloc];
}



/********************************************************************************/
#pragma mark  - Insert Row To Table

-(BOOL)createRowInTablefavorite_root:(favorite_rootField *) data
{
	NSString *root_id = data.root_id;
	NSString *root_name = data.root_name;

	NSString *root_idInit = @"";
	NSString *root_nameInit = @"";


	if (root_id)
		root_idInit = root_id;

	if (root_name)
		root_nameInit = root_name;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		const char *sql = "INSERT INTO \"favorite_root\" (\"root_id\",\"root_name\") VALUES (?1,?2)";

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--root_id
	sqlite3_bind_text(createStmt, 1, [root_idInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--root_name
	sqlite3_bind_text(createStmt, 2, [root_nameInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Update Row To Table

-(BOOL)updateRowInTablefavorite_root:(favorite_rootField *) data
{

	NSString *root_id = data.root_id;
	NSString *root_name = data.root_name;

	NSString *root_idInit = @"";
	NSString *root_nameInit = @"";


	if (root_id)
		root_idInit = root_id;

	if (root_name)
		root_nameInit = root_name;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		NSString *sqlOriginal = [NSString stringWithFormat:@"UPDATE \"favorite_root\" SET \"root_id\" = ?, \"root_name\" = ? WHERE rowid = %@", data.rowid];

		const char *sql = sqlOriginal.UTF8String;

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--root_id
	sqlite3_bind_text(createStmt, 1, [root_idInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--root_name
	sqlite3_bind_text(createStmt, 2, [root_nameInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Delete Row To Table

-(BOOL)deleteRowByRowId:(NSString *) rowid
{
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE rowid = %@", self.usesTable, rowid];
	//--NSLog(@"sql: %@", sql);

	char *err;
	if (sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {

		NSLog(@"reture code: %d",sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err));

		sqlite3_close(db);
		NSAssert(0, @"Error insert table ZCARDGAME.");

	}

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select All Row In Table

-(NSMutableArray *) getAllRowsInTablefavorite_root
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ ", self.usesTable];

	return [self getArrayDataBy_SQL:qsql];
}


//--Retrieving Array Data
-(NSMutableArray *) getArrayDataBy_SQL:(NSString *)qsql
{
	NSMutableArray *array = [NSMutableArray array];

	sqlite3_stmt *statement;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--root_id
			char *root_id = (char *) sqlite3_column_text(statement, 1);
			NSString *root_idStr = (root_id == NULL) ? @"":[NSString stringWithUTF8String:root_id];

			//--root_name
			char *root_name = (char *) sqlite3_column_text(statement, 2);
			NSString *root_nameStr = (root_name == NULL) ? @"":[NSString stringWithUTF8String:root_name];



			//--add Object
			favorite_rootField *object = [[favorite_rootField alloc]init];

			object.rowid = rowidStr;
			object.root_id = root_idStr;
			object.root_name = root_nameStr;
			
			[array addObject:object];

			//--free memory
			[object release];

		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return array;
}



/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select Row By rowid In Table

-(favorite_rootField *) getRowByRowsInTablefavorite_rootByRowId:(NSString *) rowid
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE rowid = \"%@\" ", self.usesTable,rowid];

	return [self getData_favorite_rootField_BySQL:qsql];
}


-(favorite_rootField *) getRowInTablefavorite_rootByRootName: (NSString *)rootName
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE root_name LIKE \"%@\" ", self.usesTable,rootName];
    
	return [self getData_favorite_rootField_BySQL:qsql];
}


//--Retrieving Data
-(favorite_rootField *) getData_favorite_rootField_BySQL:(NSString *) qsql
{
	sqlite3_stmt *statement;
	favorite_rootField *object = nil;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--root_id
			char *root_id = (char *) sqlite3_column_text(statement, 1);
			NSString *root_idStr = (root_id == NULL) ? @"":[NSString stringWithUTF8String:root_id];

			//--root_name
			char *root_name = (char *) sqlite3_column_text(statement, 2);
			NSString *root_nameStr = (root_name == NULL) ? @"":[NSString stringWithUTF8String:root_name];



			//--add Object
			object = [[[favorite_rootField alloc] init] autorelease];

			object.rowid = rowidStr;
			object.root_id = root_idStr;
			object.root_name = root_nameStr;
			
		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return object;
}



/********************************************************************************/
@end