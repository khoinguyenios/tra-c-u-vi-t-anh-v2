

////verbModel.h 
//  Project 
// 
//  Created by TeamiOS on 2014-06-09 07:48:47 +0000. 
//
// 

#import <Foundation/Foundation.h>
#import "AppModel.h"
#import "verbField.h"

@interface verbModel : AppModel
{

}


/** Get all rows in Table verb **/
-(NSMutableArray *) getAllRowsInTableverb;

/** Get rows by rowid in Table verb **/
-(verbField *) getRowByRowsInTableverbByRowId:(NSString *) rowid;


/** create a new row **/
-(BOOL)createRowInTableverb:(verbField *) data;

/** update row **/
-(BOOL)updateRowInTableverb:(verbField *) data;

/** delete row **/
-(BOOL)deleteRowByRowId:(NSString *) rowid;

//------------------------------------------------------------------------------------------
/**  search verb */
-(verbField *)searchVerbByKeyWord: (NSString *)keyWord;
@end