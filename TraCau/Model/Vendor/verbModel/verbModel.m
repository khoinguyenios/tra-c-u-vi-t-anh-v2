

////verbField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-06-09 07:48:47 +0000. 
//

//--CREATE TABLE "verb" ("verb_id" TEXT, "english" TEXT, "italian" TEXT, "simplified_italian" TEXT, "portuguese" TEXT, "simplified_portuguese" TEXT, "spanish" TEXT, "simplified_spanish" TEXT, "french" TEXT, "simplified_french" TEXT, "german" TEXT, "simplified_german" TEXT, "dutch" TEXT, "simplified_dutch" TEXT, )

#import "verbModel.h"

@implementation verbModel

/** init class */
-(id) init
{
	self = [super init];

	if (self)
	{
		self.usesTable = @"verb";
	}
	return self;
}


/* free memory */
-(void) dealloc
{
	[super dealloc];
}



/********************************************************************************/
#pragma mark  - Insert Row To Table

-(BOOL)createRowInTableverb:(verbField *) data
{
	NSString *verb_id = data.verb_id;
	NSString *english = data.english;
	NSString *italian = data.italian;
	NSString *simplified_italian = data.simplified_italian;
	NSString *portuguese = data.portuguese;
	NSString *simplified_portuguese = data.simplified_portuguese;
	NSString *spanish = data.spanish;
	NSString *simplified_spanish = data.simplified_spanish;
	NSString *french = data.french;
	NSString *simplified_french = data.simplified_french;
	NSString *german = data.german;
	NSString *simplified_german = data.simplified_german;
	NSString *dutch = data.dutch;
	NSString *simplified_dutch = data.simplified_dutch;

	NSString *verb_idInit = @"";
	NSString *englishInit = @"";
	NSString *italianInit = @"";
	NSString *simplified_italianInit = @"";
	NSString *portugueseInit = @"";
	NSString *simplified_portugueseInit = @"";
	NSString *spanishInit = @"";
	NSString *simplified_spanishInit = @"";
	NSString *frenchInit = @"";
	NSString *simplified_frenchInit = @"";
	NSString *germanInit = @"";
	NSString *simplified_germanInit = @"";
	NSString *dutchInit = @"";
	NSString *simplified_dutchInit = @"";


	if (verb_id)
		verb_idInit = verb_id;

	if (english)
		englishInit = english;

	if (italian)
		italianInit = italian;

	if (simplified_italian)
		simplified_italianInit = simplified_italian;

	if (portuguese)
		portugueseInit = portuguese;

	if (simplified_portuguese)
		simplified_portugueseInit = simplified_portuguese;

	if (spanish)
		spanishInit = spanish;

	if (simplified_spanish)
		simplified_spanishInit = simplified_spanish;

	if (french)
		frenchInit = french;

	if (simplified_french)
		simplified_frenchInit = simplified_french;

	if (german)
		germanInit = german;

	if (simplified_german)
		simplified_germanInit = simplified_german;

	if (dutch)
		dutchInit = dutch;

	if (simplified_dutch)
		simplified_dutchInit = simplified_dutch;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		const char *sql = "INSERT INTO \"verb\" (\"verb_id\",\"english\",\"italian\",\"simplified_italian\",\"portuguese\",\"simplified_portuguese\",\"spanish\",\"simplified_spanish\",\"french\",\"simplified_french\",\"german\",\"simplified_german\",\"dutch\",\"simplified_dutch\") VALUES (?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12,?13,?14)";

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--verb_id
	sqlite3_bind_text(createStmt, 1, [verb_idInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--english
	sqlite3_bind_text(createStmt, 2, [englishInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--italian
	sqlite3_bind_text(createStmt, 3, [italianInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--simplified_italian
	sqlite3_bind_text(createStmt, 4, [simplified_italianInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--portuguese
	sqlite3_bind_text(createStmt, 5, [portugueseInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--simplified_portuguese
	sqlite3_bind_text(createStmt, 6, [simplified_portugueseInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--spanish
	sqlite3_bind_text(createStmt, 7, [spanishInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--simplified_spanish
	sqlite3_bind_text(createStmt, 8, [simplified_spanishInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--french
	sqlite3_bind_text(createStmt, 9, [frenchInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--simplified_french
	sqlite3_bind_text(createStmt, 10, [simplified_frenchInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--german
	sqlite3_bind_text(createStmt, 11, [germanInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--simplified_german
	sqlite3_bind_text(createStmt, 12, [simplified_germanInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--dutch
	sqlite3_bind_text(createStmt, 13, [dutchInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--simplified_dutch
	sqlite3_bind_text(createStmt, 14, [simplified_dutchInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Update Row To Table

-(BOOL)updateRowInTableverb:(verbField *) data
{

	NSString *verb_id = data.verb_id;
	NSString *english = data.english;
	NSString *italian = data.italian;
	NSString *simplified_italian = data.simplified_italian;
	NSString *portuguese = data.portuguese;
	NSString *simplified_portuguese = data.simplified_portuguese;
	NSString *spanish = data.spanish;
	NSString *simplified_spanish = data.simplified_spanish;
	NSString *french = data.french;
	NSString *simplified_french = data.simplified_french;
	NSString *german = data.german;
	NSString *simplified_german = data.simplified_german;
	NSString *dutch = data.dutch;
	NSString *simplified_dutch = data.simplified_dutch;

	NSString *verb_idInit = @"";
	NSString *englishInit = @"";
	NSString *italianInit = @"";
	NSString *simplified_italianInit = @"";
	NSString *portugueseInit = @"";
	NSString *simplified_portugueseInit = @"";
	NSString *spanishInit = @"";
	NSString *simplified_spanishInit = @"";
	NSString *frenchInit = @"";
	NSString *simplified_frenchInit = @"";
	NSString *germanInit = @"";
	NSString *simplified_germanInit = @"";
	NSString *dutchInit = @"";
	NSString *simplified_dutchInit = @"";


	if (verb_id)
		verb_idInit = verb_id;

	if (english)
		englishInit = english;

	if (italian)
		italianInit = italian;

	if (simplified_italian)
		simplified_italianInit = simplified_italian;

	if (portuguese)
		portugueseInit = portuguese;

	if (simplified_portuguese)
		simplified_portugueseInit = simplified_portuguese;

	if (spanish)
		spanishInit = spanish;

	if (simplified_spanish)
		simplified_spanishInit = simplified_spanish;

	if (french)
		frenchInit = french;

	if (simplified_french)
		simplified_frenchInit = simplified_french;

	if (german)
		germanInit = german;

	if (simplified_german)
		simplified_germanInit = simplified_german;

	if (dutch)
		dutchInit = dutch;

	if (simplified_dutch)
		simplified_dutchInit = simplified_dutch;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		NSString *sqlOriginal = [NSString stringWithFormat:@"UPDATE \"verb\" SET \"verb_id\" = ?, \"english\" = ?, \"italian\" = ?, \"simplified_italian\" = ?, \"portuguese\" = ?, \"simplified_portuguese\" = ?, \"spanish\" = ?, \"simplified_spanish\" = ?, \"french\" = ?, \"simplified_french\" = ?, \"german\" = ?, \"simplified_german\" = ?, \"dutch\" = ?, \"simplified_dutch\" = ? WHERE rowid = %@", data.rowid];

		const char *sql = sqlOriginal.UTF8String;

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--verb_id
	sqlite3_bind_text(createStmt, 1, [verb_idInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--english
	sqlite3_bind_text(createStmt, 2, [englishInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--italian
	sqlite3_bind_text(createStmt, 3, [italianInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--simplified_italian
	sqlite3_bind_text(createStmt, 4, [simplified_italianInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--portuguese
	sqlite3_bind_text(createStmt, 5, [portugueseInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--simplified_portuguese
	sqlite3_bind_text(createStmt, 6, [simplified_portugueseInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--spanish
	sqlite3_bind_text(createStmt, 7, [spanishInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--simplified_spanish
	sqlite3_bind_text(createStmt, 8, [simplified_spanishInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--french
	sqlite3_bind_text(createStmt, 9, [frenchInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--simplified_french
	sqlite3_bind_text(createStmt, 10, [simplified_frenchInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--german
	sqlite3_bind_text(createStmt, 11, [germanInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--simplified_german
	sqlite3_bind_text(createStmt, 12, [simplified_germanInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--dutch
	sqlite3_bind_text(createStmt, 13, [dutchInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--simplified_dutch
	sqlite3_bind_text(createStmt, 14, [simplified_dutchInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Delete Row To Table

-(BOOL)deleteRowByRowId:(NSString *) rowid
{
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE rowid = %@", self.usesTable, rowid];
	//--NSLog(@"sql: %@", sql);

	char *err;
	if (sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {

		NSLog(@"reture code: %d",sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err));

		sqlite3_close(db);
		NSAssert(0, @"Error insert table ZCARDGAME.");

	}

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select All Row In Table

-(NSMutableArray *) getAllRowsInTableverb
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ ", self.usesTable];

	return [self getArrayDataBy_SQL:qsql];
}


//--Retrieving Array Data
-(NSMutableArray *) getArrayDataBy_SQL:(NSString *)qsql
{
	NSMutableArray *array = [NSMutableArray array];

	sqlite3_stmt *statement;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--verb_id
			char *verb_id = (char *) sqlite3_column_text(statement, 1);
			NSString *verb_idStr = (verb_id == NULL) ? @"":[NSString stringWithUTF8String:verb_id];

			//--english
			char *english = (char *) sqlite3_column_text(statement, 2);
			NSString *englishStr = (english == NULL) ? @"":[NSString stringWithUTF8String:english];

			//--italian
			char *italian = (char *) sqlite3_column_text(statement, 3);
			NSString *italianStr = (italian == NULL) ? @"":[NSString stringWithUTF8String:italian];

			//--simplified_italian
			char *simplified_italian = (char *) sqlite3_column_text(statement, 4);
			NSString *simplified_italianStr = (simplified_italian == NULL) ? @"":[NSString stringWithUTF8String:simplified_italian];

			//--portuguese
			char *portuguese = (char *) sqlite3_column_text(statement, 5);
			NSString *portugueseStr = (portuguese == NULL) ? @"":[NSString stringWithUTF8String:portuguese];

			//--simplified_portuguese
			char *simplified_portuguese = (char *) sqlite3_column_text(statement, 6);
			NSString *simplified_portugueseStr = (simplified_portuguese == NULL) ? @"":[NSString stringWithUTF8String:simplified_portuguese];

			//--spanish
			char *spanish = (char *) sqlite3_column_text(statement, 7);
			NSString *spanishStr = (spanish == NULL) ? @"":[NSString stringWithUTF8String:spanish];

			//--simplified_spanish
			char *simplified_spanish = (char *) sqlite3_column_text(statement, 8);
			NSString *simplified_spanishStr = (simplified_spanish == NULL) ? @"":[NSString stringWithUTF8String:simplified_spanish];

			//--french
			char *french = (char *) sqlite3_column_text(statement, 9);
			NSString *frenchStr = (french == NULL) ? @"":[NSString stringWithUTF8String:french];

			//--simplified_french
			char *simplified_french = (char *) sqlite3_column_text(statement, 10);
			NSString *simplified_frenchStr = (simplified_french == NULL) ? @"":[NSString stringWithUTF8String:simplified_french];

			//--german
			char *german = (char *) sqlite3_column_text(statement, 11);
			NSString *germanStr = (german == NULL) ? @"":[NSString stringWithUTF8String:german];

			//--simplified_german
			char *simplified_german = (char *) sqlite3_column_text(statement, 12);
			NSString *simplified_germanStr = (simplified_german == NULL) ? @"":[NSString stringWithUTF8String:simplified_german];

			//--dutch
			char *dutch = (char *) sqlite3_column_text(statement, 13);
			NSString *dutchStr = (dutch == NULL) ? @"":[NSString stringWithUTF8String:dutch];

			//--simplified_dutch
			char *simplified_dutch = (char *) sqlite3_column_text(statement, 14);
			NSString *simplified_dutchStr = (simplified_dutch == NULL) ? @"":[NSString stringWithUTF8String:simplified_dutch];



			//--add Object
			verbField *object = [[verbField alloc]init];

			object.rowid = rowidStr;
			object.verb_id = verb_idStr;
			object.english = englishStr;
			object.italian = italianStr;
			object.simplified_italian = simplified_italianStr;
			object.portuguese = portugueseStr;
			object.simplified_portuguese = simplified_portugueseStr;
			object.spanish = spanishStr;
			object.simplified_spanish = simplified_spanishStr;
			object.french = frenchStr;
			object.simplified_french = simplified_frenchStr;
			object.german = germanStr;
			object.simplified_german = simplified_germanStr;
			object.dutch = dutchStr;
			object.simplified_dutch = simplified_dutchStr;
			
			[array addObject:object];

			//--free memory
			[object release];

		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return array;
}



/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select Row By rowid In Table

-(verbField *) getRowByRowsInTableverbByRowId:(NSString *) rowid
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE rowid = \"%@\" ", self.usesTable,rowid];

	return [self getData_verbField_BySQL:qsql];
}


//--Retrieving Data
-(verbField *) getData_verbField_BySQL:(NSString *) qsql
{
	sqlite3_stmt *statement;
	verbField *object = nil;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--verb_id
			char *verb_id = (char *) sqlite3_column_text(statement, 1);
			NSString *verb_idStr = (verb_id == NULL) ? @"":[NSString stringWithUTF8String:verb_id];

			//--english
			char *english = (char *) sqlite3_column_text(statement, 2);
			NSString *englishStr = (english == NULL) ? @"":[NSString stringWithUTF8String:english];

			//--italian
			char *italian = (char *) sqlite3_column_text(statement, 3);
			NSString *italianStr = (italian == NULL) ? @"":[NSString stringWithUTF8String:italian];

			//--simplified_italian
			char *simplified_italian = (char *) sqlite3_column_text(statement, 4);
			NSString *simplified_italianStr = (simplified_italian == NULL) ? @"":[NSString stringWithUTF8String:simplified_italian];

			//--portuguese
			char *portuguese = (char *) sqlite3_column_text(statement, 5);
			NSString *portugueseStr = (portuguese == NULL) ? @"":[NSString stringWithUTF8String:portuguese];

			//--simplified_portuguese
			char *simplified_portuguese = (char *) sqlite3_column_text(statement, 6);
			NSString *simplified_portugueseStr = (simplified_portuguese == NULL) ? @"":[NSString stringWithUTF8String:simplified_portuguese];

			//--spanish
			char *spanish = (char *) sqlite3_column_text(statement, 7);
			NSString *spanishStr = (spanish == NULL) ? @"":[NSString stringWithUTF8String:spanish];

			//--simplified_spanish
			char *simplified_spanish = (char *) sqlite3_column_text(statement, 8);
			NSString *simplified_spanishStr = (simplified_spanish == NULL) ? @"":[NSString stringWithUTF8String:simplified_spanish];

			//--french
			char *french = (char *) sqlite3_column_text(statement, 9);
			NSString *frenchStr = (french == NULL) ? @"":[NSString stringWithUTF8String:french];

			//--simplified_french
			char *simplified_french = (char *) sqlite3_column_text(statement, 10);
			NSString *simplified_frenchStr = (simplified_french == NULL) ? @"":[NSString stringWithUTF8String:simplified_french];

			//--german
			char *german = (char *) sqlite3_column_text(statement, 11);
			NSString *germanStr = (german == NULL) ? @"":[NSString stringWithUTF8String:german];

			//--simplified_german
			char *simplified_german = (char *) sqlite3_column_text(statement, 12);
			NSString *simplified_germanStr = (simplified_german == NULL) ? @"":[NSString stringWithUTF8String:simplified_german];

			//--dutch
			char *dutch = (char *) sqlite3_column_text(statement, 13);
			NSString *dutchStr = (dutch == NULL) ? @"":[NSString stringWithUTF8String:dutch];

			//--simplified_dutch
			char *simplified_dutch = (char *) sqlite3_column_text(statement, 14);
			NSString *simplified_dutchStr = (simplified_dutch == NULL) ? @"":[NSString stringWithUTF8String:simplified_dutch];



			//--add Object
			object = [[[verbField alloc] init] autorelease];

			object.rowid = rowidStr;
			object.verb_id = verb_idStr;
			object.english = englishStr;
			object.italian = italianStr;
			object.simplified_italian = simplified_italianStr;
			object.portuguese = portugueseStr;
			object.simplified_portuguese = simplified_portugueseStr;
			object.spanish = spanishStr;
			object.simplified_spanish = simplified_spanishStr;
			object.french = frenchStr;
			object.simplified_french = simplified_frenchStr;
			object.german = germanStr;
			object.simplified_german = simplified_germanStr;
			object.dutch = dutchStr;
			object.simplified_dutch = simplified_dutchStr;
			
		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return object;
}



/********************************************************************************/


//------------------------------------------------------------------------------------------
/**   */
-(verbField *)searchVerbByKeyWord: (NSString *)keyWord
{
    //--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE english LIKE \"%@\" ", self.usesTable,keyWord];
    
	return [self getData_verbField_BySQL:qsql];

}



@end