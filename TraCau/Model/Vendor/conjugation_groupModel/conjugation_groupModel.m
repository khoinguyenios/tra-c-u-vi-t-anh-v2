

////conjugation_groupField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-06-19 03:02:46 +0000. 
//

//--CREATE TABLE "conjugation_group" ("conjugation_group_id" TEXT, "conjugation_group_name" TEXT, )

#import "conjugation_groupModel.h"

@implementation conjugation_groupModel

/** init class */
-(id) init
{
	self = [super init];

	if (self)
	{
		self.usesTable = @"conjugation_group";
	}
	return self;
}


/* free memory */
-(void) dealloc
{
	[super dealloc];
}



/********************************************************************************/
#pragma mark  - Insert Row To Table

-(BOOL)createRowInTableconjugation_group:(conjugation_groupField *) data
{
	NSString *conjugation_group_id = data.conjugation_group_id;
	NSString *conjugation_group_name = data.conjugation_group_name;

	NSString *conjugation_group_idInit = @"";
	NSString *conjugation_group_nameInit = @"";


	if (conjugation_group_id)
		conjugation_group_idInit = conjugation_group_id;

	if (conjugation_group_name)
		conjugation_group_nameInit = conjugation_group_name;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		const char *sql = "INSERT INTO \"conjugation_group\" (\"conjugation_group_id\",\"conjugation_group_name\") VALUES (?1,?2)";

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--conjugation_group_id
	sqlite3_bind_text(createStmt, 1, [conjugation_group_idInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--conjugation_group_name
	sqlite3_bind_text(createStmt, 2, [conjugation_group_nameInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Update Row To Table

-(BOOL)updateRowInTableconjugation_group:(conjugation_groupField *) data
{

	NSString *conjugation_group_id = data.conjugation_group_id;
	NSString *conjugation_group_name = data.conjugation_group_name;

	NSString *conjugation_group_idInit = @"";
	NSString *conjugation_group_nameInit = @"";


	if (conjugation_group_id)
		conjugation_group_idInit = conjugation_group_id;

	if (conjugation_group_name)
		conjugation_group_nameInit = conjugation_group_name;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		NSString *sqlOriginal = [NSString stringWithFormat:@"UPDATE \"conjugation_group\" SET \"conjugation_group_id\" = ?, \"conjugation_group_name\" = ? WHERE rowid = %@", data.rowid];

		const char *sql = sqlOriginal.UTF8String;

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--conjugation_group_id
	sqlite3_bind_text(createStmt, 1, [conjugation_group_idInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--conjugation_group_name
	sqlite3_bind_text(createStmt, 2, [conjugation_group_nameInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Delete Row To Table

-(BOOL)deleteRowByRowId:(NSString *) rowid
{
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE rowid = %@", self.usesTable, rowid];
	//--NSLog(@"sql: %@", sql);

	char *err;
	if (sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {

		NSLog(@"reture code: %d",sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err));

		sqlite3_close(db);
		NSAssert(0, @"Error insert table ZCARDGAME.");

	}

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select All Row In Table

-(NSMutableArray *) getAllRowsInTableconjugation_group
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ ", self.usesTable];

	return [self getArrayDataBy_SQL:qsql];
}


//--Retrieving Array Data
-(NSMutableArray *) getArrayDataBy_SQL:(NSString *)qsql
{
	NSMutableArray *array = [NSMutableArray array];

	sqlite3_stmt *statement;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--conjugation_group_id
			char *conjugation_group_id = (char *) sqlite3_column_text(statement, 1);
			NSString *conjugation_group_idStr = (conjugation_group_id == NULL) ? @"":[NSString stringWithUTF8String:conjugation_group_id];

			//--conjugation_group_name
			char *conjugation_group_name = (char *) sqlite3_column_text(statement, 2);
			NSString *conjugation_group_nameStr = (conjugation_group_name == NULL) ? @"":[NSString stringWithUTF8String:conjugation_group_name];



			//--add Object
			conjugation_groupField *object = [[conjugation_groupField alloc]init];

			object.rowid = rowidStr;
			object.conjugation_group_id = conjugation_group_idStr;
			object.conjugation_group_name = conjugation_group_nameStr;
			
			[array addObject:object];

			//--free memory
			[object release];

		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return array;
}



/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select Row By rowid In Table

-(conjugation_groupField *) getRowByRowsInTableconjugation_groupByRowId:(NSString *) rowid
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE rowid = \"%@\" ", self.usesTable,rowid];

	return [self getData_conjugation_groupField_BySQL:qsql];
}


//--Retrieving Data
-(conjugation_groupField *) getData_conjugation_groupField_BySQL:(NSString *) qsql
{
	sqlite3_stmt *statement;
	conjugation_groupField *object = nil;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--conjugation_group_id
			char *conjugation_group_id = (char *) sqlite3_column_text(statement, 1);
			NSString *conjugation_group_idStr = (conjugation_group_id == NULL) ? @"":[NSString stringWithUTF8String:conjugation_group_id];

			//--conjugation_group_name
			char *conjugation_group_name = (char *) sqlite3_column_text(statement, 2);
			NSString *conjugation_group_nameStr = (conjugation_group_name == NULL) ? @"":[NSString stringWithUTF8String:conjugation_group_name];



			//--add Object
			object = [[[conjugation_groupField alloc] init] autorelease];

			object.rowid = rowidStr;
			object.conjugation_group_id = conjugation_group_idStr;
			object.conjugation_group_name = conjugation_group_nameStr;
			
		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return object;
}



/********************************************************************************/
@end