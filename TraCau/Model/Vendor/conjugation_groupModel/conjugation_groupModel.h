

////conjugation_groupModel.h 
//  Project 
// 
//  Created by TeamiOS on 2014-06-19 03:02:46 +0000. 
//
// 

#import <Foundation/Foundation.h>
#import "AppModel.h"
#import "conjugation_groupField.h"

@interface conjugation_groupModel : AppModel
{

}


/** Get all rows in Table conjugation_group **/
-(NSMutableArray *) getAllRowsInTableconjugation_group;

/** Get rows by rowid in Table conjugation_group **/
-(conjugation_groupField *) getRowByRowsInTableconjugation_groupByRowId:(NSString *) rowid;


/** create a new row **/
-(BOOL)createRowInTableconjugation_group:(conjugation_groupField *) data;

/** update row **/
-(BOOL)updateRowInTableconjugation_group:(conjugation_groupField *) data;

/** delete row **/
-(BOOL)deleteRowByRowId:(NSString *) rowid;

@end