

////wordsModel.h 
//  Project 
// 
//  Created by TeamiOS on 2014-06-09 07:49:43 +0000. 
//
// 

#import <Foundation/Foundation.h>
#import "AppModel.h"
#import "wordsField.h"

@interface wordsModel : AppModel
{

}


/** Get all rows in Table words **/
-(NSMutableArray *) getAllRowsInTablewords;

/** Get rows by rowid in Table words **/
-(wordsField *) getRowByRowsInTablewordsByRowId:(NSString *) rowid;


/** create a new row **/
-(BOOL)createRowInTablewords:(wordsField *) data;

/** update row **/
-(BOOL)updateRowInTablewords:(wordsField *) data;

/** delete row **/
-(BOOL)deleteRowByRowId:(NSString *) rowid;

//------------------------------------------------------------------------------------------
/**  Get all row in table by rowid */
-(NSMutableArray *) getAllRowsIdInTablewordsWithKeyWord: (NSString *)keyWord;

@end