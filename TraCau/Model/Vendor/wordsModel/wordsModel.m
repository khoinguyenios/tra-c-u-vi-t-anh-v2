

////wordsField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-06-09 07:49:43 +0000. 
//

//--CREATE TABLE "words" ("word" TEXT, "content" TEXT, "word_id" TEXT, )

#import "wordsModel.h"

@implementation wordsModel

/** init class */
-(id) init
{
	self = [super init];

	if (self)
	{
		self.usesTable = @"words";
	}
	return self;
}


/* free memory */
-(void) dealloc
{
	[super dealloc];
}



/********************************************************************************/
#pragma mark  - Insert Row To Table

-(BOOL)createRowInTablewords:(wordsField *) data
{
	NSString *word = data.word;
	NSString *content = data.content;
	NSString *word_id = data.word_id;

	NSString *wordInit = @"";
	NSString *contentInit = @"";
	NSString *word_idInit = @"";


	if (word)
		wordInit = word;

	if (content)
		contentInit = content;

	if (word_id)
		word_idInit = word_id;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		const char *sql = "INSERT INTO \"words\" (\"word\",\"content\",\"word_id\") VALUES (?1,?2,?3)";

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--word
	sqlite3_bind_text(createStmt, 1, [wordInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--content
	sqlite3_bind_text(createStmt, 2, [contentInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--word_id
	sqlite3_bind_text(createStmt, 3, [word_idInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Update Row To Table

-(BOOL)updateRowInTablewords:(wordsField *) data
{

	NSString *word = data.word;
	NSString *content = data.content;
	NSString *word_id = data.word_id;

	NSString *wordInit = @"";
	NSString *contentInit = @"";
	NSString *word_idInit = @"";


	if (word)
		wordInit = word;

	if (content)
		contentInit = content;

	if (word_id)
		word_idInit = word_id;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		NSString *sqlOriginal = [NSString stringWithFormat:@"UPDATE \"words\" SET \"word\" = ?, \"content\" = ?, \"word_id\" = ? WHERE rowid = %@", data.rowid];

		const char *sql = sqlOriginal.UTF8String;

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--word
	sqlite3_bind_text(createStmt, 1, [wordInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--content
	sqlite3_bind_text(createStmt, 2, [contentInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--word_id
	sqlite3_bind_text(createStmt, 3, [word_idInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Delete Row To Table

-(BOOL)deleteRowByRowId:(NSString *) rowid
{
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE rowid = %@", self.usesTable, rowid];
	//--NSLog(@"sql: %@", sql);

	char *err;
	if (sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {

		NSLog(@"reture code: %d",sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err));

		sqlite3_close(db);
		NSAssert(0, @"Error insert table ZCARDGAME.");

	}

	return YES;
}

/********************************************************************************/




//------------------------------------------------------------------------------------------
/**   */
-(NSMutableArray *) getAllRowsIdInTablewordsWithKeyWord: (NSString *)keyWord
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid FROM %@ WHERE word LIKE \"%@%%\" ", self.usesTable, keyWord];
    
	NSMutableArray *array = [NSMutableArray array];
    
	sqlite3_stmt *statement;
    
	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];
            
			[array addObject:rowidStr];
            
		}
	}
    
	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}
    
	return array;
}


/********************************************************************************/
#pragma mark  - Select All Row In Table

-(NSMutableArray *) getAllRowsInTablewords
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ ", self.usesTable];

	return [self getArrayDataBy_SQL:qsql];
}


//--Retrieving Array Data
-(NSMutableArray *) getArrayDataBy_SQL:(NSString *)qsql
{
	NSMutableArray *array = [NSMutableArray array];

	sqlite3_stmt *statement;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--word
			char *word = (char *) sqlite3_column_text(statement, 1);
			NSString *wordStr = (word == NULL) ? @"":[NSString stringWithUTF8String:word];

			//--content
			char *content = (char *) sqlite3_column_text(statement, 2);
			NSString *contentStr = (content == NULL) ? @"":[NSString stringWithUTF8String:content];

			//--word_id
			char *word_id = (char *) sqlite3_column_text(statement, 3);
			NSString *word_idStr = (word_id == NULL) ? @"":[NSString stringWithUTF8String:word_id];



			//--add Object
			wordsField *object = [[wordsField alloc]init];

			object.rowid = rowidStr;
			object.word = wordStr;
			object.content = contentStr;
			object.word_id = word_idStr;
			
			[array addObject:object];

			//--free memory
			[object release];

		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return array;
}



/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select Row By rowid In Table

-(wordsField *) getRowByRowsInTablewordsByRowId:(NSString *) rowid
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE rowid = \"%@\" ", self.usesTable,rowid];

	return [self getData_wordsField_BySQL:qsql];
}


//--Retrieving Data
-(wordsField *) getData_wordsField_BySQL:(NSString *) qsql
{
	sqlite3_stmt *statement;
	wordsField *object = nil;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--word
			char *word = (char *) sqlite3_column_text(statement, 1);
			NSString *wordStr = (word == NULL) ? @"":[NSString stringWithUTF8String:word];

			//--content
			char *content = (char *) sqlite3_column_text(statement, 2);
			NSString *contentStr = (content == NULL) ? @"":[NSString stringWithUTF8String:content];

			//--word_id
			char *word_id = (char *) sqlite3_column_text(statement, 3);
			NSString *word_idStr = (word_id == NULL) ? @"":[NSString stringWithUTF8String:word_id];



			//--add Object
			object = [[[wordsField alloc] init] autorelease];

			object.rowid = rowidStr;
			object.word = wordStr;
			object.content = contentStr;
			object.word_id = word_idStr;
			
		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return object;
}



/********************************************************************************/
@end