

////search_historyModel.h 
//  Project 
// 
//  Created by TeamiOS on 2014-06-16 07:48:07 +0000. 
//
// 

#import <Foundation/Foundation.h>
#import "AppModel.h"
#import "search_historyField.h"

@interface search_historyModel : AppModel
{

}


/** Get all rows in Table search_history **/
-(NSMutableArray *) getAllRowsInTablesearch_history;

-(NSMutableArray *) getRowsInTablesearch_historyByDateId: (NSString *)dateId;

/** Get rows by rowid in Table search_history **/
-(search_historyField *) getRowByRowsInTablesearch_historyByRowId:(NSString *) rowid;
-(search_historyField *) getRowInTablesearch_historyBySearchWord: (NSString *)search_word;

/** create a new row **/
-(BOOL)createRowInTablesearch_history:(search_historyField *) data;

/** update row **/
-(BOOL)updateRowInTablesearch_history:(search_historyField *) data;

/** delete row **/
-(BOOL)deleteRowByRowId:(NSString *) rowid;
-(BOOL)clearAllRowsInTablesearch_history;

@end