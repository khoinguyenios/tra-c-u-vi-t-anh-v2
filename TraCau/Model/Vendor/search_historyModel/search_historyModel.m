

////search_historyField.m
//  Project
//
//  Created by TeamiOS on 2014-06-19 08:22:00 +0000.
//

//--CREATE TABLE "search_history" ("search_date_id" TEXT, "search_word" TEXT, "search_content" TEXT, )

#import "search_historyModel.h"

@implementation search_historyModel

/** init class */
-(id) init
{
	self = [super init];
    
	if (self)
	{
		self.usesTable = @"search_history";
	}
	return self;
}


/* free memory */
-(void) dealloc
{
	[super dealloc];
}



/********************************************************************************/
#pragma mark  - Insert Row To Table

-(BOOL)createRowInTablesearch_history:(search_historyField *) data
{
	NSString *search_date_id = data.search_date_id;
	NSString *search_word = data.search_word;
	NSString *search_content = data.search_content;
    
	NSString *search_date_idInit = @"";
	NSString *search_wordInit = @"";
	NSString *search_contentInit = @"";
    
    
	if (search_date_id)
		search_date_idInit = search_date_id;
    
	if (search_word)
		search_wordInit = search_word;
    
	if (search_content)
		search_contentInit = search_content;
    
	sqlite3_stmt *createStmt = nil;
    
	if (createStmt == nil)
	{
		//--string sql
		const char *sql = "INSERT INTO \"search_history\" (\"search_date_id\",\"search_word\",\"search_content\") VALUES (?1,?2,?3)";
        
		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}
    
 	//--search_date_id
	sqlite3_bind_text(createStmt, 1, [search_date_idInit UTF8String], -1, SQLITE_TRANSIENT);
    
 	//--search_word
	sqlite3_bind_text(createStmt, 2, [search_wordInit UTF8String], -1, SQLITE_TRANSIENT);
    
 	//--search_content
	sqlite3_bind_text(createStmt, 3, [search_contentInit UTF8String], -1, SQLITE_TRANSIENT);
    
    
    
	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}
    
	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);
    
	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Update Row To Table

-(BOOL)updateRowInTablesearch_history:(search_historyField *) data
{
    
	NSString *search_date_id = data.search_date_id;
	NSString *search_word = data.search_word;
	NSString *search_content = data.search_content;
    
	NSString *search_date_idInit = @"";
	NSString *search_wordInit = @"";
	NSString *search_contentInit = @"";
    
    
	if (search_date_id)
		search_date_idInit = search_date_id;
    
	if (search_word)
		search_wordInit = search_word;
    
	if (search_content)
		search_contentInit = search_content;
    
	sqlite3_stmt *createStmt = nil;
    
	if (createStmt == nil)
	{
		//--string sql
		NSString *sqlOriginal = [NSString stringWithFormat:@"UPDATE \"search_history\" SET \"search_date_id\" = ?, \"search_word\" = ?, \"search_content\" = ? WHERE rowid = %@", data.rowid];
        
		const char *sql = sqlOriginal.UTF8String;
        
		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}
    
 	//--search_date_id
	sqlite3_bind_text(createStmt, 1, [search_date_idInit UTF8String], -1, SQLITE_TRANSIENT);
    
 	//--search_word
	sqlite3_bind_text(createStmt, 2, [search_wordInit UTF8String], -1, SQLITE_TRANSIENT);
    
 	//--search_content
	sqlite3_bind_text(createStmt, 3, [search_contentInit UTF8String], -1, SQLITE_TRANSIENT);
    
    
    
	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}
    
	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);
    
	return YES;
}

/********************************************************************************/


#pragma mark -
#pragma mark Clear Table
/*************************************************************************************************************************/


-(BOOL)clearAllRowsInTablesearch_history
{
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ ", self.usesTable];
	//--NSLog(@"sql: %@", sql);
    
	char *err;
	if (sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {
        
		NSLog(@"reture code: %d",sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err));
        
		sqlite3_close(db);
		NSAssert(0, @"Error delete table ZCARDGAME.");
        
	}
    
	return YES;
}


/*************************************************************************************************************************/


/********************************************************************************/
#pragma mark  - Delete Row To Table

-(BOOL)deleteRowByRowId:(NSString *) rowid
{
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE rowid = %@", self.usesTable, rowid];
	//--NSLog(@"sql: %@", sql);
    
	char *err;
	if (sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {
        
		NSLog(@"reture code: %d",sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err));
        
		sqlite3_close(db);
		NSAssert(0, @"Error insert table ZCARDGAME.");
        
	}
    
	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select All Row In Table

-(NSMutableArray *) getAllRowsInTablesearch_history
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ ", self.usesTable];
    
	return [self getArrayDataBy_SQL:qsql];
}


-(NSMutableArray *) getRowsInTablesearch_historyByDateId: (NSString *)dateId
{
    NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE search_date_id = %@", self.usesTable, dateId];
    
    return [self getArrayDataBy_SQL:qsql];
}

//--Retrieving Array Data
-(NSMutableArray *) getArrayDataBy_SQL:(NSString *)qsql
{
	NSMutableArray *array = [NSMutableArray array];
    
	sqlite3_stmt *statement;
    
	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];
            
			//--search_date_id
			char *search_date_id = (char *) sqlite3_column_text(statement, 1);
			NSString *search_date_idStr = (search_date_id == NULL) ? @"":[NSString stringWithUTF8String:search_date_id];
            
			//--search_word
			char *search_word = (char *) sqlite3_column_text(statement, 2);
			NSString *search_wordStr = (search_word == NULL) ? @"":[NSString stringWithUTF8String:search_word];
            
			//--search_content
			char *search_content = (char *) sqlite3_column_text(statement, 3);
			NSString *search_contentStr = (search_content == NULL) ? @"":[NSString stringWithUTF8String:search_content];
            
            
            
			//--add Object
			search_historyField *object = [[search_historyField alloc]init];
            
			object.rowid = rowidStr;
			object.search_date_id = search_date_idStr;
			object.search_word = search_wordStr;
			object.search_content = search_contentStr;
			
			[array addObject:object];
            
			//--free memory
			[object release];
            
		}
	}
    
	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}
    
	return array;
}



/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select Row By rowid In Table

-(search_historyField *) getRowByRowsInTablesearch_historyByRowId:(NSString *) rowid
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE rowid = \"%@\" ", self.usesTable,rowid];
    
	return [self getData_search_historyField_BySQL:qsql];
}

-(search_historyField *) getRowInTablesearch_historyBySearchWord: (NSString *)search_word
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE search_word = \"%@\" ", self.usesTable,search_word];
    
	return [self getData_search_historyField_BySQL:qsql];
}

//--Retrieving Data
-(search_historyField *) getData_search_historyField_BySQL:(NSString *) qsql
{
	sqlite3_stmt *statement;
	search_historyField *object = nil;
    
	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];
            
			//--search_date_id
			char *search_date_id = (char *) sqlite3_column_text(statement, 1);
			NSString *search_date_idStr = (search_date_id == NULL) ? @"":[NSString stringWithUTF8String:search_date_id];
            
			//--search_word
			char *search_word = (char *) sqlite3_column_text(statement, 2);
			NSString *search_wordStr = (search_word == NULL) ? @"":[NSString stringWithUTF8String:search_word];
            
			//--search_content
			char *search_content = (char *) sqlite3_column_text(statement, 3);
			NSString *search_contentStr = (search_content == NULL) ? @"":[NSString stringWithUTF8String:search_content];
            
            
            
			//--add Object
			object = [[[search_historyField alloc] init] autorelease];
            
			object.rowid = rowidStr;
			object.search_date_id = search_date_idStr;
			object.search_word = search_wordStr;
			object.search_content = search_contentStr;
			
		}
	}
    
	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}
    
	return object;
}



/********************************************************************************/
@end