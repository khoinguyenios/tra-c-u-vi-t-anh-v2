

////pronounField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-06-09 07:47:37 +0000. 
//

//--CREATE TABLE "pronoun" ("pronoun_id" TEXT, "pronoun" TEXT, )

#import "pronounModel.h"

@implementation pronounModel

/** init class */
-(id) init
{
	self = [super init];

	if (self)
	{
		self.usesTable = @"pronoun";
	}
	return self;
}


/* free memory */
-(void) dealloc
{
	[super dealloc];
}



/********************************************************************************/
#pragma mark  - Insert Row To Table

-(BOOL)createRowInTablepronoun:(pronounField *) data
{
	NSString *pronoun_id = data.pronoun_id;
	NSString *pronoun = data.pronoun;

	NSString *pronoun_idInit = @"";
	NSString *pronounInit = @"";


	if (pronoun_id)
		pronoun_idInit = pronoun_id;

	if (pronoun)
		pronounInit = pronoun;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		const char *sql = "INSERT INTO \"pronoun\" (\"pronoun_id\",\"pronoun\") VALUES (?1,?2)";

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--pronoun_id
	sqlite3_bind_text(createStmt, 1, [pronoun_idInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--pronoun
	sqlite3_bind_text(createStmt, 2, [pronounInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Update Row To Table

-(BOOL)updateRowInTablepronoun:(pronounField *) data
{

	NSString *pronoun_id = data.pronoun_id;
	NSString *pronoun = data.pronoun;

	NSString *pronoun_idInit = @"";
	NSString *pronounInit = @"";


	if (pronoun_id)
		pronoun_idInit = pronoun_id;

	if (pronoun)
		pronounInit = pronoun;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		NSString *sqlOriginal = [NSString stringWithFormat:@"UPDATE \"pronoun\" SET \"pronoun_id\" = ?, \"pronoun\" = ? WHERE rowid = %@", data.rowid];

		const char *sql = sqlOriginal.UTF8String;

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--pronoun_id
	sqlite3_bind_text(createStmt, 1, [pronoun_idInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--pronoun
	sqlite3_bind_text(createStmt, 2, [pronounInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Delete Row To Table

-(BOOL)deleteRowByRowId:(NSString *) rowid
{
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE rowid = %@", self.usesTable, rowid];
	//--NSLog(@"sql: %@", sql);

	char *err;
	if (sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {

		NSLog(@"reture code: %d",sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err));

		sqlite3_close(db);
		NSAssert(0, @"Error insert table ZCARDGAME.");

	}

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select All Row In Table

-(NSMutableArray *) getAllRowsInTablepronoun
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ ", self.usesTable];

	return [self getArrayDataBy_SQL:qsql];
}


//--Retrieving Array Data
-(NSMutableArray *) getArrayDataBy_SQL:(NSString *)qsql
{
	NSMutableArray *array = [NSMutableArray array];

	sqlite3_stmt *statement;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--pronoun_id
			char *pronoun_id = (char *) sqlite3_column_text(statement, 1);
			NSString *pronoun_idStr = (pronoun_id == NULL) ? @"":[NSString stringWithUTF8String:pronoun_id];

			//--pronoun
			char *pronoun = (char *) sqlite3_column_text(statement, 2);
			NSString *pronounStr = (pronoun == NULL) ? @"":[NSString stringWithUTF8String:pronoun];



			//--add Object
			pronounField *object = [[pronounField alloc]init];

			object.rowid = rowidStr;
			object.pronoun_id = pronoun_idStr;
			object.pronoun = pronounStr;
			
			[array addObject:object];

			//--free memory
			[object release];

		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return array;
}



/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select Row By rowid In Table

-(pronounField *) getRowByRowsInTablepronounByRowId:(NSString *) rowid
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE rowid = \"%@\" ", self.usesTable,rowid];

	return [self getData_pronounField_BySQL:qsql];
}


//--Retrieving Data
-(pronounField *) getData_pronounField_BySQL:(NSString *) qsql
{
	sqlite3_stmt *statement;
	pronounField *object = nil;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--pronoun_id
			char *pronoun_id = (char *) sqlite3_column_text(statement, 1);
			NSString *pronoun_idStr = (pronoun_id == NULL) ? @"":[NSString stringWithUTF8String:pronoun_id];

			//--pronoun
			char *pronoun = (char *) sqlite3_column_text(statement, 2);
			NSString *pronounStr = (pronoun == NULL) ? @"":[NSString stringWithUTF8String:pronoun];



			//--add Object
			object = [[[pronounField alloc] init] autorelease];

			object.rowid = rowidStr;
			object.pronoun_id = pronoun_idStr;
			object.pronoun = pronounStr;
			
		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return object;
}



/********************************************************************************/
@end