

////pronounModel.h 
//  Project 
// 
//  Created by TeamiOS on 2014-06-09 07:47:37 +0000. 
//
// 

#import <Foundation/Foundation.h>
#import "AppModel.h"
#import "pronounField.h"

@interface pronounModel : AppModel
{

}


/** Get all rows in Table pronoun **/
-(NSMutableArray *) getAllRowsInTablepronoun;

/** Get rows by rowid in Table pronoun **/
-(pronounField *) getRowByRowsInTablepronounByRowId:(NSString *) rowid;


/** create a new row **/
-(BOOL)createRowInTablepronoun:(pronounField *) data;

/** update row **/
-(BOOL)updateRowInTablepronoun:(pronounField *) data;

/** delete row **/
-(BOOL)deleteRowByRowId:(NSString *) rowid;

@end