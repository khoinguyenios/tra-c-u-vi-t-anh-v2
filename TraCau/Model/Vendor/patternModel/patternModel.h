

////patternModel.h 
//  Project 
// 
//  Created by TeamiOS on 2014-06-09 07:46:31 +0000. 
//
// 

#import <Foundation/Foundation.h>
#import "AppModel.h"
#import "patternField.h"

@interface patternModel : AppModel
{

}


/** Get all rows in Table pattern **/
-(NSMutableArray *) getAllRowsInTablepattern;

/** Get rows by rowid in Table pattern **/
-(patternField *) getRowByRowsInTablepatternByRowId:(NSString *) rowid;


/** create a new row **/
-(BOOL)createRowInTablepattern:(patternField *) data;

/** update row **/
-(BOOL)updateRowInTablepattern:(patternField *) data;

/** delete row **/
-(BOOL)deleteRowByRowId:(NSString *) rowid;

-(NSMutableArray *) getPatternsByCategoryId: (NSString *)categoryId;

@end