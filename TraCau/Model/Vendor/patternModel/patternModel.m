

////patternField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-06-09 07:46:31 +0000. 
//

//--CREATE TABLE "pattern" ("pattern_id" TEXT, "category_id" TEXT, "sequence" TEXT, "sentence_vi" TEXT, "sentence_en" TEXT, )

#import "patternModel.h"

@implementation patternModel

/** init class */
-(id) init
{
	self = [super init];

	if (self)
	{
		self.usesTable = @"pattern";
	}
	return self;
}


/* free memory */
-(void) dealloc
{
	[super dealloc];
}



/********************************************************************************/
#pragma mark  - Insert Row To Table

-(BOOL)createRowInTablepattern:(patternField *) data
{
	NSString *pattern_id = data.pattern_id;
	NSString *category_id = data.category_id;
	NSString *sequence = data.sequence;
	NSString *sentence_vi = data.sentence_vi;
	NSString *sentence_en = data.sentence_en;

	NSString *pattern_idInit = @"";
	NSString *category_idInit = @"";
	NSString *sequenceInit = @"";
	NSString *sentence_viInit = @"";
	NSString *sentence_enInit = @"";


	if (pattern_id)
		pattern_idInit = pattern_id;

	if (category_id)
		category_idInit = category_id;

	if (sequence)
		sequenceInit = sequence;

	if (sentence_vi)
		sentence_viInit = sentence_vi;

	if (sentence_en)
		sentence_enInit = sentence_en;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		const char *sql = "INSERT INTO \"pattern\" (\"pattern_id\",\"category_id\",\"sequence\",\"sentence_vi\",\"sentence_en\") VALUES (?1,?2,?3,?4,?5)";

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--pattern_id
	sqlite3_bind_text(createStmt, 1, [pattern_idInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--category_id
	sqlite3_bind_text(createStmt, 2, [category_idInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--sequence
	sqlite3_bind_text(createStmt, 3, [sequenceInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--sentence_vi
	sqlite3_bind_text(createStmt, 4, [sentence_viInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--sentence_en
	sqlite3_bind_text(createStmt, 5, [sentence_enInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Update Row To Table

-(BOOL)updateRowInTablepattern:(patternField *) data
{

	NSString *pattern_id = data.pattern_id;
	NSString *category_id = data.category_id;
	NSString *sequence = data.sequence;
	NSString *sentence_vi = data.sentence_vi;
	NSString *sentence_en = data.sentence_en;

	NSString *pattern_idInit = @"";
	NSString *category_idInit = @"";
	NSString *sequenceInit = @"";
	NSString *sentence_viInit = @"";
	NSString *sentence_enInit = @"";


	if (pattern_id)
		pattern_idInit = pattern_id;

	if (category_id)
		category_idInit = category_id;

	if (sequence)
		sequenceInit = sequence;

	if (sentence_vi)
		sentence_viInit = sentence_vi;

	if (sentence_en)
		sentence_enInit = sentence_en;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		NSString *sqlOriginal = [NSString stringWithFormat:@"UPDATE \"pattern\" SET \"pattern_id\" = ?, \"category_id\" = ?, \"sequence\" = ?, \"sentence_vi\" = ?, \"sentence_en\" = ? WHERE rowid = %@", data.rowid];

		const char *sql = sqlOriginal.UTF8String;

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--pattern_id
	sqlite3_bind_text(createStmt, 1, [pattern_idInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--category_id
	sqlite3_bind_text(createStmt, 2, [category_idInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--sequence
	sqlite3_bind_text(createStmt, 3, [sequenceInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--sentence_vi
	sqlite3_bind_text(createStmt, 4, [sentence_viInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--sentence_en
	sqlite3_bind_text(createStmt, 5, [sentence_enInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Delete Row To Table

-(BOOL)deleteRowByRowId:(NSString *) rowid
{
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE rowid = %@", self.usesTable, rowid];
	//--NSLog(@"sql: %@", sql);

	char *err;
	if (sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {

		NSLog(@"reture code: %d",sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err));

		sqlite3_close(db);
		NSAssert(0, @"Error insert table ZCARDGAME.");

	}

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select All Row In Table

-(NSMutableArray *) getAllRowsInTablepattern
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ ", self.usesTable];

	return [self getArrayDataBy_SQL:qsql];
}


-(NSMutableArray *) getPatternsByCategoryId: (NSString *)categoryId
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@  WHERE category_id = \"%@\"", self.usesTable, categoryId];
    
	return [self getArrayDataBy_SQL:qsql];
}



//--Retrieving Array Data
-(NSMutableArray *) getArrayDataBy_SQL:(NSString *)qsql
{
	NSMutableArray *array = [NSMutableArray array];

	sqlite3_stmt *statement;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--pattern_id
			char *pattern_id = (char *) sqlite3_column_text(statement, 1);
			NSString *pattern_idStr = (pattern_id == NULL) ? @"":[NSString stringWithUTF8String:pattern_id];

			//--category_id
			char *category_id = (char *) sqlite3_column_text(statement, 2);
			NSString *category_idStr = (category_id == NULL) ? @"":[NSString stringWithUTF8String:category_id];

			//--sequence
			char *sequence = (char *) sqlite3_column_text(statement, 3);
			NSString *sequenceStr = (sequence == NULL) ? @"":[NSString stringWithUTF8String:sequence];

			//--sentence_vi
			char *sentence_vi = (char *) sqlite3_column_text(statement, 4);
			NSString *sentence_viStr = (sentence_vi == NULL) ? @"":[NSString stringWithUTF8String:sentence_vi];

			//--sentence_en
			char *sentence_en = (char *) sqlite3_column_text(statement, 5);
			NSString *sentence_enStr = (sentence_en == NULL) ? @"":[NSString stringWithUTF8String:sentence_en];



			//--add Object
			patternField *object = [[patternField alloc]init];

			object.rowid = rowidStr;
			object.pattern_id = pattern_idStr;
			object.category_id = category_idStr;
			object.sequence = sequenceStr;
			object.sentence_vi = sentence_viStr;
			object.sentence_en = sentence_enStr;
			
			[array addObject:object];

			//--free memory
			[object release];

		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return array;
}



/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select Row By rowid In Table

-(patternField *) getRowByRowsInTablepatternByRowId:(NSString *) rowid
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE rowid = \"%@\" ", self.usesTable,rowid];

	return [self getData_patternField_BySQL:qsql];
}



//--Retrieving Data
-(patternField *) getData_patternField_BySQL:(NSString *) qsql
{
	sqlite3_stmt *statement;
	patternField *object = nil;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--pattern_id
			char *pattern_id = (char *) sqlite3_column_text(statement, 1);
			NSString *pattern_idStr = (pattern_id == NULL) ? @"":[NSString stringWithUTF8String:pattern_id];

			//--category_id
			char *category_id = (char *) sqlite3_column_text(statement, 2);
			NSString *category_idStr = (category_id == NULL) ? @"":[NSString stringWithUTF8String:category_id];

			//--sequence
			char *sequence = (char *) sqlite3_column_text(statement, 3);
			NSString *sequenceStr = (sequence == NULL) ? @"":[NSString stringWithUTF8String:sequence];

			//--sentence_vi
			char *sentence_vi = (char *) sqlite3_column_text(statement, 4);
			NSString *sentence_viStr = (sentence_vi == NULL) ? @"":[NSString stringWithUTF8String:sentence_vi];

			//--sentence_en
			char *sentence_en = (char *) sqlite3_column_text(statement, 5);
			NSString *sentence_enStr = (sentence_en == NULL) ? @"":[NSString stringWithUTF8String:sentence_en];



			//--add Object
			object = [[[patternField alloc] init] autorelease];

			object.rowid = rowidStr;
			object.pattern_id = pattern_idStr;
			object.category_id = category_idStr;
			object.sequence = sequenceStr;
			object.sentence_vi = sentence_viStr;
			object.sentence_en = sentence_enStr;
			
		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return object;
}



/********************************************************************************/
@end