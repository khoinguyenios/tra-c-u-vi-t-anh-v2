

////favorite_sentencesField.m
//  Project
//
//  Created by TeamiOS on 2014-06-11 03:49:46 +0000.
//

//--CREATE TABLE "favorite_sentences" ("sentence_id" TEXT, "root_id" TEXT, "sentence_vi" TEXT, "sentence_en" TEXT, "sentence_type" TEXT, )

#import "favorite_sentencesModel.h"

@implementation favorite_sentencesModel

/** init class */
-(id) init
{
	self = [super init];
    
	if (self)
	{
		self.usesTable = @"favorite_sentences";
	}
	return self;
}


/* free memory */
-(void) dealloc
{
	[super dealloc];
}



/********************************************************************************/
#pragma mark  - Insert Row To Table

-(BOOL)createRowInTablefavorite_sentences:(favorite_sentencesField *) data
{
	NSString *sentence_id = data.sentence_id;
	NSString *root_id = data.root_id;
	NSString *sentence_vi = data.sentence_vi;
	NSString *sentence_en = data.sentence_en;
	NSString *sentence_type = data.sentence_type;
    
	NSString *sentence_idInit = @"";
	NSString *root_idInit = @"";
	NSString *sentence_viInit = @"";
	NSString *sentence_enInit = @"";
	NSString *sentence_typeInit = @"";
    
    
	if (sentence_id)
		sentence_idInit = sentence_id;
    
	if (root_id)
		root_idInit = root_id;
    
	if (sentence_vi)
		sentence_viInit = sentence_vi;
    
	if (sentence_en)
		sentence_enInit = sentence_en;
    
	if (sentence_type)
		sentence_typeInit = sentence_type;
    
	sqlite3_stmt *createStmt = nil;
    
	if (createStmt == nil)
	{
		//--string sql
		const char *sql = "INSERT INTO \"favorite_sentences\" (\"sentence_id\",\"root_id\",\"sentence_vi\",\"sentence_en\",\"sentence_type\") VALUES (?1,?2,?3,?4,?5)";
        
		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}
    
 	//--sentence_id
	sqlite3_bind_text(createStmt, 1, [sentence_idInit UTF8String], -1, SQLITE_TRANSIENT);
    
 	//--root_id
	sqlite3_bind_text(createStmt, 2, [root_idInit UTF8String], -1, SQLITE_TRANSIENT);
    
 	//--sentence_vi
	sqlite3_bind_text(createStmt, 3, [sentence_viInit UTF8String], -1, SQLITE_TRANSIENT);
    
 	//--sentence_en
	sqlite3_bind_text(createStmt, 4, [sentence_enInit UTF8String], -1, SQLITE_TRANSIENT);
    
 	//--sentence_type
	sqlite3_bind_text(createStmt, 5, [sentence_typeInit UTF8String], -1, SQLITE_TRANSIENT);
    
    
    
	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}
    
	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);
    
	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Update Row To Table

-(BOOL)updateRowInTablefavorite_sentences:(favorite_sentencesField *) data
{
    
	NSString *sentence_id = data.sentence_id;
	NSString *root_id = data.root_id;
	NSString *sentence_vi = data.sentence_vi;
	NSString *sentence_en = data.sentence_en;
	NSString *sentence_type = data.sentence_type;
    
	NSString *sentence_idInit = @"";
	NSString *root_idInit = @"";
	NSString *sentence_viInit = @"";
	NSString *sentence_enInit = @"";
	NSString *sentence_typeInit = @"";
    
    
	if (sentence_id)
		sentence_idInit = sentence_id;
    
	if (root_id)
		root_idInit = root_id;
    
	if (sentence_vi)
		sentence_viInit = sentence_vi;
    
	if (sentence_en)
		sentence_enInit = sentence_en;
    
	if (sentence_type)
		sentence_typeInit = sentence_type;
    
	sqlite3_stmt *createStmt = nil;
    
	if (createStmt == nil)
	{
		//--string sql
		NSString *sqlOriginal = [NSString stringWithFormat:@"UPDATE \"favorite_sentences\" SET \"sentence_id\" = ?, \"root_id\" = ?, \"sentence_vi\" = ?, \"sentence_en\" = ?, \"sentence_type\" = ? WHERE rowid = %@", data.rowid];
        
		const char *sql = sqlOriginal.UTF8String;
        
		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}
    
 	//--sentence_id
	sqlite3_bind_text(createStmt, 1, [sentence_idInit UTF8String], -1, SQLITE_TRANSIENT);
    
 	//--root_id
	sqlite3_bind_text(createStmt, 2, [root_idInit UTF8String], -1, SQLITE_TRANSIENT);
    
 	//--sentence_vi
	sqlite3_bind_text(createStmt, 3, [sentence_viInit UTF8String], -1, SQLITE_TRANSIENT);
    
 	//--sentence_en
	sqlite3_bind_text(createStmt, 4, [sentence_enInit UTF8String], -1, SQLITE_TRANSIENT);
    
 	//--sentence_type
	sqlite3_bind_text(createStmt, 5, [sentence_typeInit UTF8String], -1, SQLITE_TRANSIENT);
    
    
    
	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}
    
	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);
    
	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Delete Row To Table

-(BOOL)deleteRowByRowId:(NSString *) rowid
{
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE rowid = %@", self.usesTable, rowid];
	//--NSLog(@"sql: %@", sql);
    
	char *err;
	if (sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {
        
		NSLog(@"reture code: %d",sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err));
        
		sqlite3_close(db);
		NSAssert(0, @"Error insert table ZCARDGAME.");
        
	}
    
	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select All Row In Table

-(NSMutableArray *) getAllRowsInTablefavorite_sentences
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ ", self.usesTable];
    
	return [self getArrayDataBy_SQL:qsql];
}



-(NSMutableArray *) getRowsInTablefavorite_sentencesByRootId: (NSString *)rootId
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE root_id = \"%@\" ", self.usesTable, rootId];
    
	return [self getArrayDataBy_SQL:qsql];
}


//--Retrieving Array Data
-(NSMutableArray *) getArrayDataBy_SQL:(NSString *)qsql
{
	NSMutableArray *array = [NSMutableArray array];
    
	sqlite3_stmt *statement;
    
	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];
            
			//--sentence_id
			char *sentence_id = (char *) sqlite3_column_text(statement, 1);
			NSString *sentence_idStr = (sentence_id == NULL) ? @"":[NSString stringWithUTF8String:sentence_id];
            
			//--root_id
			char *root_id = (char *) sqlite3_column_text(statement, 2);
			NSString *root_idStr = (root_id == NULL) ? @"":[NSString stringWithUTF8String:root_id];
            
			//--sentence_vi
			char *sentence_vi = (char *) sqlite3_column_text(statement, 3);
			NSString *sentence_viStr = (sentence_vi == NULL) ? @"":[NSString stringWithUTF8String:sentence_vi];
            
			//--sentence_en
			char *sentence_en = (char *) sqlite3_column_text(statement, 4);
			NSString *sentence_enStr = (sentence_en == NULL) ? @"":[NSString stringWithUTF8String:sentence_en];
            
			//--sentence_type
			char *sentence_type = (char *) sqlite3_column_text(statement, 5);
			NSString *sentence_typeStr = (sentence_type == NULL) ? @"":[NSString stringWithUTF8String:sentence_type];
            
            
            
			//--add Object
			favorite_sentencesField *object = [[favorite_sentencesField alloc]init];
            
			object.rowid = rowidStr;
			object.sentence_id = sentence_idStr;
			object.root_id = root_idStr;
			object.sentence_vi = sentence_viStr;
			object.sentence_en = sentence_enStr;
			object.sentence_type = sentence_typeStr;
			
			[array addObject:object];
            
			//--free memory
			[object release];
            
		}
	}
    
	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}
    
	return array;
}



/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select Row By rowid In Table

-(favorite_sentencesField *) getRowByRowsInTablefavorite_sentencesByRowId:(NSString *) rowid
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE rowid = \"%@\" ", self.usesTable,rowid];
    
	return [self getData_favorite_sentencesField_BySQL:qsql];
}


//--Retrieving Data
-(favorite_sentencesField *) getData_favorite_sentencesField_BySQL:(NSString *) qsql
{
	sqlite3_stmt *statement;
	favorite_sentencesField *object = nil;
    
	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];
            
			//--sentence_id
			char *sentence_id = (char *) sqlite3_column_text(statement, 1);
			NSString *sentence_idStr = (sentence_id == NULL) ? @"":[NSString stringWithUTF8String:sentence_id];
            
			//--root_id
			char *root_id = (char *) sqlite3_column_text(statement, 2);
			NSString *root_idStr = (root_id == NULL) ? @"":[NSString stringWithUTF8String:root_id];
            
			//--sentence_vi
			char *sentence_vi = (char *) sqlite3_column_text(statement, 3);
			NSString *sentence_viStr = (sentence_vi == NULL) ? @"":[NSString stringWithUTF8String:sentence_vi];
            
			//--sentence_en
			char *sentence_en = (char *) sqlite3_column_text(statement, 4);
			NSString *sentence_enStr = (sentence_en == NULL) ? @"":[NSString stringWithUTF8String:sentence_en];
            
			//--sentence_type
			char *sentence_type = (char *) sqlite3_column_text(statement, 5);
			NSString *sentence_typeStr = (sentence_type == NULL) ? @"":[NSString stringWithUTF8String:sentence_type];
            
            
            
			//--add Object
			object = [[[favorite_sentencesField alloc] init] autorelease];
            
			object.rowid = rowidStr;
			object.sentence_id = sentence_idStr;
			object.root_id = root_idStr;
			object.sentence_vi = sentence_viStr;
			object.sentence_en = sentence_enStr;
			object.sentence_type = sentence_typeStr;
			
		}
	}
    
	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}
    
	return object;
}



/********************************************************************************/
@end