

////favorite_sentencesModel.h 
//  Project 
// 
//  Created by TeamiOS on 2014-06-09 07:45:58 +0000. 
//
// 

#import <Foundation/Foundation.h>
#import "AppModel.h"
#import "favorite_sentencesField.h"

@interface favorite_sentencesModel : AppModel
{

}


/** Get all rows in Table favorite_sentences **/
-(NSMutableArray *) getAllRowsInTablefavorite_sentences;

/** Get rows by rowid in Table favorite_sentences **/
-(favorite_sentencesField *) getRowByRowsInTablefavorite_sentencesByRowId:(NSString *) rowid;


/** create a new row **/
-(BOOL)createRowInTablefavorite_sentences:(favorite_sentencesField *) data;

/** update row **/
-(BOOL)updateRowInTablefavorite_sentences:(favorite_sentencesField *) data;

/** delete row **/
-(BOOL)deleteRowByRowId:(NSString *) rowid;

/** get rows by root id **/
-(NSMutableArray *) getRowsInTablefavorite_sentencesByRootId: (NSString *)rootId;

@end