

////conjugation_typeField.m
//  Project
//
//  Created by TeamiOS on 2014-06-19 03:04:15 +0000.
//

//--CREATE TABLE "conjugation_type" ("conjugation_type_id" TEXT, "type" TEXT, "description" TEXT, "conjugation_group_id" TEXT, )

#import "conjugation_typeModel.h"

@implementation conjugation_typeModel

/** init class */
-(id) init
{
	self = [super init];
    
	if (self)
	{
		self.usesTable = @"conjugation_type";
	}
	return self;
}


/* free memory */
-(void) dealloc
{
	[super dealloc];
}



/********************************************************************************/
#pragma mark  - Insert Row To Table

-(BOOL)createRowInTableconjugation_type:(conjugation_typeField *) data
{
	NSString *conjugation_type_id = data.conjugation_type_id;
	NSString *type = data.type;
	NSString *description = data.description;
	NSString *conjugation_group_id = data.conjugation_group_id;
    
	NSString *conjugation_type_idInit = @"";
	NSString *typeInit = @"";
	NSString *descriptionInit = @"";
	NSString *conjugation_group_idInit = @"";
    
    
	if (conjugation_type_id)
		conjugation_type_idInit = conjugation_type_id;
    
	if (type)
		typeInit = type;
    
	if (description)
		descriptionInit = description;
    
	if (conjugation_group_id)
		conjugation_group_idInit = conjugation_group_id;
    
	sqlite3_stmt *createStmt = nil;
    
	if (createStmt == nil)
	{
		//--string sql
		const char *sql = "INSERT INTO \"conjugation_type\" (\"conjugation_type_id\",\"type\",\"description\",\"conjugation_group_id\") VALUES (?1,?2,?3,?4)";
        
		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}
    
 	//--conjugation_type_id
	sqlite3_bind_text(createStmt, 1, [conjugation_type_idInit UTF8String], -1, SQLITE_TRANSIENT);
    
 	//--type
	sqlite3_bind_text(createStmt, 2, [typeInit UTF8String], -1, SQLITE_TRANSIENT);
    
 	//--description
	sqlite3_bind_text(createStmt, 3, [descriptionInit UTF8String], -1, SQLITE_TRANSIENT);
    
 	//--conjugation_group_id
	sqlite3_bind_text(createStmt, 4, [conjugation_group_idInit UTF8String], -1, SQLITE_TRANSIENT);
    
    
    
	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}
    
	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);
    
	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Update Row To Table

-(BOOL)updateRowInTableconjugation_type:(conjugation_typeField *) data
{
    
	NSString *conjugation_type_id = data.conjugation_type_id;
	NSString *type = data.type;
	NSString *description = data.description;
	NSString *conjugation_group_id = data.conjugation_group_id;
    
	NSString *conjugation_type_idInit = @"";
	NSString *typeInit = @"";
	NSString *descriptionInit = @"";
	NSString *conjugation_group_idInit = @"";
    
    
	if (conjugation_type_id)
		conjugation_type_idInit = conjugation_type_id;
    
	if (type)
		typeInit = type;
    
	if (description)
		descriptionInit = description;
    
	if (conjugation_group_id)
		conjugation_group_idInit = conjugation_group_id;
    
	sqlite3_stmt *createStmt = nil;
    
	if (createStmt == nil)
	{
		//--string sql
		NSString *sqlOriginal = [NSString stringWithFormat:@"UPDATE \"conjugation_type\" SET \"conjugation_type_id\" = ?, \"type\" = ?, \"description\" = ?, \"conjugation_group_id\" = ? WHERE rowid = %@", data.rowid];
        
		const char *sql = sqlOriginal.UTF8String;
        
		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}
    
 	//--conjugation_type_id
	sqlite3_bind_text(createStmt, 1, [conjugation_type_idInit UTF8String], -1, SQLITE_TRANSIENT);
    
 	//--type
	sqlite3_bind_text(createStmt, 2, [typeInit UTF8String], -1, SQLITE_TRANSIENT);
    
 	//--description
	sqlite3_bind_text(createStmt, 3, [descriptionInit UTF8String], -1, SQLITE_TRANSIENT);
    
 	//--conjugation_group_id
	sqlite3_bind_text(createStmt, 4, [conjugation_group_idInit UTF8String], -1, SQLITE_TRANSIENT);
    
    
    
	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}
    
	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);
    
	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Delete Row To Table

-(BOOL)deleteRowByRowId:(NSString *) rowid
{
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE rowid = %@", self.usesTable, rowid];
	//--NSLog(@"sql: %@", sql);
    
	char *err;
	if (sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {
        
		NSLog(@"reture code: %d",sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err));
        
		sqlite3_close(db);
		NSAssert(0, @"Error insert table ZCARDGAME.");
        
	}
    
	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select All Row In Table

-(NSMutableArray *) getAllRowsInTableconjugation_type
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ ", self.usesTable];
    
	return [self getArrayDataBy_SQL:qsql];
}


-(NSMutableArray *) getRowsInTableconjugation_typeByConjugationGroupId: (NSString *)conjugation_group_id
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE  conjugation_group_id = %@", self.usesTable, conjugation_group_id];
    
	return [self getArrayDataBy_SQL:qsql];
}


//--Retrieving Array Data
-(NSMutableArray *) getArrayDataBy_SQL:(NSString *)qsql
{
	NSMutableArray *array = [NSMutableArray array];
    
	sqlite3_stmt *statement;
    
	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];
            
			//--conjugation_type_id
			char *conjugation_type_id = (char *) sqlite3_column_text(statement, 1);
			NSString *conjugation_type_idStr = (conjugation_type_id == NULL) ? @"":[NSString stringWithUTF8String:conjugation_type_id];
            
			//--type
			char *type = (char *) sqlite3_column_text(statement, 2);
			NSString *typeStr = (type == NULL) ? @"":[NSString stringWithUTF8String:type];
            
			//--description
			char *description = (char *) sqlite3_column_text(statement, 3);
			NSString *descriptionStr = (description == NULL) ? @"":[NSString stringWithUTF8String:description];
            
			//--conjugation_group_id
			char *conjugation_group_id = (char *) sqlite3_column_text(statement, 4);
			NSString *conjugation_group_idStr = (conjugation_group_id == NULL) ? @"":[NSString stringWithUTF8String:conjugation_group_id];
            
            
            
			//--add Object
			conjugation_typeField *object = [[conjugation_typeField alloc]init];
            
			object.rowid = rowidStr;
			object.conjugation_type_id = conjugation_type_idStr;
			object.type = typeStr;
			object.description = descriptionStr;
			object.conjugation_group_id = conjugation_group_idStr;
			
			[array addObject:object];
            
			//--free memory
			[object release];
            
		}
	}
    
	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}
    
	return array;
}



/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select Row By rowid In Table

-(conjugation_typeField *) getRowByRowsInTableconjugation_typeByRowId:(NSString *) rowid
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE rowid = \"%@\" ", self.usesTable,rowid];
    
	return [self getData_conjugation_typeField_BySQL:qsql];
}


//--Retrieving Data
-(conjugation_typeField *) getData_conjugation_typeField_BySQL:(NSString *) qsql
{
	sqlite3_stmt *statement;
	conjugation_typeField *object = nil;
    
	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];
            
			//--conjugation_type_id
			char *conjugation_type_id = (char *) sqlite3_column_text(statement, 1);
			NSString *conjugation_type_idStr = (conjugation_type_id == NULL) ? @"":[NSString stringWithUTF8String:conjugation_type_id];
            
			//--type
			char *type = (char *) sqlite3_column_text(statement, 2);
			NSString *typeStr = (type == NULL) ? @"":[NSString stringWithUTF8String:type];
            
			//--description
			char *description = (char *) sqlite3_column_text(statement, 3);
			NSString *descriptionStr = (description == NULL) ? @"":[NSString stringWithUTF8String:description];
            
			//--conjugation_group_id
			char *conjugation_group_id = (char *) sqlite3_column_text(statement, 4);
			NSString *conjugation_group_idStr = (conjugation_group_id == NULL) ? @"":[NSString stringWithUTF8String:conjugation_group_id];
            
            
            
			//--add Object
			object = [[[conjugation_typeField alloc] init] autorelease];
            
			object.rowid = rowidStr;
			object.conjugation_type_id = conjugation_type_idStr;
			object.type = typeStr;
			object.description = descriptionStr;
			object.conjugation_group_id = conjugation_group_idStr;
			
		}
	}
    
	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}
    
	return object;
}



/********************************************************************************/
@end