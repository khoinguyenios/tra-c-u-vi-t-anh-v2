

////conjugation_typeModel.h 
//  Project 
// 
//  Created by TeamiOS on 2014-06-19 03:04:15 +0000. 
//
// 

#import <Foundation/Foundation.h>
#import "AppModel.h"
#import "conjugation_typeField.h"

@interface conjugation_typeModel : AppModel
{

}


/** Get all rows in Table conjugation_type **/
-(NSMutableArray *) getAllRowsInTableconjugation_type;

-(NSMutableArray *) getRowsInTableconjugation_typeByConjugationGroupId: (NSString *)conjugation_group_id;

/** Get rows by rowid in Table conjugation_type **/
-(conjugation_typeField *) getRowByRowsInTableconjugation_typeByRowId:(NSString *) rowid;


/** create a new row **/
-(BOOL)createRowInTableconjugation_type:(conjugation_typeField *) data;

/** update row **/
-(BOOL)updateRowInTableconjugation_type:(conjugation_typeField *) data;

/** delete row **/
-(BOOL)deleteRowByRowId:(NSString *) rowid;

@end