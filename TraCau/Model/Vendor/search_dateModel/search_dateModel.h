

////search_dateModel.h 
//  Project 
// 
//  Created by TeamiOS on 2014-06-16 07:47:12 +0000. 
//
// 

#import <Foundation/Foundation.h>
#import "AppModel.h"
#import "search_dateField.h"

@interface search_dateModel : AppModel
{

}


/** Get all rows in Table search_date **/
-(NSMutableArray *) getAllRowsInTablesearch_date;

/** Get rows by rowid in Table search_date **/
-(search_dateField *) getRowByRowsInTablesearch_dateByRowId:(NSString *) rowid;
-(search_dateField *) getRowInTablesearch_dateByDate: (NSString *)date;


/** create a new row **/
-(BOOL)createRowInTablesearch_date:(search_dateField *) data;

/** update row **/
-(BOOL)updateRowInTablesearch_date:(search_dateField *) data;

/** delete row **/
-(BOOL)deleteRowByRowId:(NSString *) rowid;
-(BOOL)clearAllRowsInTablesearch_date;

@end