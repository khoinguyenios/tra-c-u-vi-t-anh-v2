

////search_dateField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-06-16 07:47:12 +0000. 
//

//--CREATE TABLE "search_date" ("date" TEXT, )

#import "search_dateModel.h"

@implementation search_dateModel

/** init class */
-(id) init
{
	self = [super init];

	if (self)
	{
		self.usesTable = @"search_date";
	}
	return self;
}


/* free memory */
-(void) dealloc
{
	[super dealloc];
}

#pragma mark -
#pragma mark Clear Table
/*************************************************************************************************************************/


-(BOOL)clearAllRowsInTablesearch_date
{
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ ", self.usesTable];
	//--NSLog(@"sql: %@", sql);
    
	char *err;
	if (sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {
        
		NSLog(@"reture code: %d",sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err));
        
		sqlite3_close(db);
		NSAssert(0, @"Error delete table ZCARDGAME.");
        
	}
    
	return YES;
}


/*************************************************************************************************************************/





/********************************************************************************/
#pragma mark  - Insert Row To Table

-(BOOL)createRowInTablesearch_date:(search_dateField *) data
{
	NSString *date = data.date;

	NSString *dateInit = @"";


	if (date)
		dateInit = date;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		const char *sql = "INSERT INTO \"search_date\" (\"date\") VALUES (?1)";

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--date
	sqlite3_bind_text(createStmt, 1, [dateInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Update Row To Table

-(BOOL)updateRowInTablesearch_date:(search_dateField *) data
{

	NSString *date = data.date;

	NSString *dateInit = @"";


	if (date)
		dateInit = date;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		NSString *sqlOriginal = [NSString stringWithFormat:@"UPDATE \"search_date\" SET \"date\" = ? WHERE rowid = %@", data.rowid];

		const char *sql = sqlOriginal.UTF8String;

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--date
	sqlite3_bind_text(createStmt, 1, [dateInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Delete Row To Table

-(BOOL)deleteRowByRowId:(NSString *) rowid
{
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE rowid = %@", self.usesTable, rowid];
	//--NSLog(@"sql: %@", sql);

	char *err;
	if (sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {

		NSLog(@"reture code: %d",sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err));

		sqlite3_close(db);
		NSAssert(0, @"Error insert table ZCARDGAME.");

	}

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select All Row In Table

-(NSMutableArray *) getAllRowsInTablesearch_date
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ ", self.usesTable];

	return [self getArrayDataBy_SQL:qsql];
}


//--Retrieving Array Data
-(NSMutableArray *) getArrayDataBy_SQL:(NSString *)qsql
{
	NSMutableArray *array = [NSMutableArray array];

	sqlite3_stmt *statement;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--date
			char *date = (char *) sqlite3_column_text(statement, 1);
			NSString *dateStr = (date == NULL) ? @"":[NSString stringWithUTF8String:date];



			//--add Object
			search_dateField *object = [[search_dateField alloc]init];

			object.rowid = rowidStr;
			object.date = dateStr;
			
			[array addObject:object];

			//--free memory
			[object release];

		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return array;
}



/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select Row By rowid In Table

-(search_dateField *) getRowByRowsInTablesearch_dateByRowId:(NSString *) rowid
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE rowid = \"%@\" ", self.usesTable,rowid];

	return [self getData_search_dateField_BySQL:qsql];
}

-(search_dateField *) getRowInTablesearch_dateByDate: (NSString *)date
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE date = \"%@\" ", self.usesTable,date];
    
	return [self getData_search_dateField_BySQL:qsql];
}

//--Retrieving Data
-(search_dateField *) getData_search_dateField_BySQL:(NSString *) qsql
{
	sqlite3_stmt *statement;
	search_dateField *object = nil;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--date
			char *date = (char *) sqlite3_column_text(statement, 1);
			NSString *dateStr = (date == NULL) ? @"":[NSString stringWithUTF8String:date];



			//--add Object
			object = [[[search_dateField alloc] init] autorelease];

			object.rowid = rowidStr;
			object.date = dateStr;
			
		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return object;
}



/********************************************************************************/
@end