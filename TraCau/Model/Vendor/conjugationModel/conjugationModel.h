

////conjugationModel.h 
//  Project 
// 
//  Created by TeamiOS on 2014-06-09 07:42:26 +0000. 
//
// 

#import <Foundation/Foundation.h>
#import "AppModel.h"
#import "conjugationField.h"

@interface conjugationModel : AppModel
{

}


/** Get all rows in Table conjugation **/
-(NSMutableArray *) getAllRowsInTableconjugation;

/** Get rows by rowid in Table conjugation **/
-(conjugationField *) getRowByRowsInTableconjugationByRowId:(NSString *) rowid;


/** create a new row **/
-(BOOL)createRowInTableconjugation:(conjugationField *) data;

/** update row **/
-(BOOL)updateRowInTableconjugation:(conjugationField *) data;

/** delete row **/
-(BOOL)deleteRowByRowId:(NSString *) rowid;

//------------------------------------------------------------------------------------------
/**  get RowIds In Table conjugation By VerbId  */
-(NSArray *)getRowIdsInTableconjugationByVerbId: (NSString *)verbId;

//------------------------------------------------------------------------------------------
/**  get RowIds In Table conjugation By VerbId And Type id  */
-(NSArray *)getRowIdsInTableconjugationByVerbId: (NSString *)verbId AndConjugationTypeId: (NSString *)conjugation_type_id;
@end