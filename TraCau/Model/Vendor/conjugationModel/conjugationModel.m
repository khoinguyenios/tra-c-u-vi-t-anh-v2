

////conjugationField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-06-09 07:42:26 +0000. 
//

//--CREATE TABLE "conjugation" ("conjugation_id" TEXT, "verb" TEXT, "verbid" TEXT, "pronounid" TEXT, "conjugation_type_id" TEXT, "not_used" TEXT, "orto" TEXT, "irregular" TEXT, )

#import "conjugationModel.h"

@implementation conjugationModel

/** init class */
-(id) init
{
	self = [super init];

	if (self)
	{
		self.usesTable = @"conjugation";
	}
	return self;
}


/* free memory */
-(void) dealloc
{
	[super dealloc];
}



/********************************************************************************/
#pragma mark  - Insert Row To Table

-(BOOL)createRowInTableconjugation:(conjugationField *) data
{
	NSString *conjugation_id = data.conjugation_id;
	NSString *verb = data.verb;
	NSString *verbid = data.verbid;
	NSString *pronounid = data.pronounid;
	NSString *conjugation_type_id = data.conjugation_type_id;
	NSString *not_used = data.not_used;
	NSString *orto = data.orto;
	NSString *irregular = data.irregular;

	NSString *conjugation_idInit = @"";
	NSString *verbInit = @"";
	NSString *verbidInit = @"";
	NSString *pronounidInit = @"";
	NSString *conjugation_type_idInit = @"";
	NSString *not_usedInit = @"";
	NSString *ortoInit = @"";
	NSString *irregularInit = @"";


	if (conjugation_id)
		conjugation_idInit = conjugation_id;

	if (verb)
		verbInit = verb;

	if (verbid)
		verbidInit = verbid;

	if (pronounid)
		pronounidInit = pronounid;

	if (conjugation_type_id)
		conjugation_type_idInit = conjugation_type_id;

	if (not_used)
		not_usedInit = not_used;

	if (orto)
		ortoInit = orto;

	if (irregular)
		irregularInit = irregular;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		const char *sql = "INSERT INTO \"conjugation\" (\"conjugation_id\",\"verb\",\"verbid\",\"pronounid\",\"conjugation_type_id\",\"not_used\",\"orto\",\"irregular\") VALUES (?1,?2,?3,?4,?5,?6,?7,?8)";

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--conjugation_id
	sqlite3_bind_text(createStmt, 1, [conjugation_idInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--verb
	sqlite3_bind_text(createStmt, 2, [verbInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--verbid
	sqlite3_bind_text(createStmt, 3, [verbidInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--pronounid
	sqlite3_bind_text(createStmt, 4, [pronounidInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--conjugation_type_id
	sqlite3_bind_text(createStmt, 5, [conjugation_type_idInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--not_used
	sqlite3_bind_text(createStmt, 6, [not_usedInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--orto
	sqlite3_bind_text(createStmt, 7, [ortoInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--irregular
	sqlite3_bind_text(createStmt, 8, [irregularInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Update Row To Table

-(BOOL)updateRowInTableconjugation:(conjugationField *) data
{

	NSString *conjugation_id = data.conjugation_id;
	NSString *verb = data.verb;
	NSString *verbid = data.verbid;
	NSString *pronounid = data.pronounid;
	NSString *conjugation_type_id = data.conjugation_type_id;
	NSString *not_used = data.not_used;
	NSString *orto = data.orto;
	NSString *irregular = data.irregular;

	NSString *conjugation_idInit = @"";
	NSString *verbInit = @"";
	NSString *verbidInit = @"";
	NSString *pronounidInit = @"";
	NSString *conjugation_type_idInit = @"";
	NSString *not_usedInit = @"";
	NSString *ortoInit = @"";
	NSString *irregularInit = @"";


	if (conjugation_id)
		conjugation_idInit = conjugation_id;

	if (verb)
		verbInit = verb;

	if (verbid)
		verbidInit = verbid;

	if (pronounid)
		pronounidInit = pronounid;

	if (conjugation_type_id)
		conjugation_type_idInit = conjugation_type_id;

	if (not_used)
		not_usedInit = not_used;

	if (orto)
		ortoInit = orto;

	if (irregular)
		irregularInit = irregular;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		NSString *sqlOriginal = [NSString stringWithFormat:@"UPDATE \"conjugation\" SET \"conjugation_id\" = ?, \"verb\" = ?, \"verbid\" = ?, \"pronounid\" = ?, \"conjugation_type_id\" = ?, \"not_used\" = ?, \"orto\" = ?, \"irregular\" = ? WHERE rowid = %@", data.rowid];

		const char *sql = sqlOriginal.UTF8String;

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--conjugation_id
	sqlite3_bind_text(createStmt, 1, [conjugation_idInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--verb
	sqlite3_bind_text(createStmt, 2, [verbInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--verbid
	sqlite3_bind_text(createStmt, 3, [verbidInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--pronounid
	sqlite3_bind_text(createStmt, 4, [pronounidInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--conjugation_type_id
	sqlite3_bind_text(createStmt, 5, [conjugation_type_idInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--not_used
	sqlite3_bind_text(createStmt, 6, [not_usedInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--orto
	sqlite3_bind_text(createStmt, 7, [ortoInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--irregular
	sqlite3_bind_text(createStmt, 8, [irregularInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Delete Row To Table

-(BOOL)deleteRowByRowId:(NSString *) rowid
{
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE rowid = %@", self.usesTable, rowid];
	//--NSLog(@"sql: %@", sql);

	char *err;
	if (sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {

		NSLog(@"reture code: %d",sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err));

		sqlite3_close(db);
		NSAssert(0, @"Error insert table ZCARDGAME.");

	}

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select All Row In Table

-(NSMutableArray *) getAllRowsInTableconjugation
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ ", self.usesTable];

	return [self getArrayDataBy_SQL:qsql];
}



//------------------------------------------------------------------------------------------
/**  get RowIds In Table conjugation By VerbId  */
-(NSArray *)getRowIdsInTableconjugationByVerbId: (NSString *)verbId
{
    //--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT conjugation_id FROM %@ WHERE verbid = \"%@\"", self.usesTable, verbId];
    
    NSMutableArray *array = [NSMutableArray array];
    
	sqlite3_stmt *statement;
    
	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *conjugation_id = (char *) sqlite3_column_text(statement, 0);
			NSString *conjugation_idStr  = [NSString stringWithUTF8String:conjugation_id];

			[array addObject:conjugation_idStr];
		}
	}
    
	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

    return array;
}




//------------------------------------------------------------------------------------------
/**  get RowIds In Table conjugation By VerbId And Type id  */
-(NSArray *)getRowIdsInTableconjugationByVerbId: (NSString *)verbId AndConjugationTypeId: (NSString *)conjugation_type_id
{
    //--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE verbid = \"%@\"  AND conjugation_type_id = \"%@\"", self.usesTable, verbId, conjugation_type_id];
    
//    NSMutableArray *array = [NSMutableArray array];
//    
//	sqlite3_stmt *statement;
//    
//	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
//	{
//		while(sqlite3_step(statement) == SQLITE_ROW)
//		{
//			//--rowid
//			char *conjugation_id = (char *) sqlite3_column_text(statement, 0);
//			NSString *conjugation_idStr  = [NSString stringWithUTF8String:conjugation_id];
//            
//			[array addObject:conjugation_idStr];
//		}
//	}
//    
//	if (statement)
//	{
//		sqlite3_reset(statement);
//		sqlite3_finalize(statement);
//	}
//    
//    return array;
    return [self getArrayDataBy_SQL:qsql];
}





//--Retrieving Array Data
-(NSMutableArray *) getArrayDataBy_SQL:(NSString *)qsql
{
	NSMutableArray *array = [NSMutableArray array];

	sqlite3_stmt *statement;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--conjugation_id
			char *conjugation_id = (char *) sqlite3_column_text(statement, 1);
			NSString *conjugation_idStr = (conjugation_id == NULL) ? @"":[NSString stringWithUTF8String:conjugation_id];

			//--verb
			char *verb = (char *) sqlite3_column_text(statement, 2);
			NSString *verbStr = (verb == NULL) ? @"":[NSString stringWithUTF8String:verb];

			//--verbid
			char *verbid = (char *) sqlite3_column_text(statement, 3);
			NSString *verbidStr = (verbid == NULL) ? @"":[NSString stringWithUTF8String:verbid];

			//--pronounid
			char *pronounid = (char *) sqlite3_column_text(statement, 4);
			NSString *pronounidStr = (pronounid == NULL) ? @"":[NSString stringWithUTF8String:pronounid];

			//--conjugation_type_id
			char *conjugation_type_id = (char *) sqlite3_column_text(statement, 5);
			NSString *conjugation_type_idStr = (conjugation_type_id == NULL) ? @"":[NSString stringWithUTF8String:conjugation_type_id];

			//--not_used
			char *not_used = (char *) sqlite3_column_text(statement, 6);
			NSString *not_usedStr = (not_used == NULL) ? @"":[NSString stringWithUTF8String:not_used];

			//--orto
			char *orto = (char *) sqlite3_column_text(statement, 7);
			NSString *ortoStr = (orto == NULL) ? @"":[NSString stringWithUTF8String:orto];

			//--irregular
			char *irregular = (char *) sqlite3_column_text(statement, 8);
			NSString *irregularStr = (irregular == NULL) ? @"":[NSString stringWithUTF8String:irregular];



			//--add Object
			conjugationField *object = [[conjugationField alloc]init];

			object.rowid = rowidStr;
			object.conjugation_id = conjugation_idStr;
			object.verb = verbStr;
			object.verbid = verbidStr;
			object.pronounid = pronounidStr;
			object.conjugation_type_id = conjugation_type_idStr;
			object.not_used = not_usedStr;
			object.orto = ortoStr;
			object.irregular = irregularStr;
			
			[array addObject:object];

			//--free memory
			[object release];

		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return array;
}



/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select Row By rowid In Table

-(conjugationField *) getRowByRowsInTableconjugationByRowId:(NSString *) rowid
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE rowid = \"%@\" ", self.usesTable,rowid];

	return [self getData_conjugationField_BySQL:qsql];
}


//--Retrieving Data
-(conjugationField *) getData_conjugationField_BySQL:(NSString *) qsql
{
	sqlite3_stmt *statement;
	conjugationField *object = nil;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--conjugation_id
			char *conjugation_id = (char *) sqlite3_column_text(statement, 1);
			NSString *conjugation_idStr = (conjugation_id == NULL) ? @"":[NSString stringWithUTF8String:conjugation_id];

			//--verb
			char *verb = (char *) sqlite3_column_text(statement, 2);
			NSString *verbStr = (verb == NULL) ? @"":[NSString stringWithUTF8String:verb];

			//--verbid
			char *verbid = (char *) sqlite3_column_text(statement, 3);
			NSString *verbidStr = (verbid == NULL) ? @"":[NSString stringWithUTF8String:verbid];

			//--pronounid
			char *pronounid = (char *) sqlite3_column_text(statement, 4);
			NSString *pronounidStr = (pronounid == NULL) ? @"":[NSString stringWithUTF8String:pronounid];

			//--conjugation_type_id
			char *conjugation_type_id = (char *) sqlite3_column_text(statement, 5);
			NSString *conjugation_type_idStr = (conjugation_type_id == NULL) ? @"":[NSString stringWithUTF8String:conjugation_type_id];

			//--not_used
			char *not_used = (char *) sqlite3_column_text(statement, 6);
			NSString *not_usedStr = (not_used == NULL) ? @"":[NSString stringWithUTF8String:not_used];

			//--orto
			char *orto = (char *) sqlite3_column_text(statement, 7);
			NSString *ortoStr = (orto == NULL) ? @"":[NSString stringWithUTF8String:orto];

			//--irregular
			char *irregular = (char *) sqlite3_column_text(statement, 8);
			NSString *irregularStr = (irregular == NULL) ? @"":[NSString stringWithUTF8String:irregular];



			//--add Object
			object = [[[conjugationField alloc] init] autorelease];

			object.rowid = rowidStr;
			object.conjugation_id = conjugation_idStr;
			object.verb = verbStr;
			object.verbid = verbidStr;
			object.pronounid = pronounidStr;
			object.conjugation_type_id = conjugation_type_idStr;
			object.not_used = not_usedStr;
			object.orto = ortoStr;
			object.irregular = irregularStr;
			
		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return object;
}



/********************************************************************************/
@end